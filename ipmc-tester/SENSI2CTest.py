'''
The Sensor I2C subtest validate the functionality of the Sensor i2c bus located on the edge connector.
Therefore, it uses a temperature sensor connected to the bus, poll it address and check whether it
was detected. Finally, it compares its value to the on-mezzanine sensor allowing the tester to check
both the i2c bus and the internal sensor calibration.

The pass value depends on the test result:
    *  0: success
    * -1: ATCA init failed
    * -2: Internal sensor read failed (IPMI error)
    * -3: Tester sensor read failed (IPMI error)
    * -4: Tester sensor read failed (Bus error)
    * -5: Sensor delta is out of range
    * -6: Wrong address polling failed (IPMI error)
    * -7: Wrong address polling failed (Bus error)
'''

#!/usr/bin/env python
# encoding: utf-8
import time
import pprint
import random

from IPMCDevLib import IPMCDevCom as IPMCDevCom
from IPMCDevLib import IPMCDevATCA as IPMCDevATCA

import lib.IPMI as IPMI
import lib.sysapi as sysapi

@sysapi.register_debug('I2C Sensor Subtest')
def test_i2c_sensor(hostname='192.168.1.34', timeout=100, **kwargs):
    '''
    The Sensor I2C subtest validate the functionality of the Sensor i2c bus located on the edge connector.
    Therefore, it uses a temperature sensor connected to the bus, poll it address and check whether it
    was detected. Finally, it compares its value to the on-mezzanine sensor allowing the tester to check
    both the i2c bus and the internal sensor calibration.

    When the timeout value is exceeded during the ATCA reset phase, the test is considered to
    be failed.

    :Parameters:

        :timeout: Maximum time to wait for a function to be executed.

    :Returns:
        The function returns a dictionary formated to be used in the test instance.

        .. code-block:: javascript

            {
                'test_type_name': 'I2CSENS_v.1.0',
                'name': 'i2c_sensor_tester',

                'details': {
                    'timeout': timeout
                },

                'summary': {
                    'sensor_delta': ontester-onboard,
                    'tester_temp': ontester,
                    'verbose': 'Success',
                    'duration': round((time.time()-start),2)
                },

                'measurements': measurements,
                'pass': passed
            }

        Where pass can be:
            *  0: success
            * -1: ATCA init failed
            * -2: Internal sensor read failed (IPMI error)
            * -3: Tester sensor read failed (IPMI error)
            * -4: Tester sensor read failed (Bus error)
            * -5: Sensor delta is out of range
            * -6: Wrong address polling failed (IPMI error)
            * -7: Wrong address polling failed (Bus error)
    '''

    debug_level = kwargs['debug_level'] if 'debug_level' in kwargs else 0
    IPMCDevObject = IPMCDevCom.IPMCDevCom(**kwargs)
    IPMCDevATCAObject = IPMCDevATCA.IPMCDevATCA(IPMCDevObject)

    start = time.time()

    timeout = int(timeout)

    passed = 0
    measurements = []

    ipmiLan = IPMI.IPMI(hostname=hostname, debug = 1 if debug_level >= 10 else 0)

    measurements = []

    try:
        #r = ipmiLan.ipmi_send(0x04, 0x2d, [0x0c]) #Read temperature sensor
        ipmc_temp = ipmiLan.ipmc_sensor_read('IPMC_Temp') #Read temperature sensor

    except Exception as e:
        IPMCDevObject.close()
        ipmiLan.close()

        return {
            'test_type_name': 'I2CSENS_v.1.0',
            'name': 'i2c_sensor_tester',

            'details': {
                'timeout': timeout
            },

            'summary': {
                'verbose': 'Internal sensor cannot be read ({})'.format(str(e)),
                'duration': round((time.time()-start),2)
            },

            'measurements': measurements,
            'pass': -2
        }

    measurements.append({'name': 'temperature', 'data': {'s': 'IPMC', 'v': ipmc_temp}, 'pass': 0})
    #onboard = ipmc_temp
    print(f'[Info] IPMC temperature sensor: {ipmc_temp} degC')

    try:
        devkit_temp = ipmiLan.ipmc_sensor_read('Devkit_Temp') #Read tester temperature

    except:
        IPMCDevObject.close()
        ipmiLan.close()

        return {
            'test_type_name': 'I2CSENS_v.1.0',
            'name': 'i2c_sensor_tester',

            'details': {
                'hardware_address': hardware_address,
                'timeout': timeout
            },

            'summary': {
                'verbose': 'Tester sensor cannot be read',
                'duration': round((time.time()-start),2)
            },

            'measurements': measurements,
            'pass': -3
        }

    measurements.append({'name': 'temperature', 'data': {'s': 'On tester', 'v': devkit_temp}, 'pass': 0})
    print(f'[Info] DevKit temperature sensor: {devkit_temp} degC')

    temp_delta = abs(ipmc_temp-devkit_temp)
    if temp_delta >= 10.0:
        IPMCDevObject.close()
        ipmiLan.close()

        return {
            'test_type_name': 'I2CSENS_v.1.0',
            'name': 'i2c_sensor_tester',

            'details': {
                'timeout': timeout
            },

            'summary': {
                'sensor_delta': temp_delta,
                'tester_temp': ipmc_temp,
                'verbose': 'Sensor delta is out of tolerance',
                'duration': round((time.time()-start),2)
            },

            'measurements': measurements,
            'pass': -5
        }

    try:
        r = ipmiLan.ipmi_send(46, 0x53, [96, 0, 0, 140]) #Read tester temperature (wrong sensor addr)

    except:
        IPMCDevObject.close()
        ipmiLan.close()

        return {
            'test_type_name': 'I2CSENS_v.1.0',
            'name': 'i2c_sensor_tester',

            'details': {
                'timeout': timeout
            },

            'summary': {
                'verbose': 'Wrong address polling failed',
                'duration': round((time.time()-start),2)
            },

            'measurements': measurements,
            'pass': -6
        }

    if len(r) < 4:
        print('ISSUE in SENSORTEST(1): r = {}'.format(r))

        IPMCDevObject.close()
        ipmiLan.close()

        return {
            'test_type_name': 'I2CSENS_v.1.0',
            'name': 'i2c_sensor_tester',

            'details': {
                'timeout': timeout
            },

            'summary': {
                'verbose': 'Wrong address polling failed',
                'duration': round((time.time()-start),2)
            },

            'measurements': measurements,
            'pass': -6
        }

    if r[3] == 0:
        IPMCDevObject.close()
        ipmiLan.close()

        return {
            'test_type_name': 'I2CSENS_v.1.0',
            'name': 'i2c_sensor_tester',

            'details': {
                'timeout': timeout
            },

            'summary': {
                'sensor_delta': temp_delta,
                'tester_temp': ipmc_temp,
                'verbose': 'Wrong address polling failed (Bus error)',
                'duration': round((time.time()-start),2)
            },

            'measurements': measurements,
            'pass': -6
        }


    IPMCDevObject.close()
    ipmiLan.close()

    return {
        'test_type_name': 'I2CSENS_v.1.0',
        'name': 'i2c_sensor_tester',

        'details': {
            'timeout': timeout
        },

        'summary': {
            'sensor_delta': temp_delta,
            'tester_temp': ipmc_temp,
            'verbose': 'Success',
            'duration': round((time.time()-start),2)
        },

        'measurements': measurements,
        'pass': passed
    }

if __name__ == "__main__":
    pprint.pprint(test_i2c_sensor( hw_address=0x43, timeout = 100))
