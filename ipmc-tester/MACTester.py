'''
The MAC address test is more a configuration procedure than a real test. Even if it
tests the lan connectivity with the CERN-IPMC module, it generates a unique MAC
address using the database and flash it into the module.

.. graphviz::
    :align: center

    digraph G {
        node [fontname = "Handlee"];
        edge [fontname = "Handlee"];

        start [
            label = "Start";
            shape = oval;
        ];

        A [
            label = "Generate MAC address";
            shape = rect;
        ];

        B [
            label = "Check address";
            shape = diamond;
        ];

        C [
            label = "Reset ATCA (using HA=0x00)";
            shape = rect;
        ];

        D [
            label = "Write MAC address";
            shape = rect;
        ];

        end [
            label = "End";
            shape = oval;
        ];



        start -> A;
        A -> B;
        B -> C [label = "Unique"];
        B:e -> A:e [label = "Used"];
        C -> D;
        D -> end;
    }

The pass value depends on the test result:
    *  0: success
    * -1: ATCA init failed
    * -2: Database connection failed
    * -3: MAC address generation failed
    * -4: OEM command failed
'''

#!/usr/bin/env python
# encoding: utf-8
import time
import pprint
import random
import os

from IPMCDevLib import IPMCDevCom as IPMCDevCom
from IPMCDevLib import IPMCDevATCA as IPMCDevATCA

import lib.IPMI as IPMI
import lib.sysapi as sysapi

def increment_mac_addr(macAddr, maxMacAddr):
    for i in range(len(macAddr)):
        if macAddr[len(macAddr)-i-1] < 0xFF:
            macAddr[len(macAddr)-i-1] += 1

            if macAddr[len(macAddr)-i-1] > maxMacAddr[len(macAddr)-i-1]:
                raise Exception('New MAC Address value [{}] is out of range [max: {}]'.format(':'.join(['{:02x}'.format(x) for x in macAddr]), ':'.join(['{:02x}'.format(x) for x in maxMacAddr])))

            return macAddr
        else:
            macAddr[len(macAddr)-i-1] = 0

    raise Exception('New MAC Address value [{}] is out of range [max: {}]'.format(':'.join(['{:02x}'.format(x) for x in macAddr]), ':'.join(['{:02x}'.format(x) for x in maxMacAddr])))

@sysapi.register_debug('MAC Address Subtest')
def test_mac_address(serial_number, build_number, hardware_address=0x43, timeout = 100, **kwargs):
    '''
    The MAC address test is more a configuration procedure than a real test. Even if it
    tests the lan connectivity with the CERN-IPMC module, it generates a unique MAC
    address using the database and flash it into the module.

    When the timeout value is exceeded for a specific function, the test is considered to
    be failed.

    :Parameters:
        :serial_nb: Serial number
        :prod_nb: Production number
        :hw_addr: ATCA Hardware address (default: 0x43)
        :timeout: Maximum time to wait for a function to be executed

    Returns:
        The function returns a dictionary formated to be used in the test instance.

        .. code-block:: javascript

            {
                'test_type_name': 'LAN_v.1.0',
                'name': 'lan_mac_setting',

                'details': {
                    'serial_number': serial_number,
                    'production_number': build_number,
                    'hardware_address': hardware_address,
                    'timeout': timeout
                },

                'summary': {
                    'verbose': 'Success',
                    'mac_address': ':'.join(['{:02x}'.format(x) for x in macAddr]),
                    'duration': round((time.time()-start),2)
                },

                'measurements': [],
                'pass': 0
            }

        Where pass can be:
            *  0: success
            * -1: ATCA init failed
            * -2: Database connection failed
            * -3: MAC address generation failed
            * -4: OEM command failed
            * -4: DB connection details not found
    '''

    MAC_ADDRESS_MIN = [0x80,0xd3,0x36,0x00,0x40,0x00]
    MAC_ADDRESS_MAX = [0x80,0xd3,0x36,0x00,0x43,0xFF]
    MAC_ADDRESS_USED = [
            [0x80,0xd3,0x36,0x00,0x40,0x00],
            [0x80,0xd3,0x36,0x00,0x40,0x01],
            [0x80,0xd3,0x36,0x00,0x40,0x02],
            [0x80,0xd3,0x36,0x00,0x40,0x03],
            [0x80,0xd3,0x36,0x00,0x40,0x04],
            [0x80,0xd3,0x36,0x00,0x40,0x05],
            [0x80,0xd3,0x36,0x00,0x40,0x06],
            [0x80,0xd3,0x36,0x00,0x40,0x07],
            [0x80,0xd3,0x36,0x00,0x40,0x08],
            [0x80,0xd3,0x36,0x00,0x40,0x09],
            [0x80,0xd3,0x36,0x00,0x40,0x0a],
            [0x80,0xd3,0x36,0x00,0x40,0x0b],
            [0x80,0xd3,0x36,0x00,0x40,0x0c],
            [0x80,0xd3,0x36,0x00,0x40,0x0d]
        ]

    start = time.time()
    hardware_address = int(hardware_address)
    timeout = int(timeout)

    sysapi.set_job_progress(10)


    try:
        host = os.environ['IPMC_DB_HOST']
        user = os.environ['IPMC_DB_USERNAME']
        port = int(os.environ['IPMC_DB_PORT'])
        passwd = os.environ['IPMC_DB_PASSWD']
        dbname = os.environ['IPMC_DB_DBNAME']
    except:
        return {
                'test_type_name': 'LAN_v.1.0',
                'name': 'lan_mac_setting',

                'details': {
                    'serial_number': serial_number,
                    'production_number': build_number,
                    'hardware_address': hardware_address,
                    'timeout': timeout
                },

                'summary': {
                    'verbose': 'DB connection details were not found',
                    'duration': round((time.time()-start),2)
                },

                'measurements': [],
                'pass': -5
            }

    #db = PSMSFramework.PSQLWriter.PSQLWriter(host=host,username=user,password=passwd,port=port,dbname=dbname)

    try:
        device = db.getDevice(deviceDesc = {'sn': serial_number, 'pn': build_number})

        if len(device) > 1:
            db.disconnect()
            IPMCDevObject.close()

            return {
                'test_type_name': 'LAN_v.1.0',
                'name': 'lan_mac_setting',

                'details': {
                    'serial_number': serial_number,
                    'production_number': build_number,
                    'hardware_address': hardware_address,
                    'timeout': timeout
                },

                'summary': {
                    'verbose': 'Too many devices found for sn={} and pn={} [{} modules]'.format(serial_number, build_number, len(device)),
                    'duration': round((time.time()-start),2)
                },

                'measurements': [],
                'pass': -2
            }

        else:
            deviceId = device[0]['deviceId']
            print("\tDevice found: {}".format(deviceId))
            macStrArr = device[0]['deviceDetails']['mac'].split(':')
            macAddr = [int(x,16) for x in macStrArr]

    except:

        '''New device - generate mac address'''
        macAddr = MAC_ADDRESS_MIN

        while True:
            #1st step: generate macAddr
            while True:
                if macAddr in MAC_ADDRESS_USED:
                    try:
                        macAddr = increment_mac_addr(macAddr, MAC_ADDRESS_MAX)
                    except Exception as e:

                        db.disconnect()

                        return {
                            'test_type_name': 'LAN_v.1.0',
                            'name': 'lan_mac_setting',

                            'details': {
                                'serial_number': serial_number,
                                'production_number': build_number,
                                'hardware_address': hardware_address,
                                'timeout': timeout
                            },

                            'summary': {
                                'verbose': str(e),
                                'duration': round((time.time()-start),2)
                            },

                            'measurements': [],
                            'pass': -3
                        }
                else:
                    break

            try:
                device = db.getDevice(deviceDesc = {'mac':':'.join(['{:02x}'.format(x) for x in macAddr])})
                macAddr = increment_mac_addr(macAddr, MAC_ADDRESS_MAX)
            except:
                #Does not exist - Add device
                deviceDetails =  {'sn': serial_number, 'pn':build_number, 'mac':':'.join(['{:02x}'.format(x) for x in macAddr])}
                print("New device: {}".format(deviceDetails))
                break

    db.disconnect()

    sysapi.set_job_progress(40)

    IPMCDevObject = IPMCDevCom.IPMCDevCom(**kwargs)
    IPMCDevATCAObject = IPMCDevATCA.IPMCDevATCA(IPMCDevObject)

    try:
        #pwr_off_timeout, pwr_on_timeout = configure_atca(IPMCDevATCAObject, hardware_address, timeout)
        pwr_off_timeout, pwr_on_timeout = 0, 0

    except Exception as e:
        IPMCDevObject.close()

        return {
            'test_type_name': 'LAN_v.1.0',
            'name': 'lan_mac_setting',

            'details': {
                'serial_number': serial_number,
                'production_number': build_number,
                'hardware_address': hardware_address,
                'timeout': timeout
            },

            'summary': {
                'verbose': 'ATCA Init failed',
                'mac_address': ':'.join(['{:02x}'.format(x) for x in macAddr]),
                'duration': round((time.time()-start),2)
            },

            'measurements': [],
            'pass': -1
        }

    sysapi.set_job_progress(60)

    try:
        print('Init lan')
        ipmiLan = IPMI.IPMI(hostname='192.168.1.34', username='', password='')
        print('prepare command')
        data = [96, 0, 0]
        data.extend(reversed(macAddr))
        r = ipmiLan.ipmi_send(46, 0x52, data)
        print('command sent')

        sysapi.set_job_progress(90)

        IPMCDevObject.close()
        ipmiLan.close()

    except:
        return {
            'test_type_name': 'LAN_v.1.0',
            'name': 'lan_mac_setting',

            'details': {
                'serial_number': serial_number,
                'production_number': build_number,
                'hardware_address': hardware_address,
                'timeout': timeout
            },

            'summary': {
                'verbose': 'IPMI command failed with ccode={}'.format(r),
                'mac_address': ':'.join(['{:02x}'.format(x) for x in macAddr]),
                'duration': round((time.time()-start),2)
            },

            'measurements': [],
            'pass': -4
        }

    return {
        'test_type_name': 'LAN_v.1.0',
        'name': 'lan_mac_setting',

        'details': {
            'serial_number': serial_number,
            'production_number': build_number,
            'hardware_address': hardware_address,
            'timeout': timeout
        },

        'summary': {
            'verbose': 'Success',
            'mac_address': ':'.join(['{:02x}'.format(x) for x in macAddr]),
            'duration': round((time.time()-start),2)
        },

        'measurements': [],
        'pass': 0
    }

if __name__ == "__main__":
    pprint.pprint(test_mac_address('P20200001','0000 0001', hardware_address=0x43, timeout = 100))
