#!/usr/bin/env python3

'''
The ATCA Programmer subtest configures the FPGA logic and its embedded flash memory for the SoC firmware.

The pass value depends on the test result:
    *  0: success
    * -1: Init failed - device not found?
    * -2: Erase failed
    * -3: Failed to program FPGA array
    * -4: Failed to verify FPGA array
    * -5: Failed to program flash memory
    * -6: Failed to verify flash memory
'''

# encoding: utf-8
import os
import sys
import subprocess
import time
import pprint
import json

import lib.sysapi as sysapi

from queue import Queue, Empty
from threading  import Thread

def enqueue_output(out, queue):
    '''
    Get messages from jtag-player binary.

    Run a separated thread that pushes into a queue the messages received by the
    subprocess. It is use to connect both stdout and stderr, allowing the main
    program to retreive the output and extract the information.
    '''

    for line in iter(out.readline, b''):
        queue.put(line)

@sysapi.register_debug('Program')
def program(file_path, **kwargs):
    '''
    The program function read the STAPL file passed as input and execute the program action
    using the jtag-player tool. This binary was modify to report messages encoded in json,
    allowing the programing test to get status using the stdout output. Therefore, the test
    get the progress and reports the final status.

    Args:
        file_path: STAPL file to be programmed

    Returns:
        The function returns a dictionary formated to be used in the test instance.

        .. code-block:: javascript

            {
                'test_type_name': 'PROGRAM_v.1.0',
                'name': 'jam_player_program',

                'details': {
                    'file': file_path
                },

                'summary': {
                    'verbose': exitVerbose,
                    'exitcode': exitCode,
                    'finalState': currState,
                    'duration': round((time.time()-start),2)
                },

                'measurements': measurements,
                'pass': exitCode
            }

        Where pass can be:
            *  0: success
            * -1: Init failed - device not found?
            * -2: Erase failed
            * -3: Failed to program FPGA array
            * -4: Failed to verify FPGA array
            * -5: Failed to program flash memory
            * -6: Failed to verify flash memory
    '''

    start = time.time()

    print(f'[INFO] program file \"{file_path}\"')
    process = subprocess.Popen(['jtag-player '+os.path.abspath(file_path)+' -aPROGRAM'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    
    stdoutQueue = Queue()
    stderrQueue = Queue()

    stdoutThread = Thread(target=enqueue_output, args=(process.stdout, stdoutQueue))
    stdoutThread.daemon = True
    stdoutThread.start()

    stderrThread = Thread(target=enqueue_output, args=(process.stderr, stderrQueue))
    stderrThread.daemon = True
    stderrThread.start()

    currState = -1

    start = time.time()
    step_start = time.time()
    step_duration = {}
    
    measurements = []
    verbose = ['Init','Erase','FPGA Array','FPGA Verify','Flash Memory','Flash verify']

    exitVerbose = ''
    exitCode = -1
    
    while True:
        try:  nextStdoutline = stdoutQueue.get_nowait()
        except Empty:
            pass     #Null: nothing to do
        else:
            msg = nextStdoutline.decode()
            if 'EXPORT:' in msg:
                msg = msg.replace('EXPORT:','')
                try:
                    m = json.loads(msg)
                except Exception as e:
                    print('Warning: message decoding issue ({})'.format(msg))

                if m['key'] == 'message':
                    tmp = currState

                    if m['value'] == 'Erase ...':   currState = 0
                    if m['value'] == 'Programming FPGA Array':   currState = 1
                    if m['value'] == 'Verifying FPGA Array':   currState = 2
                    if m['value'] == 'Program Embedded Flash Memory Module ALL....':   currState = 3
                    if m['value'] == 'Verify Embedded Flash Memory Module ALL...':   currState = 4

                    print('[Info]     {}'.format(m['value']))

                    if currState != tmp:
                        measurements.append({'name': 'timer', 'data': {'s': verbose[1+tmp], 'v': (time.time() - step_start)*1000}, 'pass': 0 })
                        step_start = time.time()

            if 'STATUS:' in msg:
                msg = msg.replace('STATUS:','')
                try:
                    m = json.loads(msg)
                except Exception as e:
                    print('Warning: message decoding issue ({})'.format(msg))

                if m['key'] == 'exit':
                    measurements.append({'name': 'timer', 'data': {'s': verbose[1+currState], 'v': (time.time() - step_start)*1000}, 'pass': 0 })
                    exitCode = m['code']
                    exitVerbose = m['verbose']

        if stdoutThread.is_alive() == False and stdoutQueue.empty():
            break

    procret = process.wait()

    if currState == -1:
        exitCode = -1
        exitVerbose = 'Init failed - device not found?'

    if currState == 0:
        exitCode = -2
        exitVerbose = 'Erase failed'

    if currState == 1:
        exitCode = -3
        exitVerbose = 'Failed to program FPGA array'

    if currState == 2:
        exitCode = -4
        exitVerbose = 'Failed to verify FPGA array'

    if currState == 3:
        exitCode = -5
        exitVerbose = 'Failed to program flash memory'

    if exitCode == -1 and currState == 4:
        exitCode = -6
        exitVerbose = 'Failed to verify flash memory'


    return {
        'test_type_name': 'PROGRAM_v.1.0',
        'name': 'jam_player_program',

        'details': {
            'file': file_path
        },

        'summary': {
            'verbose': exitVerbose,
            'exitcode': exitCode,
            'finalState': currState,
            'duration': round((time.time()-start),2)
        },

        'measurements': measurements,
        'pass': exitCode
    }

if __name__ == "__main__":

    if len(sys.argv) != 2:
        print('Error: please specify reset or a stapl file to be flashed \n\nExamples:\n\t> jtag_player -reset\n\t> jtag_player default.stp')
        exit(-1)

    if sys.argv[1] == '-reset':
        filePath = os.path.dirname(os.path.abspath(__file__))
        filePath = '{}/../ipmc-config/stapl/20201130_cern_ipmc.stp'.format(filePath)

    else:
        filePath = sys.argv[1]

    pprint.pprint(program(filePath))
