'''
The UART subtest checks the functionality of the IPMC UARTs.

The pass value depends on the test result:
    *  0: Success
    * -1: Error
'''

#!/usr/bin/env python
# encoding: utf-8
import time
import pprint

import lib.sysapi as sysapi


import lib.IPMI as IPMI

@sysapi.register_debug('UART Subtest')
def test_uart(uarts, hostname='192.168.1.34', timeout=100, **kwargs):
    '''
    The UART subtest checks the functionality of the IPMC UARTs using ipmitool.

    Args:
        uart: UART device file or a list of UART device files to be tested
        timeout: Maximum time to wait for a function to be executed

    Returns:
        The function returns a list of dictionary formated to be used in the test instance.
        Each instance of the list is one subtest, linked to a specific UART.

        .. code-block:: javascript

            [{
                'test_type_name': 'UART_v.1.0',
                'name': 'uart_tester_{}'.format(uart),

                'details': {
                    'uart': uart,
                    'timeout': timeout
                },

                'summary': {
                    'verbose': verbose,
                    'duration': round((time.time()-start),2)
                },

                'measurements': measurements,
                'pass': passed
            }, ...]

        Where pass can be:
            *  0: success
            * -1: error
    '''

    if isinstance(uarts, list):
        uart_list = uarts
    else:
        uart_list = [str(uarts)]

    timeout = int(timeout)

    start = time.time()

    subtests = []

    debug_level = kwargs['debug_level'] if 'debug_level' in kwargs else 0

    # read FRU from IPMC LAN
    ipmiLan = IPMI.IPMI(hostname, debug = 1 if debug_level >= 10 else 0)
    fru_lan = ipmiLan.ipmi_fru_read()
    ipmiLan.close()

    for uart in uart_list:
        measurements = []
        passed = 0
        verbose = 'Test successfully passed'
        print('[Info] UART {}'.format(uart))

        # open UART
        ipmiUart=None
        try:
            ipmiUart = IPMI.IPMI(serial=uart)
        except Exception as e:
            measurements.append({'name': 'get_uart', 'data': {'s': 'Open UART', 'v': -1}, 'pass': -1 })
            passed = -1
            verbose = 'IPMI issue opening UART: {}'.format(str(e))
            print(f'[ERROR] {verbose}')

        # read FRU from IPMC UART
        if ipmiUart:
            fru_uart=None
            try:
                fru_uart = ipmiUart.ipmi_fru_read()
            except Exception as e:
                measurements.append({'name': 'read_fru', 'data': {'s': 'Read FRU', 'v': -1}, 'pass': -2 })
                passed = -2
                verbose = 'IPMI issue reading FRU: {}'.format(str(e))
                print(f'[ERROR] {verbose}')

            if fru_uart:
                if fru_uart != fru_lan:
                    measurements.append({'name': 'read_fru', 'data': {'s': 'Check FRU', 'v': fru_uart}, 'pass': -3 })
                    passed = -3
                    verbose = 'FRU incorrect: {}\n[ERROR] FRU expected:  {}'.format(fru_uart,fru_lan)
                    print(f'[ERROR] {verbose}')
                else:
                    measurements.append({'name': 'read_fru', 'data': {'s': 'Check FRU', 'v': fru_uart}, 'pass': 0 })

            ipmiUart.close()

        subtests.append({ 'test_type_name': 'UART_v.1.0',
                          'name': 'uart_tester_{}'.format(uart),
                          'details': { 'uart': uart,
                                       'timeout': timeout
                          },
                          'summary': { 'verbose': verbose,
                                       'duration': round((time.time()-start),2)
                          },
                          'measurements': measurements,
                          'pass': passed
        })

    return subtests

if __name__ == "__main__":
    uart_list = [ '/dev/ipmc-payload' ]
    pprint.pprint(test_uart(list(uart_list), timeout = 100))
