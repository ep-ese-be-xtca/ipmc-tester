#!/usr/bin/env python3

"""
The 'CERN-IPMC tester' checks the module functionality.

This test is always executed after the module production. It allows programming the device and
checking all of the interfaces of the CERN-IPMC. Therefore, it pass through a set of subtests
as detailed below:

All of the results are stored into the cern-ipmc dedicated database.
"""

import time
from datetime import datetime
import getpass
#import git
import os
import argparse
import pickle

import AMCSlotTester
import ATCAProgrammer
import HATest
import ATCATester
import LEDTest
import ConfigTest
import IOTest
import MACTester
import MGTI2CTest
import Power
import SENSI2CTest
import UARTTest
#import SNBNFlasher

import lib.sysapi as sysapi

from IPMCDevLib import IPMCDevCom as IPMCDevCom
from IPMCDevLib import IPMCDevATCA as IPMCDevATCA

@sysapi.register_test('CERN-IPMC General test')
def cernipmc_test():
    #Device details
    dev_sn = PSMSFramework.sysapi.get_data('Device PN/SN','''
        Please, inform what is the device under test. To do this you can eithe:
        <ul>
            <li>Scan the QR code located on the device sticker</li>
            <li>Enter the PN/SN value manually</li>
        </ul>
    ''')
    boardSplittedRef = dev_sn.rstrip().split("/")
    return cernipmc_test_seq(boardSplittedRef, True)

def cernipmc_test_seq(boardSplittedRef=[None, None], ha_address=0x41, hostname='192.168.1.34', enableProgramming=False, enableMACFlashing=False, **kwargs):

    enablePowerTesting = True
    enableHaTesting = True
    enableAtcaTesting = True
    enableLedTesting = True
    enableAmcTesting = True
    enableIoTesting = True
    enableMgtI2cTesting = True
    enableSensorI2cTesting = True
    enableUartTesting = True
    enableConfigTesting = True

    #Test details
    try:
        repo = git.Repo(search_parent_directories=True)
        sha = repo.head.object.hexsha
        branch = repo.active_branch
    except:
        repo = 'Unknown'
        sha = 'Unknown'
        branch = 'Unknown'

    operator = getpass.getuser()

    device = {
        'name': 'CERN-IPMC',
        'details': {
            'pn': boardSplittedRef[0],
            'sn': boardSplittedRef[1],
        },
        'batch': {
            'details': {
                'partnumber': boardSplittedRef[0]
            }
        }
    }

    global_res = 0
    global_prt = ''
    subtests = []

    # set the H/W address
    IPMCDevObject = IPMCDevCom.IPMCDevCom(**kwargs)
    IPMCDevATCAObject = IPMCDevATCA.IPMCDevATCA(IPMCDevObject)
    IPMCDevATCAObject.SETHA(ha_address)
    print('[Info] Set hardware address {:02x}h\n'.format(ha_address))

    #Power subtest
    if enablePowerTesting:
        print('[Test] Power check')
        s = Power.power_check(measurement_count = 20, **kwargs)
        subtests.append(s)
        if s['pass'] != 0:
            global_res |= 0x01
            global_prt += '[Result] Power test FAILED!\n'
        print('[Result] Power test {}\n'.format('passed' if s['pass'] == 0 else 'failed'))

    #Program
    if enableProgramming:
        print('[Test] Module programming')
        filePath = os.path.dirname(os.path.abspath(__file__))
        filePath = '{}/../ipmc-config/stapl/cern_ipmc_tester.stp'.format(filePath)

        s = ATCAProgrammer.program(filePath,**kwargs)

        subtests.append(s)
        if s['pass'] != 0:
            global_res |= 0x02
            global_prt += '[Result] Programming test FAILED!\n'
        print('[Result] Programming test {}\n'.format('passed' if s['pass'] == 0 else 'failed'))

#MJ: disabled    #Flash serial number
#MJ: disabled    SNBNFlasher.test_flash_snbn(build_number=boardSplittedRef[0], serial_number=boardSplittedRef[1], hw_address=0x41, timeout = 100)

    #HA
    if enableHaTesting:
        print('[Test] Test HA function')
        ha_list = [ 0x01, 0x02, 0x04, 0x08, 0x092, 0x20, 0x40 ]
        slist = HATest.test_ha(ha_list, hardware_address=ha_address, hostname=hostname, **kwargs)
        for s in slist:
            subtests.append(s)
            if s['pass'] != 0:
                global_res |= 0x04
                global_prt += '[Result] HA test {} FAILED!\n'.format(hex(s['details']['ha']))
        print('[Result] HA test {}\n'.format('passed' if s['pass'] == 0 else 'failed'))

    #ATCA
    if enableAtcaTesting:
        print('[Test] Test ATCA function')
        s = ATCATester.test_atca(hardware_address=ha_address, hostname=hostname, **kwargs)
        subtests.append(s)
        if s['pass'] != 0:
            global_res |= 0x08
            global_prt += '[Result] ATCA test FAILED!\n'
        print('[Result] ATCA test {}\n'.format('passed' if s['pass'] == 0 else 'failed'))

    #LED
    if enableLedTesting:
        print('[Test] Test LED function')
        slist = LEDTest.test_led(hardware_address=ha_address, hostname=hostname, **kwargs)
        for s in slist:
            subtests.append(s)
            if s['pass'] != 0:
                global_res |= 0x10
                global_prt += '[Result] LED test {} FAILED!\n'.format(hex(s['details']['led']))
        print('[Result] LED test {}\n'.format('passed' if s['pass'] == 0 else 'failed'))

    #LAN
    if enableMACFlashing:
        print('[Test] Test LAN interface')
        s = MACTester.test_mac_address(serial_number=boardSplittedRef[1], build_number=boardSplittedRef[0], hardware_address=ha_address, **kwargs)
        subtests.append(s)
        if s['pass'] != 0:
            global_res |= 0x20
            global_prt += '[Result] LAN test FAILED!\n'
        else:
            device['details']['mac'] = s['summary']['mac_address']
        print('[Result] LAN test {}\n'.format('passed' if s['pass'] == 0 else 'failed'))

    #AMC
    if enableAmcTesting:
        print('[Test] Test AMC interfaces')
        slist = AMCSlotTester.amc_tester([0,1,2,3,4,5,6,7,8], [0xea,0x72,0x74,0x76,0x78,0x7a,0x7c,0x7e,0x80], **kwargs)
        for s in slist:
            subtests.append(s)
            if s['pass'] != 0:
                global_res |= 0x40
                global_prt += '[Result] AMC test {} FAILED!\n'.format(s['details']['slot'])
        print('[Result] AMC test {}\n'.format('passed' if s['pass'] == 0 else 'failed'))

    #IO
    if enableIoTesting:
        print('[Test] Test IO interfaces')
        io_list = range(0,51)
        slist = IOTest.test_io(list(io_list), hostname=hostname, **kwargs)
        for s in slist:
            subtests.append(s)
            if s['pass'] != 0:
                global_res |= 0x80
                global_prt += '[Result] I/O test {} {} FAILED!\n'.format(s['details']['typ'],s['details']['io'])
        print('[Result] I/O test {}\n'.format('passed' if s['pass'] == 0 else 'failed'))

    #MGTI2C
    if enableMgtI2cTesting:
        print('[Test] Test MGT I2C interface')
        s = MGTI2CTest.test_i2c_mgt(count=250, hostname=hostname, **kwargs)
        subtests.append(s)
        if s['pass'] != 0:
            global_res |= 0x100
            global_prt += '[Result] MGT I2C test FAILED!\n'
        print('[Result] MGT I2C test {}\n'.format('passed' if s['pass'] == 0 else 'failed'))

    #SENSI2C
    if enableSensorI2cTesting:
        print('[Test] Test Sensor I2C interface')
        s = SENSI2CTest.test_i2c_sensor(hostname=hostname, **kwargs)
        subtests.append(s)
        if s['pass'] != 0:
            global_res |= 0x200
            global_prt += '[Result] Sensor I2C test FAILED!\n'
        print('[Result] Sensor I2C test {}\n'.format('passed' if s['pass'] == 0 else 'failed'))

    #UART
    if enableUartTesting:
        uart_devices = 3 if (kwargs['uart_devices'] is None) else kwargs['uart_devices']
        if uart_devices > 0:
            uart_list = []
            if (uart_devices & 1):
                uart_list.append('/dev/ipmc-payload')
            if (uart_devices & 2):
                uart_list.append('/dev/ipmc-debug')
            print('[Test] Test UART interfaces')
            slist = UARTTest.test_uart(list(uart_list), hostname=hostname, **kwargs)
            for s in slist:
                subtests.append(s)
                if s['pass'] != 0:
                    global_res |= 0x400
                    global_prt += '[Result] UART test {} FAILED!\n'.format(s['details']['uart'])
            print('[Result] UART test {}\n'.format('passed' if s['pass'] == 0 else 'failed'))

    #CONFIG
    if enableConfigTesting:
        kwargs['hostname'] = hostname
        print('[Test] Test Configuration')
        s = ConfigTest.config_test(**kwargs)
        subtests.append(s)
        if s['pass'] != 0:
            global_res |= 0x800
            global_prt += '[Result] Configuration test FAILED!\n'
        print('[Result] Configuration test {}\n'.format('passed' if s['pass'] == 0 else 'failed'))

    if global_res != 0:
        print('[Result] Summary - SOME TEST(S) FAILED:\n{}'.format(global_prt))
    else:
        print('[Result] Summary: all tests requests passed - SUCCESS!')

    return {
        'pass': -global_res,
        'details': {
            'commit': sha,
            'branch': str(branch),
            'operator': operator
        },
        'device': device,
        'subtests': subtests,

        'display': [
            {
                'test_type_name': 'POWER_v.1.0',
                'title': 'Power subtest',
                'summary': {
                    'type': 'table', #Only table is supported for subtest display
                    'columns': [
                        {'col': 'details', 'key': 'measurement_count', 'verbose': 'Number of measurement(s)'},
                        {'col': 'summary', 'key': 'tester_voltage', 'verbose': 'Tester\'s voltage'},
                        {'col': 'summary', 'key': 'ipmc_voltage', 'verbose': 'IPMC\'s voltage'},
                        {'col': 'summary', 'key': 'ipmc_current', 'verbose': 'IPMC\'s current'},
                        {'col': 'summary', 'key': 'verbose', 'verbose': 'Status'}
                    ]
                },
                'measurements': [
                    {
                        'type': 'scatter',
                        'label': 'CERN-IPMC power',
                        'title': 'power variation',
                        'doc': 'During the power test, the module voltage and current are taken several times to compute the average.',
                        'point_names': 'power_meas',
                        'ykey': 'ipmc_power'
                    }
                ],
                'doc': 'The power check test measures the IPMC tester voltage and CERN-IPMC power to ensure the functionnality of the module.'
            },
            {
                'test_type_name': 'PROGRAM_v.1.0',
                'title': 'Device programming',
                'summary': {
                    'type': 'table', #Only table is supported for subtest display
                    'columns': [
                        {'col': 'details', 'key': 'file', 'verbose': 'File programmed'},
                        {'col': 'summary', 'key': 'duration', 'verbose': 'Duration'},
                        {'col': 'summary', 'key': 'verbose', 'verbose': 'Status'}
                    ]
                },
                'measurements': [
                    {
                        'title': 'Programing steps',
                        'doc': 'Duration of each programming step',
                        'type': 'table',
                        'point_names': 'timer',
                        'columns': [
                            {'key': 's', 'verbose': 'Programming stage'},
                            {'key': 'v', 'verbose': 'Duration (s)'},
                        ]
                    }
                ],
                'doc': 'The ATCA Programmer subtest configures the FPGA logic and its embedded flash memory for the SoC firmware.'
            },
            {
                'test_type_name': 'ATCA_v.1.0',
                'title': 'ATCA functions',
                'summary': {
                    'type': 'table', #Only table is supported for subtest display
                    'columns': [
                        {'col': 'details', 'key': 'hardware_address', 'verbose': 'ATCA Hardware address'},
                        {'col': 'details', 'key': 'timeout', 'verbose': 'Timeout for atca function'},
                        {'col': 'summary', 'key': 'duration', 'verbose': 'Duration (s)'},
                        {'col': 'summary', 'key': 'verbose', 'verbose': 'Status'}
                    ]
                },
                'measurements': [
                    {
                        'title': 'ATCA steps',
                        'doc': 'Duration of each ATCA testing step',
                        'type': 'table',
                        'point_names': 'timer',
                        'columns': [
                            {'key': 's', 'verbose': 'Testing step'},
                            {'key': 'v', 'verbose': 'Duration (s)'},
                        ]
                    }
                ],
                'doc': 'The ATCA subtest restarts the ATCA blade and checks the functionality of the required pins.'
            },
            {
                'test_type_name': 'LAN_v.1.0',
                'title': 'LAN configuration',
                'summary': {
                    'type': 'table', #Only table is supported for subtest display
                    'columns': [
                        {'col': 'details', 'key': 'production_number', 'verbose': 'Production number'},
                        {'col': 'details', 'key': 'serial_number', 'verbose': 'Serial number'},
                        {'col': 'summary', 'key': 'mac_address', 'verbose': 'MAC address'},
                        {'col': 'summary', 'key': 'verbose', 'verbose': 'Status'}
                    ]
                },
                'doc': 'The LAN configuration test check the lan connection and set the MAC address.'
            },
            {
                'test_type_name': 'AMC_v.1.0',
                'title': 'AMC support',
                'summary': {
                    'type': 'table', #Only table is supported for subtest display
                    'columns': [
                        {'col': 'details', 'key': 'slot', 'verbose': 'AMC Slot'},
                        {'col': 'details', 'key': 'target_addr', 'verbose': 'AMC geographical address'},
                        {'col': 'summary', 'key': 'duration', 'verbose': 'Duration (s)'},
                        {'col': 'summary', 'key': 'verbose', 'verbose': 'Status'}
                    ]
                },
                'measurements': [
                    {
                        'title': 'AMC steps',
                        'doc': 'Duration of each AMC testing step',
                        'type': 'table',
                        'point_names': 'timer',
                        'columns': [
                            {'key': 's', 'verbose': 'Testing step'},
                            {'key': 'v', 'verbose': 'Duration (s)'},
                        ]
                    }
                ],
                'doc': 'The AMC slot subtest restarts the ATCA blade and checks the AMC port functionality.'
            },
            {
                'test_type_name': 'IO_v.1.0',
                'title': 'I/O pins',
                'summary': {
                    'type': 'table', #Only table is supported for subtest display
                    'columns': [
                        {'col': 'details', 'key': 'io', 'verbose': 'IO id'},
                        {'col': 'summary', 'key': 'duration', 'verbose': 'Duration (s)'},
                        {'col': 'summary', 'key': 'verbose', 'verbose': 'Status'}
                    ]
                },
                'measurements': [
                    {
                        'title': 'AMC steps',
                        'doc': 'Duration of each AMC testing step',
                        'type': 'table',
                        'point_names': 'get_io',
                        'columns': [
                            {'key': 's', 'verbose': 'Testing step'},
                            {'key': 'v', 'verbose': 'Value'},
                        ]
                    }
                ],
                'doc': 'The I/O subtest restarts the ATCA blade and checks the functionality of the user I/Os.'
            },
            {
                'test_type_name': 'I2CMGT_v.1.0',
                'title': 'Management i2c bus',
                'summary': {
                    'type': 'table', #Only table is supported for subtest display
                    'columns': [
                        {'col': 'details', 'key': 'count', 'verbose': 'Number of rd/wr'},
                        {'col': 'summary', 'key': 'good_transactions', 'verbose': 'Number of good transaction(s)'},
                        {'col': 'summary', 'key': 'bad_transactions', 'verbose': 'Number of bad transaction(s)'},
                        {'col': 'summary', 'key': 'register_tested', 'verbose': 'Number of register(s) tested'},
                        {'col': 'summary', 'key': 'duration', 'verbose': 'Duration (s)'}
                    ]
                },
                'measurements': [
                    {
                        'type': 'scatter',
                        'label': 'EEPROM registers',
                        'title': 'Register tested',
                        'doc': 'During the mgt i2c test, different register of the eeprom are accessed in write and read mode.',
                        'point_names': 'memory_i2c',
                        'ykey': 'addr'
                    }
                ],
                'doc': 'The MGT I2C subtest validate the functionality of the MGT i2c bus located on the edge connector.'
            },
            {
                'test_type_name': 'I2CSENS_v.1.0',
                'title': 'Sensor i2c bus',
                'summary': {
                    'type': 'table', #Only table is supported for subtest display
                    'columns': [
                        {'col': 'summary', 'key': 'tester_temp', 'verbose': 'Tester temperature'},
                        {'col': 'summary', 'key': 'sensor_delta', 'verbose': 'Onboard/tester temp. delta'},
                        {'col': 'summary', 'key': 'duration', 'verbose': 'Duration (s)'},
                        {'col': 'summary', 'key': 'verbose', 'verbose': 'Status'}
                    ]
                },
                'measurements': [
                    {
                        'title': 'Sensor test',
                        'doc': 'Duration sensor test, the tester and mezzanine card temperature are measured.',
                        'type': 'table',
                        'point_names': 'temperature',
                        'columns': [
                            {'key': 's', 'verbose': 'Sensor'},
                            {'key': 'v', 'verbose': 'Value'}
                        ]
                    }
                ],
                'doc': 'The Sensor I2C subtest validates the functionality of the Sensor i2c bus located on the edge connector.'
            }
        ]
    }


def fix_hardware_address(ha):
    # mask the parity bit
    ha &= 0x7f
    # calculate parity
    par = ha ^ (ha >> 4);
    par = par ^ (par >> 2);
    par = par ^ (par >> 1);
    # insert the odd parity bit
    ha |= (~par & 1) << 7 
    return ha

if __name__ == "__main__":
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('-h', '--help', action='store_true', required=False)
    parser.add_argument('-o', '--output', required=False)
    parser.add_argument('-a', '--address', required=False)
    parser.add_argument('-p', '--program', action='store_true', required=False)
    parser.add_argument('-u', '--umgt', action='store', required=False, default='/dev/ipmc-umgt')
    parser.add_argument('-i', '--ipmc_ip', action='store', required=False, default='192.168.1.34')
    parser.add_argument('-b', '--batch_name', action='store', required=False)
    parser.add_argument('-s', '--serial_number', action='store', required=False)
    parser.add_argument('-m', '--mac_address', action='store', required=False)
    parser.add_argument('-P', '--program_user', action='store_true', required=False)
    parser.add_argument('-U', '--uart_devices', action='store', type=int, required=False)
    parser.add_argument('-d', '--debug_level', action='store', type=int, required=False)

    args = parser.parse_args()

    if args.help:
        print('Usage: ipmc_test -a, --address        hardware address (hexadecimal, default: 0x41)')
        print('                 -p, --program        program the IPMC test firmware (for testing)')
        print('                 -o, --output         enable test result output to file')
        print('                 -u, --umgt           uMGT serial port (default: /dev/ipmc-umgt; if empty then search)')
        print('                 -i, --ipmc_ip        IPMC IP address (default: 192.168.1.34)')
        print('                 -P, --program_user   program the IPMC user firmware (for shipping)')
        print('                 -U, --uart_devices   UART devices to be tested (0=none, 1=payload, 2=debug, default: 3=both)')
        print('                 -b, --batch_name     IPMC batch name (9 chars, e.g. 10184002A)')
        print('                 -s, --serial_number  IPMC serial number (8 digits, e.g. 24020270)')
        print('                 -m, --mac_address    IPMC MAC address (6 bytes, e.g. 80:d3:36:00:43:60)')
        print('                 -d, --debug_level    debug level (default: 0=no debug)')
        print('                 -h, --help           print this help and exit')
        exit(0)

    kwargs = {}

    # enable debug output
    if args.debug_level:
        kwargs['debug_level'] = args.debug_level

    # uMGT serial port`
    kwargs['umgt_port'] = '/dev/ipmc-umgt'
    if args.umgt:
        umgt = args.umgt
        if not os.path.exists(umgt):
            print('[Error] uMGT serial port \"{}\" does not exist.'.format(umgt))
            exit(-1)
        kwargs['umgt_port'] = umgt

    # hardware address
    ha_address = 0x41
    if args.address:
        ha_address = int(args.address, 16)
    # fix H/W address parity bit
    ha_address = fix_hardware_address(ha_address)

    # IPMC hostname
    hostname = '192.168.1.34'
    if args.ipmc_ip:
        hostname = args.ipmc_ip

    # CONFIG parameters
    #print(args)
    if args.batch_name:
        kwargs['batch_name'] = args.batch_name
    if args.serial_number:
        kwargs['serial_number'] = args.serial_number
    if args.mac_address:
        kwargs['mac_address'] = args.mac_address
    if args.program_user:
        kwargs['program_user'] = args.program_user
    kwargs['uart_devices'] = 3
    if args.uart_devices is not None:
        kwargs['uart_devices'] = args.uart_devices

    print(f'[Info] {datetime.now().strftime("%d-%^b-%Y %H:%M:%S")}, starting test ...')
    print(f'[Info] H/W address = 0x{ha_address:02x}')

    #sysapi.set_verbosity(sysapi.SET_NO_VERBOSITY)

    r = cernipmc_test_seq(['unknown','unknown'], ha_address, hostname, args.program, False, **kwargs)

    if args.output:
        pickle.dump(r, open(args.output,"wb"))
        print('[Info] Output saved: {}'.format(args.output))

