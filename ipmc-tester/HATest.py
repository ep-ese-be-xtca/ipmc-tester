'''
The HA subtest checks the functionality of the IPMC hardware address.

The pass value depends on the test result:
    *  0: Success
    * -1: Error
'''

#!/usr/bin/env python
# encoding: utf-8
import time
import pprint

import lib.sysapi as sysapi

from IPMCDevLib import IPMCDevCom as IPMCDevCom
from IPMCDevLib import IPMCDevATCA as IPMCDevATCA

import lib.IPMI as IPMI

@sysapi.register_debug('HA Subtest')
def test_ha(hai, hostname='192.168.1.34', timeout=100, **kwargs):
    '''
    The HA subtest checks the functionality of the IPMC hardware address.

    Args:
        hai: hardware address: a list of hardware addresses to be tested
        timeout: Maximum time to wait for a function to be executed

    Returns:
        The function returns a list of dictionary formatted to be used in the test instance.
        Each instance of the list is one subtest, linked to a specific HA.

        .. code-block:: javascript

            [{
                'test_type_name': 'HA_v.1.0',
                'name': 'ha_tester_{}'.format(ha),

                'details': {
                    'ha': ha,
                    'timeout': timeout
                },

                'summary': {
                    'verbose': verbose,
                    'duration': round((time.time()-start),2)
                },

                'measurements': measurements,
                'pass': passed
            }, ...]

        Where pass can be:
            *  0: success
            * -1: error
    '''

    debug_level = kwargs['debug_level'] if 'debug_level' in kwargs else 0
    IPMCDevObject = IPMCDevCom.IPMCDevCom(**kwargs)
    IPMCDevATCAObject = IPMCDevATCA.IPMCDevATCA(IPMCDevObject)
    ipmiLan = IPMI.IPMI(hostname=hostname, debug = 1 if debug_level >= 10 else 0)

    if isinstance(hai, list):
        ha_list = hai
    else:
        ha_list = [int(hai)]

    timeout = int(timeout)

    start = time.time()

    subtests_ret = []

    subtests = []

    for ha in ha_list:
        measurements = []
        passed = 0
        verbose = 'Test successfully passed'
        print(f'[Info] HA {ha:02x}h')

        # set HA
        r = IPMCDevATCAObject.SETHA(ha)
        if r != 0:
            print(f'[ERROR] Setting hardware address: {ha}')
        r = IPMCDevATCAObject.GETHA()
        ha_set = int(r,16) # read from hex string
        if ha_set != ha:
            print(f'[ERROR] Setting hardware address: wrong address {ha_set} - expected {ha}')

        # reset IPMC
        print('[Info] Reset the IPMC - wait for IPMC to be ready ...')
        try:
            ipmiLan.ipmc_reset()
        except Exception as e:
            print(f'[ERROR] Resetting IPMC: {str(e)}')

        try:
            r = ipmiLan.ipmc_read_ha()
            ha_read = int(r)
        except Exception as e:
            print(f'[ERROR] Reading hardware address: {str(e)}')

        # compare HA (without odd-parity bit)
        if ha_read != (ha & 0x7f):
            measurements.append({'name': 'read_ha', 'data': {'s': 'Hardware address', 'v': hex(ha_read)}, 'pass': -1 })
            passed = -1
            verbose = f'HA incorrect: {hex(ha)} expected - {hex(ha_read)} read'
            print(f'[ERROR ] {verbose}')

        print(f'[Info] IPMC hardware address: {hex(ha)}')
        if passed == 0:
            measurements.append({'name': 'read_ha', 'data': {'s': 'Hardware address', 'v': hex(ha_read)}, 'pass': 0 })

        subtests.append({ 'test_type_name': 'HA_v.1.0',
                          'name': 'ha_tester_{}'.format(ha),
                          'details': { 'ha': ha,
                                       'timeout': timeout
                          },
                          'summary': { 'verbose': verbose,
                                       'duration': round((time.time()-start),2)
                          },
                          'measurements': measurements,
                          'pass': passed
        })

    return subtests

if __name__ == "__main__":
    ha_list = [ 0xc1 ]
    pprint.pprint(test_ha(list(ha_list), timeout = 100))
