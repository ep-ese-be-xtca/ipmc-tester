#!/usr/bin/python3
# encoding: utf-8

import pprint
import time

import lib.sysapi as sysapi

from IPMCDevLib import IPMCDevCom as IPMCDevCom
from IPMCDevLib import IPMCDevMgt as IPMCDevMgt

# script for setting IPMC identifiers: MAC address, serial number, board number

import argparse
import json
import lib.IPMI as IPMI
import lib.IPMCID as IPMCID

# ---- helper for repeating function to times until success --------------------

def attempt_function(to, func, *args, **kwargs):
    for i in range(to):
        try:
            rtnv = func(*args, **kwargs)
        except:
            if (i + 1) >= to:
                raise
            else:
                pass
        else:
            return rtnv

# ---- CONFIG TEST -------------------------------------------------------------

@sysapi.register_debug('CONFIG Subtest')
def config_test(**kwargs):

    if kwargs.get('hostname') is None:
        raise Exception('\"hostname\" not set')
    else:
        hostname = kwargs.get('hostname')

    if kwargs.get('batch_name') is None:
        config_batch_name = False
    else:
        config_batch_name = True
        batch_name = kwargs.get('batch_name')

    if kwargs.get('serial_number') is None:
        config_serial_number = False
    else:
        config_serial_number = True
        serial_number = int(kwargs.get('serial_number'),10)

    if kwargs.get('mac_address') is None:
        config_mac_address = False
    else:
        config_mac_address = True

    if kwargs.get('debug_level') is None:
        debug_level = 0
    else:
        debug_level = kwargs.get('debug_level')

    if kwargs.get('program_user') is None:
        program_user = False
    else:
        program_user = kwargs.get('program_user')

    # default parameters - depending on firmware
    def_user_firmware_file = '../ipmc-firmware/user-firmware/hpm1all.img'
    def_lan_channel = 5
    def_timeout = 5

    ipmi = IPMI.IPMI(hostname=hostname, username='', password='')
    ipmcid = IPMCID.IPMCID(ipmi)
    nerr = 0
    measurements = []
    start = time.time()

    if config_mac_address:
        mac_address = ipmcid.mac_string_to_number(kwargs.get('mac_address'))

    # write BATCH name (EEPROM)
    if config_batch_name:
        if debug_level >= 1:
            print(f'[Info] BATCH  (EEPROM) to be written: {batch_name}')
        ipmcid.batch_name_write(batch_name)

    # write SERIAL number (EEPROM)
    if config_serial_number:
        if debug_level >= 1:
            print(f'[Info] SERIAL (EEPROM) to be written: {ipmcid.serial_number_to_string(serial_number)}')
        ipmcid.serial_number_write(serial_number)

    # write MAC address (EEPROM)
    if config_mac_address:
        if debug_level >= 1:
            print(f'[Info] MAC    (EEPROM) to be written: {ipmcid.mac_number_to_string(mac_address)}')
        ipmcid.mac_number_write(mac_address)

    # program user firmware for shipping or reset
    if program_user:
        # program user firmware
        print(f'[Info] programming user firmware: {def_user_firmware_file}')
        ipmi.ipmi_hpm_upgrade(def_user_firmware_file)
        ipmi.ipmi_hpm_activate()
    else:
        # do a management controller reset
        print(f'[Info] resetting IPMC firmware')
        ipmi.ipmc_reset()

    # read BATCH name (EEPROM)
    s_out = attempt_function(def_timeout, ipmcid.batch_name_read, )
    if debug_level >= 1:
        print(f'[Info] BATCH  (EEPROM) read:          {s_out}')
    if config_batch_name and s_out != batch_name:
        print(f'[Error] BATCH name DIFFERENT!')
        nerr += 1

    # read SERIAL number (EEPROM)
    n_out = ipmcid.serial_number_read()
    if debug_level >= 1:
        print(f'[Info] SERIAL (EEPROM) read:          {ipmcid.serial_number_to_string(n_out)}')
    if config_serial_number and n_out != serial_number:
        print(f'[Error] SERIAL number DIFFERENT!')
        nerr += 1

    # read MAC address (EEPROM)
    n_out = ipmcid.mac_number_read()
    if debug_level >= 1:
        print(f'[Info] MAC    (EEPROM) read:          {ipmcid.mac_number_to_string(n_out)}')
    if config_mac_address and n_out != mac_address:
        print(f'[Error] MAC address (EEPROM) DIFFERENT!')
        nerr += 1

    # read MAC address (LAN)
    lan = ipmi.ipmi_lan_read(channel=def_lan_channel)
    n_out = 0
    if 'MAC Address' in lan:
        n_out = ipmcid.mac_string_to_number(lan['MAC Address'])
    if debug_level >= 1:
        print(f'[Info] MAC    (LAN) read:             {ipmcid.mac_number_to_string(n_out)}')
    if config_mac_address and n_out != mac_address:
        print(f'[Error] MAC address (LAN) DIFFERENT!')
        nerr += 1

    # write BATCH name (FRU)
    if config_batch_name:
        if debug_level >= 2:
            print(f'[Info] BATCH  (FRU) to be written:    {batch_name}')
        ipmi.ipmi_fru_write(0, 'p', 2, batch_name)

    # read BATCH name (FRU)
    fru = ipmi.ipmi_fru_read()
    bn_out = fru['Product Part Number']
    if debug_level >= 1:
        print(f'[Info] BATCH  (FRU) read:             {bn_out}')
    if config_batch_name:
        if bn_out != batch_name:
            print(f'[Error] BATCH (FRU) name DIFFERENT!')
            measurements.append({ 'name': 'batch_name_fru', 'data': { 'batch_name': batch_name}, 'pass': -1 })
            nerr += 1
        else:
            measurements.append({ 'name': 'batch_name_fru', 'data': { 'batch_name': batch_name}, 'pass': 0 })

    # write SERIAL number (FRU)
    if config_serial_number:
        s = ipmcid.serial_number_to_string(serial_number)
        if debug_level >= 2:
            print(f'[Info] SERIAL (FRU) to be written:    {s}')
        ipmi.ipmi_fru_write(0, 'p', 4, s)

    # read SERIAL number (FRU)
    fru = ipmi.ipmi_fru_read()
    sn_out = fru['Product Serial']
    if debug_level >= 1:
        print(f'[Info] SERIAL (FRU) read:             {sn_out}')
    if config_serial_number:
        s = ipmcid.serial_number_to_string(serial_number)
        if sn_out != s:
            print(f'[Error] SERIAL (FRU) number DIFFERENT!')
            measurements.append({ 'name': 'serial_number_fru', 'data': { 'serial_number': s}, 'pass': -1 })
            nerr += 1
        else:
            measurements.append({ 'name': 'serial_number_fru', 'data': { 'serial_number': s}, 'pass': 0 })

    # ----- RESULTS ------------------------------------------------------------
    if nerr != 0:
        print(f'[Error] THERE WERE {nerr} ERROR(S)!')
        exitCode = -1
        exitVerbose = "There were errors"
    else:
        exitCode = 0
        exitVerbose = "Success"

    if 'output_filename' in locals():
        with open(output_filename, 'w') as outfile:
            json.dump({'batch_name':batch_name,
                       'serial_number':serial_number_to_string(serial_number),
                       'mac_address':mac_number_to_string(mac_address)},
                       outfile)
            outfile.write('\n')
            outfile.close()
        print(f'[Info] written identifiers to file \'{output_filename}\'')

    ipmi.close()

    return {
        'test_type_name': 'CONFIG',
        'name': 'config_test',
        'details': {
        },
        'summary': {
            'exitcode': exitCode,
            'verbose': exitVerbose,
            'duration': round((time.time()-start),2)
        },
        'measurements': measurements,
        'pass': exitCode
    }

if __name__ == "__main__":
    pprint.pprint(config_test())
