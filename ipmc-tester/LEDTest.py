'''
The LED subtest checks the functionality of the IPMC FUR LEDs.

The pass value depends on the test result:
    *  0: Success
    * -1: Error
'''

#!/usr/bin/env python
# encoding: utf-8
import time
import pprint

import lib.sysapi as sysapi

from IPMCDevLib import IPMCDevCom as IPMCDevCom
from IPMCDevLib import IPMCDevATCA as IPMCDevATCA

import lib.IPMI as IPMI

@sysapi.register_debug('LED Subtest')
def test_led(hostname='192.168.1.34', timeout=100, **kwargs):
    '''
    The LED subtest checks the functionality of the IPMC FRU LEDs.

    Args:
        timeout: Maximum time to wait for a function to be executed

    Returns:
        The function returns a list of dictionary formatted to be used in the test instance.
        Each instance of the list is one subtest, linked to a specific LED.

        .. code-block:: javascript

            [{
                'test_type_name': 'LED_v.1.0',
                'name': 'led_tester_{}'.format(led),

                'details': {
                    'led': led,
                    'timeout': timeout
                },

                'summary': {
                    'verbose': verbose,
                    'duration': round((time.time()-start),2)
                },

                'measurements': measurements,
                'pass': passed
            }, ...]

        Where pass can be:
            *  0: success
            * -1: error
    '''

    debug_level = kwargs['debug_level'] if 'debug_level' in kwargs else 0

    IPMCDevObject = IPMCDevCom.IPMCDevCom(**kwargs)
    IPMCDevATCAObject = IPMCDevATCA.IPMCDevATCA(IPMCDevObject)
    ipmiLan = IPMI.IPMI(hostname=hostname, debug = 1 if debug_level >= 10 else 0)

    timeout = int(timeout)

    start = time.time()

    subtests_ret = []

    led_state_on = { 'off-duration':0, 'on-duration':1, 'color':1, 'off-first':0 }
    led_state_off = { 'off-duration':1, 'on-duration':0, 'color':1, 'off-first':0 }

    subtests = []

    fru = 0
    led_color = [ 1, 2, 3, 5 ]

    for led in range(4):
        measurements = []
        passed = 0
        verbose = 'Test successfully passed'
        print(f'[Info] LED #{led}')

        # set LED - ON
        try:
            led_state_on['color'] = led_color[led]
            r = ipmiLan.ipmc_write_led(fru, led, led_state_on)
        except Exception as e:
            measurements.append({'name': 'led_read', 'data': {'s': 'write LED ON error', 'v': led_read}, 'pass': -1 })
            passed = -1
            verbose = f'Writing LED #{led} ON: {str(e)}'
            print(f'[ERROR] {verbose}')
        try:
            led_state_read = ipmiLan.ipmc_read_led(fru, led)
        except Exception as e:
            measurements.append({'name': 'led_read', 'data': {'s': 'read LED ON error', 'v': led_read}, 'pass': -1 })
            passed = -1
            verbose = f'Reading LED #{led} ON: {str(e)}'
            print(f'[ERROR] {verbose}')
        if led_state_read != led_state_on:
            measurements.append({'name': 'led_read', 'data': {'s': 'read LED ON - different state', 'v': led_read}, 'pass': -1 })
            passed = -1
            verbose = f'Reading LED #{led} ON: different state {led_state_read}'
            print(f'[ERROR] {verbose}')

        # get LED state and compare
        led_read = IPMCDevATCAObject.GETLED(led)
        if led_read < 0:
            print(f'[ERROR] Reading LED #{led}: r = {led_read}')
        if led_read != 1:
            measurements.append({'name': 'led_read', 'data': {'s': 'get LED ON - wrong reading', 'v': led_read}, 'pass': -1 })
            passed = -1
            verbose = f'LED #{led}: ON expected - OFF read'
            print(f'[ERROR ] {verbose}')

        # set LED - OFF
        try:
            led_state_off['color'] = led_color[led]
            r = ipmiLan.ipmc_write_led(fru, led, led_state_off)
        except Exception as e:
            measurements.append({'name': 'led_read', 'data': {'s': 'write LED OFF error', 'v': led_read}, 'pass': -1 })
            passed = -1
            verbose = f'Writing LED #{led} OFF: {str(e)}'
            print(f'[ERROR] {verbose}')
        try:
            led_state_read = ipmiLan.ipmc_read_led(fru, led)
        except Exception as e:
            measurements.append({'name': 'led_read', 'data': {'s': 'read LED OFF error', 'v': led_read}, 'pass': -1 })
            passed = -1
            verbose = f'Reading LED #{led} OFF: {str(e)}'
            print(f'[ERROR] {verbose}')
        if led_state_read != led_state_off:
            measurements.append({'name': 'led_read', 'data': {'s': 'read LED OFF - different state', 'v': led_read}, 'pass': -1 })
            passed = -1
            verbose = f'Reading LED #{led} OFF: different state {led_state_read}'
            print(f'[ERROR] {verbose}')

        # get LED state and compare
        led_read = IPMCDevATCAObject.GETLED(led)
        if led_read < 0:
            print(f'[ERROR] Reading LED #{led} (r = {led_read})')
        if led_read != 0:
            measurements.append({'name': 'led_read', 'data': {'s': 'get LED OFF - wrong reading', 'v': led_read}, 'pass': -1 })
            passed = -1
            verbose = f'LED #{led}: OFF expected - ON read'
            print(f'[ERROR ] {verbose}')

        if passed == 0:
            measurements.append({'name': 'read_led', 'data': {'s': 'LED test', 'v': 0}, 'pass': 0 })

        subtests.append({ 'test_type_name': 'LED_v.1.0',
                          'name': 'led_tester_{}'.format(led),
                          'details': { 'led': led,
                                       'timeout': timeout
                          },
                          'summary': { 'verbose': verbose,
                                       'duration': round((time.time()-start),2)
                          },
                          'measurements': measurements,
                          'pass': passed
        })

    return subtests

if __name__ == "__main__":
    pprint.pprint(test_led(), timeout = 100)
