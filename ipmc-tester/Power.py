'''
The power check test measures the IPMC tester voltage and CERN-IPMC power to ensure the functionnality of the
module.

The pass value depends on the test result:
    *  0: success
    * -1: Tester voltage is out of range (3.1V < x < 3.4V)
    * -2: IPMC voltage is out of range (3.1V < x < 3.4V)
    * -3: IPMC current is out of range (x < 0.3A)
'''

#!/usr/bin/env python
# encoding: utf-8
import pprint
import time

import lib.sysapi as sysapi

from IPMCDevLib import IPMCDevCom as IPMCDevCom
from IPMCDevLib import IPMCDevMgt as IPMCDevMgt

@sysapi.register_debug('Power check')
def power_check(measurement_count, **kwargs):
    '''
    The program function read the STAPL file passed as input and execute the program action
    using the jtag-player tool. This binary was modify to report messages encoded in json,
    allowing the programing test to get status using the stdout output. Therefore, the test
    get the progress and reports the final status.

    Args:
        file_path: STAPL file to be programmed

    Returns:
        The function returns a dictionary formated to be used in the test instance.

        .. code-block:: javascript

            {
                'test_type_name': 'POWER_v.1.0',
                'name': 'power_meas',

                'details': {
                    'measurement_count': measurement_count
                },

                'summary': {
                    'exitcode': exitCode,
                    'verbose': exitVerbose,
                    'tester_voltage':testerVoltage,
                    'ipmc_voltage': ipmcVoltage,
                    'ipmc_current': ipmcCurrent,
                    'duration': round((time.time()-start),2)
                },

                'measurements': measurements,
                'pass': exitCode
            }

        Where pass can be:
            *  0: success
            * -1: Tester voltage is out of range (3.1V < x < 3.4V)
            * -2: IPMC voltage is out of range (3.1V < x < 3.4V)
            * -3: IPMC current is out of range (x < 0.3A)
    '''
    IPMCDevObject = IPMCDevCom.IPMCDevCom(**kwargs)
    IPMCDevMgtObject = IPMCDevMgt.IPMCDevMgt(IPMCDevObject)

    start = time.time()

    measurements = []
    testerVoltages = []
    ipmcVoltages = []
    ipmcCurrents = []

    for i in range(int(measurement_count)):
        testerVoltage = IPMCDevMgtObject.GETMGTVOLTAGE()
        ipmcVoltage = IPMCDevMgtObject.GETIPMCVOLTAGE()
        ipmcCurrent = IPMCDevMgtObject.GETIPMCCURRENT()

        measPassed = 0
        if testerVoltage > 3.3 * 1.05 or testerVoltage < 3.3 * 0.95:
            measPassed = -1

        if ipmcVoltage > 3.3 * 1.05 or ipmcVoltage < 3.3 * 0.95:
            measPassed = -2

        if ipmcCurrent > 0.15:
            measPassed = -3

        measurements.append({
                'name': 'power_meas',
                'pass': measPassed,
                'data': {
                        'tester_voltage': testerVoltage,
                        'ipmc_voltage': ipmcVoltage,
                        'ipmc_current': ipmcCurrent,
                        'ipmc_power': (ipmcVoltage*ipmcCurrent)
                    }
            })

        testerVoltages.append(testerVoltage)
        ipmcVoltages.append(ipmcVoltage)
        ipmcCurrents.append(ipmcCurrent)

    testerVoltage = float(sum(testerVoltages))/float(len(testerVoltages))
    ipmcVoltage = float(sum(ipmcVoltages))/float(len(ipmcVoltages))
    ipmcCurrent = float(sum(ipmcCurrents))/float(len(ipmcCurrents))

    print(f"[Info] Tester voltage: {testerVoltage:.3f} Volts")
    print(f"[Info] IPMC Voltage: {ipmcVoltage:.3f} Volts")
    print(f"[Info] IPMC Current: {ipmcCurrent:.3f} Amps")

    exitCode = 0
    exitVerbose = "Success"

    if testerVoltage > 3.4 or testerVoltage < 3.1:
        exitCode = -1
        exitVerbose = "Tester voltage is out of range"

    if ipmcVoltage > 3.4 or ipmcVoltage < 3.1:
        exitCode = -2
        exitVerbose = "IPMC voltage is out of range"

    if ipmcCurrent > 0.3:
        exitCode = -3
        exitVerbose = "IPMC current is out of range"

    IPMCDevObject.close()

    return {
        'test_type_name': 'POWER_v.1.0',
        'name': 'power_meas',

        'details': {
            'measurement_count': int(measurement_count)
        },

        'summary': {
            'exitcode': exitCode,
            'verbose': exitVerbose,
            'tester_voltage':testerVoltage,
            'ipmc_voltage': ipmcVoltage,
            'ipmc_current': ipmcCurrent,
            'duration': round((time.time()-start),2)
        },

        'measurements': measurements,
        'pass': exitCode
    }

if __name__ == "__main__":
    pprint.pprint(power_check(20))
