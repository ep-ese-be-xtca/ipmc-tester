#!/usr/bin/env python

'''
The AMC slot subtest restarts the ATCA blade and checks the AMC port functionality.
Therefore, it emulates an AMC card insertion, followed by PP issue and MP issue.
After executing the command, it checks the status of the setup and reports the time
required to be in the expected state.

The pass value depends on the test result:
    *  0: success (stop at "**End**")
    * -1: ATCA init failed (stop at "**Reset ATCA (using HA=0x00)**")
    * -2: AMC init failed (stop at "**Wait for 12V Enable**")
    * -3: PP de-assertion failed (stop at "**De-assert PP Good**")
    * -4: MP de-assertion failed (stop at "**De-assert MP Good**")
'''

# encoding: utf-8
import time
import pprint

import lib.sysapi as sysapi

from IPMCDevLib import IPMCDevCom as IPMCDevCom
from IPMCDevLib import IPMCDevATCA as IPMCDevATCA
from IPMCDevLib import IPMCDevAMC as IPMCDevAMC

@sysapi.register_debug('AMC Subtest')
def amc_tester(slot, target_addr, timeout = 100, **kwargs):
    '''
    The AMC debug functions restart the ATCA blade and checks the AMC port functionality.
    Therefore, it emulates an AMC card insertion, followed by PP issue and MP issue.
    After executing the command, it checks the status of the setup and reports the time
    required to be in the expected state.

    When the timeout value is exceeded for a specific function, the test is considered to
    be failed.

    Args:
        slot: Physical slot value from 0 to 8 (can be an integer or an array)
        target_addr: AMC address connected to this slot (can be an integer or an array)
        timeout: Maximum time to wait for a function to be executed

    Returns:
        The function returns a dictionary formated to be used in the test instance.

        .. code-block:: javascript

            {
                'test_type_name': 'AMC_v.1.0',
                'name': 'amc_port_{}'.format(slot),

                'details': {
                    'slot': slot,
                    'target_addr': target_addr,
                    'timeout': timeout
                },

                'summary': {
                    'verbose': str(e),
                    'duration': round((time.time() - start),2)
                },

                'measurements': measurements,
                'pass': pass
            }

        Where pass can be:
            *  0: success
            * -1: ATCA init failed
            * -2: AMC init failed
            * -3: PP de-assertion failed
            * -4: MP de-assertion failed
    '''

    IPMCDevObject = IPMCDevCom.IPMCDevCom(**kwargs)
    IPMCDevATCAObject = IPMCDevATCA.IPMCDevATCA(IPMCDevObject)
    IPMCDevAMCObject = IPMCDevAMC.IPMCDevAMC(IPMCDevObject)

    if isinstance(target_addr, list):
        target_addr_list = target_addr
        slot_list = slot

    else:
        target_addr_list = [int(target_addr)]
        slot_list = [int(slot)]

    timeout = int(timeout)

    start = time.time()

    #Extract AMC
    IPMCDevAMCObject.OPENHSW()
    IPMCDevAMCObject.EXTRACTAMC()

    subtests_ret = []

    for i in range(0, len(target_addr_list)):

        t_addr = target_addr_list[i]
        slt = slot_list[i]
        start = time.time()

        print('[Info] Test AMC slot {} (GA: {:02x}h)'.format(slt, t_addr))

        measurements = []

        #measurements.append({'name': 'timer', 'data': {'s': 'ATCA power off', 'v': pwr_off_timeout}, 'pass': 0 })
        #measurements.append({'name': 'timer', 'data': {'s': 'ATCA power on', 'v': pwr_on_timeout}, 'pass': 0 })

        #Select AMC and set Geographical address
        IPMCDevAMCObject.SELAMC(slt)
        IPMCDevAMCObject.SETGA(t_addr)

        #Assert MP/PP good
        IPMCDevAMCObject.ASSERTMPGOOD()
        IPMCDevAMCObject.ASSERTPPGOOD()

        #Emulate AMC insertion
        IPMCDevAMCObject.INSERTAMC()

        print('[Info] AMC card inserted')

        #Wait for MMC to be initialized
        time.sleep(2)

        #Close AMC handle switch
        IPMCDevAMCObject.CLOSEHSW()

        #Wait for AMC to be ON
        amc_on_timeout = 0
        while IPMCDevAMCObject.GETPPEN() != 1 and amc_on_timeout < timeout:
            time.sleep(0.1)
            amc_on_timeout = amc_on_timeout + 1

        if amc_on_timeout >= timeout:
            IPMCDevAMCObject.EXTRACTAMC()
            IPMCDevObject.close()

            subtests_ret.append({
                'test_type_name': 'AMC_v.1.0',
                'name': 'amc_port_{}'.format(slt),

                'details': {
                    'slot': slt,
                    'target_addr': t_addr,
                    'timeout': timeout
                },

                'summary': {
                    'verbose': 'AMC remains off after initialization',
                    'duration': round((time.time()-start),2)
                },

                'measurements': measurements,
                'pass': -2
            })

            break

        print('[Info] AMC card turned ON')

        measurements.append({'name': 'timer', 'data': {'s': 'AMC power on', 'v': amc_on_timeout}, 'pass': 0 })

        #Emulate 12V error
        IPMCDevAMCObject.DEASSERTPPGOOD()

        pperr_timeout = 0
        while IPMCDevAMCObject.GETPPEN() != 0 and pperr_timeout < timeout:
            time.sleep(0.1)
            pperr_timeout = pperr_timeout + 1

        if pperr_timeout >= timeout:
            IPMCDevAMCObject.EXTRACTAMC()
            IPMCDevObject.close()

            subtests_ret.append({
                'test_type_name': 'AMC_v.1.0',
                'name': 'amc_port_{}'.format(slt),

                'details': {
                    'slot': slt,
                    'target_addr': t_addr,
                    'timeout': timeout
                },

                'summary': {
                    'verbose': 'PP remains ON after PP failure',
                    'duration': round((time.time()-start),2)
                },

                'measurements': measurements,
                'pass': -3
            })

            break

        print('[Info] Payload power issue well detected')

        measurements.append({'name': 'timer', 'data': {'s': 'PPGood de-assertion', 'v': pperr_timeout}, 'pass': 0 })

        #Emulate 3.3V error
        IPMCDevAMCObject.DEASSERTMPGOOD()

        mperr_timeout = 0
        while IPMCDevAMCObject.GETMPEN() != 0 and mperr_timeout < timeout:
            time.sleep(0.1)
            mperr_timeout = mperr_timeout + 1

        if mperr_timeout >= timeout:
            IPMCDevAMCObject.EXTRACTAMC()
            IPMCDevObject.close()

            subtests_ret.append({
                'test_type_name': 'AMC_v.1.0',
                'name': 'amc_port_{}'.format(slt),

                'details': {
                    'slot': slt,
                    'target_addr': t_addr,
                    'timeout': timeout
                },

                'summary': {
                    'verbose': 'MP remains ON after MP failure',
                    'duration': round((time.time()-start),2)
                },

                'measurements': measurements,
                'pass': -4
            })

            break

        print('[Info] Management power issue well detected')

        measurements.append({'name': 'timer', 'data': {'s': 'MPGood de-assertion', 'v': mperr_timeout}, 'pass': 0 })

        #Extract AMC
        IPMCDevAMCObject.EXTRACTAMC()

        subtests_ret.append({
                'test_type_name': 'AMC_v.1.0',
                'name': 'amc_port_{}'.format(slt),

                'details': {
                    'slot': slt,
                    'target_addr': t_addr,
                    'timeout': timeout
                },

                'summary': {
                    'verbose': 'Test successfully passed',
                    'duration': round((time.time()-start),2)
                },

                'measurements': measurements,
                'pass': 0
        })


        print('[Info] AMC card extracted')

    #Close serial interface
    IPMCDevObject.close()

    return subtests_ret

if __name__ == "__main__":
    pprint.pprint(amc_tester([0,1], [0xea,0x72], timeout = 100))
