#!/usr/bin/env python

import time
import pigpio
import sys, getopt
import lib.pcap as pcap

from IPMCDevLib import IPMCDevCom as IPMCDevCom
from IPMCDevLib import IPMCDevMgt as IPMCDevMgt

class sniffer:
   """
   A class to passively monitor activity on an I2C bus.  This should
   work for an I2C bus running at 100kbps or less.  You are unlikely
   to get any usable results for a bus running any faster.
   """

   def __init__(self, pi, SCL, SDA, cback, set_as_inputs=True):
      """
      Instantiate with the Pi and the gpios for the I2C clock
      and data lines.
      If you are monitoring one of the Raspberry Pi buses you
      must set set_as_inputs to False so that they remain in
      I2C mode.
      The pigpio daemon should have been started with a higher
      than default sample rate.
      For an I2C bus rate of 100Kbps sudo pigpiod -s 2 should work.
      A message is printed for each I2C transaction formatted with
      "[" for the START
      "XX" two hex characters for each data byte
      "+" if the data is ACKd, "-" if the data is NACKd
      "]" for the STOP
      E.g. Reading the X, Y, Z values from an ADXL345 gives:
      [A6+32+]
      [A7+01+FF+F2+FF+06+00-]
      """

      self.cback = cback
      self.pi = pi
      self.gSCL = SCL
      self.gSDA = SDA

      self.FALLING = 0
      self.RISING = 1
      self.STEADY = 2

      self.in_data = False
      self.byte = 0
      self.bit = 0
      self.oldSCL = 1
      self.oldSDA = 1

      self.transact = ""
      self.rx_data = []
      self.ack = True

      if set_as_inputs:
         self.pi.set_mode(SCL, pigpio.INPUT)
         self.pi.set_mode(SDA, pigpio.INPUT)

      self.cbA = self.pi.callback(SCL, pigpio.EITHER_EDGE, self._cb)
      self.cbB = self.pi.callback(SDA, pigpio.EITHER_EDGE, self._cb)

   def _parse(self, SCL, SDA):
      """
      Accumulate all the data between START and STOP conditions
      into a string and output when STOP is detected.
      """

      if SCL != self.oldSCL:
         self.oldSCL = SCL
         if SCL:
            xSCL = self.RISING
         else:
            xSCL = self.FALLING
      else:
            xSCL = self.STEADY

      if SDA != self.oldSDA:
         self.oldSDA = SDA
         if SDA:
            xSDA = self.RISING
         else:
            xSDA = self.FALLING
      else:
            xSDA = self.STEADY

      if xSCL == self.RISING:
         if self.in_data:
            if self.bit < 8:
               self.byte = (self.byte << 1) | SDA
               self.bit += 1
            else:
               self.transact += '{:02X}'.format(self.byte)
               self.rx_data.append(self.byte)

               if SDA:
                  self.transact += '-'
                  self.ack = False
               else:
                  self.transact += '+'
               self.bit = 0
               self.byte = 0

      elif xSCL == self.STEADY:

         if xSDA == self.RISING:
            if SCL:
               self.in_data = False
               self.byte = 0
               self.bit = 0
               self.transact += ']' # STOP
               self.cback(self.rx_data, self.ack)
               self.transact = ""

         if xSDA == self.FALLING:
            if SCL:
               self.in_data = True
               self.byte = 0
               self.bit = 0
               self.transact += '[' # START

               self.ack = True
               self.rx_data = []

   def _cb(self, gpio, level, tick):
      """
      Check which line has altered state (ignoring watchdogs) and
      call the parser with the new state.
      """
      SCL = self.oldSCL
      SDA = self.oldSDA

      if gpio == self.gSCL:
         if level == 0:
            SCL = 0
         elif level == 1:
            SCL = 1

      if gpio == self.gSDA:
         if level == 0:
            SDA = 0
         elif level == 1:
            SDA = 1

      self._parse(SCL, SDA)

   def cancel(self):
      """Cancel the I2C callbacks."""
      self.cbA.cancel()
      self.cbB.cancel()

class ipmbsniffer:
    def __init__(self, filename, timeout):
        self.p = pcap.pcap(filename)  # Create a new file
        self.i = 0
        self.byte_cnt = 0

        pi = pigpio.pi()

        if timeout == -1:
            print('[Info] Press Enter to stop capturing')

        s = sniffer(pi, 21, 26, self.i2c_callback, True) # leave gpios 1/0 in I2C mode

        if timeout == -1:
            input("")
        else:
            time.sleep(timeout)

        s.cancel()
        pi.stop()

    def acq_length(self):
        return self.byte_cnt

    def i2c_callback(self, data, ack):
        if ack:
            if len(data) > 0:
                self.byte_cnt += len(data)
                print('[Info] Received: {} bytes             '.format(self.byte_cnt), end='\r')

            s = bytearray()
            for d in data:
                s.append(d)
            self.p.write(((0,self.i,5+len(data)), s))
            self.i += 1

if __name__ == "__main__":

    #Init
    timeout = -1
    filename = ''
    port = -1
    port_v = ''

    #Scan args
    try:
        opts, args = getopt.getopt(sys.argv[1:],"ht:f:p:",["help", "timeout=","filename=","port="])
    except getopt.GetoptError:
        print('Usage: ipmb_sniffer -t <timeout> -f <filename> -p <ipmb port>')
        print('')
        print('Where:')
        print('\ttimeout: acquisition time in seconds')
        print('\tfilename: output file (pcap)')
        print('\tipmb port: ipmba, ipmbb or ipmbl')
        print('')
        sys.exit(-1)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print('Usage: ipmb_sniffer -t <timeout> -f <filename> -p <ipmb port>')
            print('')
            print('Where:')
            print('\ttimeout: acquisition time in seconds')
            print('\tfilename: output file (pcap)')
            print('\tipmb port: ipmba, ipmbb or ipmbl')
            print('')
            sys.exit()
        elif opt in ("-t", "--timeout"):
            try:
                timeout = int(arg)
            except:
                print('ERROR: timeout shall be an integer in seconds. Cannot be {} \n'.format(arg))
                print('Usage: ipmb_sniffer -t <timeout> -f <filename> -p <ipmb port>')
                print('')
                print('Where:')
                print('\ttimeout: acquisition time in seconds')
                print('\tfilename: output file (pcap)')
                print('\tipmb port: ipmba, ipmbb or ipmbl')
                print('')
                sys.exit(-1)


        elif opt in ("-f", "--filename"):
            filename = arg
        elif opt in ("-p", "--port"):
            port_v = arg

            if arg.lower() == 'ipmba':
                port = 0
            elif arg.lower() == 'ipmbb':
                port = 1
            elif arg.lower() == 'ipmbl':
                port = 2
            else:
                print('ERROR: port shall be either ipmba, ipmbb or ipmbl. Cannot be {} \n'.format(arg))
                print('Usage: ipmb_sniffer -t <timeout> -f <filename> -p <ipmb port>')
                print('')
                print('Where:')
                print('\ttimeout: acquisition time in seconds')
                print('\tfilename: output file (pcap)')
                print('\tipmb port: ipmba, ipmbb or ipmbl')
                print('')
                sys.exit(-1)

    if port == -1 or filename == '':
        print('Usage: ipmb_sniffer -t <timeout> -f <filename> -p <ipmb port>')
        print('')
        print('Where:')
        print('\ttimeout: acquisition time in seconds')
        print('\tfilename: output file (pcap)')
        print('\tipmb port: ipmba, ipmbb or ipmbl')
        print('')
        sys.exit(-1)

    print('[Info] Open port {}-{} [{}]'.format(port_v[:-1].upper(), port_v[-1:].upper(),port))

    IPMCDevObject = IPMCDevCom.IPMCDevCom()
    IPMCDevMgtObject = IPMCDevMgt.IPMCDevMgt(IPMCDevObject)
    IPMCDevMgtObject.SELI2C(2)
    IPMCDevObject.close()

    print('[Info] Start saving to {} for {} seconds'.format(filename, timeout))
    s = ipmbsniffer(filename, timeout)
    print('[Info] Saved {} bytes                       '.format(s.acq_length()))
