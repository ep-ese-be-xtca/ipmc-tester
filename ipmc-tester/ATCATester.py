'''
The ATCA subtest restarts the ATCA blade and checks the functionality of the required pins.
Therefore, it executes a full insertion/extraction cycle for a give hardware address and
checks the controller reactions. Finally, it triggers power issue alarm to validate the
failure detection.

The pass value depends on the test result:
    *  0: success (stop at "**End**")
    * -1: ATCA init failed (stop at "**Reset ATCA (using HA=0x00)**")
    * -2: AMC init failed (stop at "**Wait for 12V Enable**")
    * -3: PP de-assertion failed (stop at "**De-assert PP Good**")
    * -4: MP de-assertion failed (stop at "**De-assert MP Good**")
'''

#!/usr/bin/env python
# encoding: utf-8
import time
import sys
import pprint

import lib.sysapi as sysapi

from IPMCDevLib import IPMCDevCom as IPMCDevCom
from IPMCDevLib import IPMCDevATCA as IPMCDevATCA

import lib.IPMI as IPMI

@sysapi.register_debug('ATCA Subtest')
def test_atca(hardware_address=0xc1, hostname='192.168.1.34', timeout = 100, **kwargs):
    '''
    The ATCA debug functions restart the ATCA blade and checks the blade controller functionality.
    Therefore, it emulates a card insertion, followed by power issue.
    After executing the command, it checks the status of the setup and reports the time
    required to be in the expected states.

    When the timeout value is exceeded for a specific function, the test is considered to
    be failed.

    Args:
        hardware_address: ATCA Hardware address (default: 0xc1)
        timeout: Maximum time to wait for a function to be executed

    Returns:
        The function returns a dictionary formated to be used in the test instance.

        .. code-block:: javascript

            {
                'test_type_name': 'ATCA_v.1.0',
                'name': 'atca_tester',

                'details': {
                    'hardware_address': hardware_address,
                    'timeout': timeout
                },

                'summary': {
                    'verbose': str,
                    'rdy_on': rdy_duration,
                    'duration': round((time.time()-start),2)
                },

                'measurements': measurements,
                'pass': 0
            }

        Where pass can be:
            *  0: success
            * -1: ATCA init failed
            * -2: Not ready after timeout
            * -3: 12V not on after timeout
            * -4: Power alarms not detected
    '''

    debug_level = kwargs['debug_level'] if 'debug_level' in kwargs else 0
    IPMCDevObject = IPMCDevCom.IPMCDevCom(**kwargs)
    IPMCDevATCAObject = IPMCDevATCA.IPMCDevATCA(IPMCDevObject)
    ipmiLan = IPMI.IPMI(hostname=hostname,debug = 1 if debug_level >= 10 else 0)

    hardware_address = int(hardware_address)
    timeout = int(timeout)

    start = time.time()
    measurements = []

    #Assert PGOOD signals
    IPMCDevATCAObject.ASSERTPGOODA()
    IPMCDevATCAObject.ASSERTPGOODB()
    print('[Info] PGOOD signals asserted')

    #Close handle switch
    IPMCDevATCAObject.CONFHSW(IPMCDevATCAObject.VCC)
    print('[Info] Handle switch closed')

    #Set HA
    setha = IPMCDevATCAObject.SETHA(hardware_address)
    print(f'[Info] Set hardware address {hardware_address:02x}h')

    #reset the IPMC
    print('[Info] Reset the IPMC')
    ipmiLan.ipmc_reset()    

    print('[Info] Waiting for IPMC to be ready ...')
    ha = ipmiLan.ipmc_read_ha()
    print(f'[Info] IPMC hardware address: {ha:02x}h')

    #Open handle switch
    IPMCDevATCAObject.CONFHSW(IPMCDevATCAObject.GND)
    print('[Info] Handle switch opened')

    ins_start = time.time()
    # Wait for BLUE Led ON (Ready)
    rdy_timeout = 0
    while IPMCDevATCAObject.GETBLUELED() != 1 and rdy_timeout < timeout:
        #print(f'rdy = {rdy_timeout}')
        time.sleep(0.1)
        rdy_timeout = rdy_timeout + 1

    if rdy_timeout >= timeout:
        IPMCDevObject.close()

        return {
            'test_type_name': 'ATCA_v.1.0',
            'name': 'atca_tester',

            'details': {
                'hardware_address': hardware_address,
                'timeout': timeout
            },

            'summary': {
                'verbose': 'ATCA ready timeout',
                'duration': round((time.time()-start),2)
            },

            'measurements': measurements,
            'pass': -2
        }

    print('[Info] Blue LED ON')

    rdy_duration = round((time.time()-ins_start),2)
    measurements.append({'name': 'timer', 'data': {'s': 'ATCA ready', 'v': rdy_timeout}, 'pass': 0 })

    #Close handle switch
    IPMCDevATCAObject.CONFHSW(IPMCDevATCAObject.VCC)
    print('[Info] Handle switch closed')
    
    #Wait for 12V on
    on_timeout = 0
    while IPMCDevATCAObject.GET12VEN() == 0 and on_timeout < timeout:
        time.sleep(0.1)
        on_timeout = on_timeout + 1

    if on_timeout >= timeout:
        IPMCDevObject.close()

        return {
            'test_type_name': 'ATCA_v.1.0',
            'name': 'atca_tester',

            'details': {
                'hardware_address': hardware_address,
                'timeout': timeout
            },

            'summary': {
                'verbose': 'ATCA Power ON timeout',
                'duration': round((time.time()-start),2)
            },

            'measurements': measurements,
            'pass': -1
        }

    print('[Info] 12V is ON')

    measurements.append({'name': 'timer', 'data': {'s': 'ATCA power on', 'v': on_timeout}, 'pass': 0 })

    #Open handle switch
    IPMCDevATCAObject.CONFHSW(IPMCDevATCAObject.GND)
    print('[Info] Handle switch opened')

    #Wait for 12V on
    off_timeout = 0
    while IPMCDevATCAObject.GET12VEN() == 1 and off_timeout < timeout:
        time.sleep(0.1)
        off_timeout = off_timeout + 1

    if off_timeout >= timeout:
        IPMCDevObject.close()

        return {
            'test_type_name': 'ATCA_v.1.0',
            'name': 'atca_tester',

            'details': {
                'hardware_address': hardware_address,
                'timeout': timeout
            },

            'summary': {
                'verbose': 'ATCA Power ON timeout',
                'duration': round((time.time()-start),2)
            },

            'measurements': measurements,
            'pass': -3
        }
    print('[Info] 12V is OFF')

    measurements.append({'name': 'timer', 'data': {'s': 'ATCA power off', 'v': off_timeout}, 'pass': 0 })

    #Close handle switch
    IPMCDevATCAObject.CONFHSW(IPMCDevATCAObject.VCC)
    print('[Info] Handle switch closed')
    
    #Wait for 12V on
    on_timeout = 0
    while IPMCDevATCAObject.GET12VEN() != 1 and on_timeout < timeout:
        time.sleep(0.1)
        on_timeout = on_timeout + 1
    if on_timeout >= timeout:
        IPMCDevObject.close()
        print(f'[Error] power-on time exceeded')
        sys.exit()
    print('[Info] 12V is ON')

    #De-assert PGOOD signals
    IPMCDevATCAObject.DEASSERTPGOODA()
    IPMCDevATCAObject.DEASSERTPGOODB()
    print('[Info] PGOOD signals de-asserted')

    # Wait for 12V Enable OFF
    pgooderr_timeout = 0
    while IPMCDevATCAObject.GET12VEN() != 0 and pgooderr_timeout < timeout:
        time.sleep(0.1)
        pgooderr_timeout = pgooderr_timeout + 1

    if pgooderr_timeout >= timeout:
        IPMCDevObject.close()

        return {
            'test_type_name': 'ATCA_v.1.0',
            'name': 'atca_tester',

            'details': {
                'hardware_address': hardware_address,
                'timeout': timeout
            },

            'summary': {
                'verbose': 'ATCA PGOODA/B timeout',
                'duration': round((time.time()-start),2)
            },

            'measurements': measurements,
            'pass': -4
        }
    print('[Info] 12V is OFF')

    measurements.append({'name': 'timer', 'data': {'s': 'PGOOD error detection', 'v': pgooderr_timeout}, 'pass': 0 })

    # Open handle switch
    IPMCDevATCAObject.CONFHSW(IPMCDevATCAObject.GND)
    print('[Info] Handle switch opened')

    # Assert PGOOD signals
    IPMCDevATCAObject.ASSERTPGOODA()
    IPMCDevATCAObject.ASSERTPGOODB()
    print('[Info] PGOOD signals asserted')

    #Close handle switch
    IPMCDevATCAObject.CONFHSW(IPMCDevATCAObject.VCC)
    print('[Info] Handle switch closed')

    #Wait for 12V on
    timeout = 50
    on_timeout = 0
    while IPMCDevATCAObject.GET12VEN() != 1 and on_timeout < timeout:
        time.sleep(0.1)
        on_timeout = on_timeout + 1

    sysapi.set_job_progress(100)

    IPMCDevObject.close()
    ipmiLan.close()

    return {
        'test_type_name': 'ATCA_v.1.0',
        'name': 'atca_tester',

        'details': {
            'hardware_address': hardware_address,
            'timeout': timeout
        },

        'summary': {
            'verbose': 'Success',
            'rdy_on': rdy_duration,
            'duration': round((time.time()-start),2)
        },

        'measurements': measurements,
        'pass': 0
    }

if __name__ == "__main__":
    pprint.pprint(test_atca(hardware_address=0xc1, umgt_port='/dev/ipmc-umgt'), timeout=100)
