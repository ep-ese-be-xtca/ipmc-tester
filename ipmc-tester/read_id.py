#!/usr/bin/env python3

# script for reading IPMC identifiers: MAC address, serial number, board number

import argparse
import lib.IPMI as IPMI
import lib.IPMCID as IPMCID

# ---- read configuration ------------------------------------------------------

def read_config(**kwargs):

    if kwargs.get('hostname') is None:
        raise Exception('\"hostname\" not set')
    else:
        hostname = kwargs.get('hostname')

    if kwargs.get('debug_level') is None:
        debug_level = 0
    else:
        debug_level = kwargs.get('debug_level')

    # default parameters - depending on firmware
    def_lan_channel = 5
    def_timeout = 5

    ipmi = IPMI.IPMI(hostname=hostname, username='', password='')
    ipmcid = IPMCID.IPMCID(ipmi)

    # read BATCH name (EEPROM)
    s_out = ipmcid.batch_name_read()
    print(f'[Info] BATCH  (EEPROM) read:          {s_out}')

    # read SERIAL number (EEPROM)
    n_out = ipmcid.serial_number_read()
    print(f'[Info] SERIAL (EEPROM) read:          {ipmcid.serial_number_to_string(n_out)}')

    # read MAC address (EEPROM)
    n_out = ipmcid.mac_number_read()
    print(f'[Info] MAC    (EEPROM) read:          {ipmcid.mac_number_to_string(n_out)}')

    # read MAC address (LAN)
    lan = ipmi.ipmi_lan_read(channel=def_lan_channel)
    n_out = 0
    if 'MAC Address' in lan:
        n_out = ipmcid.mac_string_to_number(lan['MAC Address'])
    print(f'[Info] MAC    (LAN) read:             {ipmcid.mac_number_to_string(n_out)}')

    # read BATCH name (FRU)
    fru = ipmi.ipmi_fru_read()
    bn_out = fru['Product Part Number']
    print(f'[Info] BATCH  (FRU) read:             {bn_out}')

    # read SERIAL number (FRU)
    sn_out = fru['Product Serial']
    print(f'[Info] SERIAL (FRU) read:             {sn_out}')

    ipmi.close()

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--ipmc_ip', action='store', required=False, default='192.168.1.34')
    args = parser.parse_args()

    kwargs = {}

    if args.ipmc_ip:
        kwargs['hostname'] = args.ipmc_ip

    read_config(**kwargs)
