#!/usr/bin/env python3

class IPMCID:

    def __init__(self, ipmi):
        self.ipmi = ipmi

    # ---- MAC number <-> string -----------------------------------------------
    def mac_number_to_string(self, n):
        tstr = f'{n:012x}'
        rstr = ''
        flg = False
        # order from left to right
        for c1,c2 in (tstr[i:i+2] for i in range(0,11,+2)):
            if flg:
                rstr += ':'
            else :
                flg = True
            rstr += '{}{}'.format(c1,c2)
        return rstr

    def mac_string_to_number(self, s):
        tlst = s.split(':')
        rstr = ''
        # order from left to right
        for i in range(len(tlst)):
            rstr += f'{tlst[i]}'
        return int(rstr,16)

    # ---- Serial number <-> string --------------------------------------------
    def serial_number_to_string(self, n):
        tstr = f'{n:08d}'
        rstr = tstr[0:4] + ' ' + tstr[4:8]
        return rstr

    def serial_string_to_number(self, s):
        rstr = ''
        for c in s:
            if c == ' ':
                continue
            rstr += c
        return int(rstr)

    # ---- BATCH name write/read functions -------------------------------------
    def batch_name_write(self, s):
        def batch_name_to_oem(s):
            r = []
            # order from left to right
            for c in s:
                r.append(ord(c))
            return r
        return self.ipmi.ipmi_send(0x2e, 0x56, [0x60, 0x00, 0x00] + batch_name_to_oem(s))

    def batch_name_read(self):
        def batch_oem_to_name(o):
            rstr = ''
            for c in o:
                rstr += chr(c)
            return rstr
        r = self.ipmi.ipmi_send(0x2e, 0x57, [0x60, 0x00, 0x00])
        return batch_oem_to_name(r[3:])

    # ---- SERIAL number write/read functions ----------------------------------
    def serial_number_write(self, n):
        def serial_number_to_oem(n):
            tstr = f'{n:08d}'
            r = []
            # order from left to right
            for i in range(0,len(tstr)):
                r.append(ord(f'{tstr[i]}'))
                if i == 3:
                    r.append(ord(' '))
            return r
        return self.ipmi.ipmi_send(0x2e, 0x54, [0x60, 0x00, 0x00] + serial_number_to_oem(n))

    def serial_number_read(self):
        def serial_oem_to_number(o):
            rstr = ''
            # order from left to right
            for i in range(0,len(o)):
                if o[i] == ord(' '):
                    continue
                rstr += chr(int(o[i]))
            return int(rstr)
        r = self.ipmi.ipmi_send(0x2e, 0x55, [0x60, 0x00, 0x00])
        return serial_oem_to_number(r[3:])

    # ---- MAC address number write/read functions -----------------------------
    def mac_number_write(self, n):
        def mac_number_to_oem(n):
            tstr = f'{n:#0{14}x}'[2:]
            r = []
            # order from right to left
            for c in (tstr[i:i+2] for i in range(10,-1,-2)):
                r.append(int('{}'.format(c),16))
            return r
        return self.ipmi.ipmi_send(0x2e, 0x52, [0x60, 0x00, 0x00] + mac_number_to_oem(n))

    def mac_number_read(self):
        def mac_oem_to_number(o):
            rstr = ''
            # order from right to left
            for i in range(len(o)-1,0,-1):
                rstr += f'{o[i]:0{2}x}'
            r = int(rstr,16)
            return r
        r = self.ipmi.ipmi_send(0x2e, 0x4f, [0x60, 0x00, 0x00])
        return mac_oem_to_number(r[2:])
