import subprocess
import platform
import os

class IPMI:

    def __init__(self, hostname='', username='', password='', serial='', debug=0):
        self.hostname = hostname
        self.username = username
        self.password = password
        self.serial = serial
        self.prefix = ''
        self.debug = debug

        if hostname:
            current_os = platform.system().lower()
            if current_os == "windows":
                parameter = "-n"
            else:
                parameter = "-c"
            for i in range (5):
                exit_code = os.system(f"ping {parameter} 1 -w2 {hostname} > /dev/null 2>&1")
                if exit_code == 0:
                    break
            if exit_code != 0:
                raise Exception('IPMC not detected: "{}" does not reply'.format(hostname))
            self.prefix = [ 'ipmitool',
                            '-I', 'lan',
                            '-H', self.hostname,
                            '-U', self.username,
                            '-P', self.password ]
        elif serial:
            if not os.path.exists(serial):
                raise Exception('IPMC serial device "{}" does not exist'.format(serial))
            self.prefix = [ 'ipmitool',
                            '-I', 'serial',
                            '-D', self.serial+":115200",
                            '-U', self.username,
                            '-P', self.password ]
        else:
            raise Exception('IPMC not created: need hostname or serial')

    def close(self):
        return

    def ipmi_send(self, netfn, cmd, data):
        cmd = self.prefix + [ 'raw', hex(netfn), hex(cmd) ]
        for d in data:
            cmd.append(hex(d))
        if self.debug:
            print(f'[Debug] IPMI command = {cmd}')
        cmd_rd = subprocess.run(cmd, stdout=subprocess.PIPE)
        if cmd_rd.returncode != 0 or cmd_rd.stdout.decode() == '':
            raise Exception('IPMI command failed')
        if self.debug:
            print(f'[Debug] IPMI response = {cmd_rd.stdout.decode()}')
        try:
            ret = cmd_rd.stdout.decode().split()
            reply = [int(x, 16) for x in ret]
        except:
            raise Exception(f'[Error] IPMI command {cmd} failed ({cmd_rd})')
        return reply

    def ipmc_reset(self, reset_type='warm'):
        """
        Issue a reset command to the IPMC
        """
        cmd = self.prefix + [ 'mc', 'reset', str(reset_type) ]
        if self.debug:
            print(f'[Debug] IPMI command = {cmd}')
        result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if result.returncode != 0:
            raise Exception(f'IPMI command (mc reset) failed, stderr={result.stderr.decode()}')
        return result

    def ipmc_read_ha(self):
        """
        Read the hardware address from the IPMC using an OEM command
        """
        reply = self.ipmi_send(0x2e, 0x05, [0x0a, 0x40, 0x00])
        return reply[3] 

    def ipmc_read_led(self, fru, led):
        """
        Read the LED state from the IPMC using an OEM command
        """
        reply = self.ipmi_send(0x2e, 0x19, [0x0a, 0x40, 0x00, fru, led])
        ret = {}
        ret['off-duration'] = reply[3]
        ret['on-duration'] = reply[4]
        ret['color'] = reply[5]
        ret['off-first'] = reply[6]
        return ret

    def ipmc_write_led(self, fru, led, state):
        """
        Write the LED state from the IPMC using an OEM command
        """
        reply = self.ipmi_send(0x2e, 0x18, [0x0a, 0x40, 0x00, fru, led, state['off-duration'], state['on-duration'], state['color'], state['off-first']])
        return (reply == [0x0a, 0x40, 0x00])

    def ipmc_sensor_read(self, name):
        """
        Read a sensor from the IPMC
        """
        cmd = self.prefix + [ 'sensor', 'reading', str(name) ]
        if self.debug:
            print(f'[Debug] IPMI command = {cmd}')
        result = subprocess.run(cmd, stdout=subprocess.PIPE)
        if result.returncode != 0:
            raise Exception('IPMI command (sensor reading) failed')
        try:
            reading = result.stdout.decode().rstrip().split('|')[1]
        except:
            raise Exception('IPMI command (sensor reading decoding) failed')
        return float(reading)

    def ipmi_fru_write(self, fru, section, field, data):
        cmd = self.prefix + [ 'fru', 'edit', str(fru), 'field', str(section), str(field), str(data) ]
        if self.debug:
            print(f'[Debug] IPMI command = {cmd}')
        result = subprocess.run(cmd, stdout=subprocess.PIPE)
        if result.returncode != 0:
            prt = result.stdout.decode()
            if "String size are not equal" in prt and "Done" in prt:
                pass
            else:
                raise Exception('IPMI command (fru edit) failed')

    def ipmi_fru_read(self):
        cmd = self.prefix + [ 'fru', 'print' ]
        if self.debug:
            print(f'[Debug] IPMI command = {cmd}')
        result = subprocess.run(cmd, stdout=subprocess.PIPE)
        if result.returncode != 0:
            raise Exception('IPMI command (fru print) failed')
        if self.debug:
            print(f'[Debug] IPMI response = {result.stdout.decode()}')
        fru={}
        try:
            for line in result.stdout.decode().split('\n'):
                key, val = line.partition(" : ")[::2]
                if key:
                    fru[key.strip()] = val
        except:
            raise Exception('IPMI command failed reading the FRU info')
        return fru

    def ipmi_lan_read(self, channel=5):
        cmd = self.prefix + [ 'lan', 'print', str(channel) ]
        if self.debug:
            print(f'[Debug] IPMI command = {cmd}')
        result = subprocess.run(cmd, stdout=subprocess.PIPE)
        if result.returncode != 0:
            raise Exception('IPMI command (lan print) failed')
        if self.debug:
            print(f'[Debug] IPMI response = {result.stdout.decode()}')
        fld = 0
        try:
            fld, ret, lv2 = {}, {}, {}
            key = ''
            for line in result.stdout.decode().split('\n'):
                fld = [x.strip() for x in line.split(' : ')]
                if len(fld) == 1:
                    continue
                if fld[0] != '' and fld[0] != key and lv2 != {}:
                    ret[key] = {}
                    for x in lv2:
                        ret[key][x] = lv2[x]
                    lv2.clear()
                if len(fld) == 2:
                    key = fld[0]
                    val = fld[1]
                elif len(fld) == 3:
                    if fld[0] == '':
                        lv2[fld[1]] = fld[2]
                    else:
                        key = fld[0]
                        lv2[fld[1]] = fld[2]
                ret[key] = val
            return ret
        except:
            raise Exception('IPMI command failed reading the FRU info)')

    def ipmi_hpm_upgrade(self, file_name):
        cmd = self.prefix + [ 'hpm', 'upgrade', str(file_name), 'force' ]
        if self.debug:
            print(f'[Debug] IPMI command = {cmd}')
        result = subprocess.run(cmd, input='y'.encode(), stdout=subprocess.PIPE)
        if result.returncode != 0:
            raise Exception('IPMI command (hpm upgrade) failed')
        print(result.stdout.decode())

    def ipmi_hpm_activate(self):
        cmd = self.prefix + [ 'hpm', 'activate' ]
        if self.debug:
            print(f'[Debug] IPMI command = {cmd}')
        result = subprocess.run(cmd, input='y'.encode(), stdout=subprocess.PIPE)
        if result.returncode != 0:
            raise Exception('IPMI command (hpm activate) failed')
        print(result.stdout.decode())
