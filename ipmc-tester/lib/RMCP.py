import socket

class RMCP:

    LAN_PORT = 623

    RMCP_VERSION = 0x06
    RMCP_CLASS_ASF = 0x06
    RMCP_CLASS_IPMI = 0x07
    RMCP_CLASS_OEM = 0x08

    def __init__(self, hostname):
        self.rmcp_sequence_number = 1
        self.hostname = hostname

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return

    def ipmi(self, ipmi_session, ipmi_message):
        msg = [
                self.RMCP_VERSION,
                0x00,
                0xFF, #FFh for IPMI
                self.RMCP_CLASS_IPMI #07h for IPMI
            ]

        msg.extend(ipmi_session)
        msg.append(len(ipmi_message))
        msg.extend(ipmi_message)

        self.sock.sendto(bytes(msg), (self.hostname, self.LAN_PORT))
        r = self.sock.recvfrom(1024)
        msg = r[0]

        ipmi_session = []

        if msg[4] == 0:
            ipmi_session = {
                    'ipmi_auth_type': 0,
                    'ipmi_session_seq': msg[5] << 24 | msg[6] << 16 | msg[7] << 8 | msg[8],
                    'ipmi_session_id': msg[9] << 24 | msg[10] << 16 | msg[11] << 8 | msg[12]
                }
            ipmi_msg_offset = 13

        else:
            ipmi_session = {
                    'ipmi_auth_type': 0,
                    'ipmi_session_seq': msg[5] << 24 | msg[6] << 16 | msg[7] << 8 | msg[8],
                    'ipmi_session_id': msg[9] << 24 | msg[10] << 16 | msg[11] << 8 | msg[12],
                    'ipmi_auth_code': [x for x in msg[13:28]]
                }
            ipmi_msg_offset = 29

        return {
                'rmcp_header': msg[0],
                'rmcp_sequence_number': msg[2],
                'rmcp_class': msg[3],
                'ipmi_session': ipmi_session,
                'ipmi_len': msg[ipmi_msg_offset],
                'ipmi_message': {
                        'ipmi_rqaddr': msg[(ipmi_msg_offset+1)],
                        'ipmi_netfn': msg[(ipmi_msg_offset+2)] >> 2,
                        'ipmi_rqlun': msg[(ipmi_msg_offset+2)] & 0x03,
                        'ipmi_hdr_chck': msg[(ipmi_msg_offset+3)],
                        'ipmi_rsaddr': msg[(ipmi_msg_offset+4)],
                        'ipmi_rqSeq': msg[(ipmi_msg_offset+5)] >> 2,
                        'ipmi_rslun': msg[(ipmi_msg_offset+5)] & 0x03,
                        'ipmi_cmd': msg[(ipmi_msg_offset+6)],
                        'ipmi_ccode': msg[(ipmi_msg_offset+7)],
                        'ipmi_data': [x for x in msg[(ipmi_msg_offset+8):-1]],
                        'ipmi_data_chck': msg[-1],
                    }
            }

    def ping(self, message_tag=0x20):
        msg = bytes([
            self.RMCP_VERSION,
            0x00,
            self.rmcp_sequence_number,
            self.RMCP_CLASS_ASF,
            0x00,
            0x00,
            0x11,
            0xbe,
            0x80,
            message_tag,
            0x00,
            0x00])

        self.rmcp_sequence_number += 1
        if self.rmcp_sequence_number > 254:
            self.rmcp_sequence_number = 1

        self.sock.sendto(msg, (self.hostname, self.LAN_PORT))
        r = self.sock.recvfrom(1024)
        msg = r[0]

        return {
                'rmcp_header': msg[0],
                'rmcp_sequence_number': msg[2],
                'rmcp_class': msg[3],
                'asf_iana': (msg[4] << 24 | msg[5] << 16 | msg[6] << 8 | msg[7]),
                'asf_message_type': msg[8],
                'asf_message_tag': msg[9],
                'asf_data_length': msg[11],
                'asf_supported_entities': msg[20]
            }
