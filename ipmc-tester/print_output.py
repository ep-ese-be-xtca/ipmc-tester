#!/usr/bin/env python3

import sys
import pickle
from pprint import pprint

try:
    fn = sys.argv[1]
except:
    print('must specify a file name')
    exit(-1)

with open('xxx.json','rb') as file:
    data = pickle.load(file)
    pprint(data)
