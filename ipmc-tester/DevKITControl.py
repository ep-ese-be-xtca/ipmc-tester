#!/usr/bin/env python3

import sys, getopt
import json
import argparse


from IPMCDevLib import IPMCDevCom as IPMCDevCom
from IPMCDevLib import IPMCDevMgt as IPMCDevMgt
from IPMCDevLib import IPMCDevATCA as IPMCDevATCA
from IPMCDevLib import IPMCDevAMC as IPMCDevAMC

def even_parity(x):
    x = x ^ (x >> 4);
    x = x ^ (x >> 2);
    x = x ^ (x >> 1);
    # Rightmost bit holds the parity value
    # 1 means parity is odd else even
    return ~x & 1

def set_atca_ha(ipmcdevmgt, ipmcdevatca, ipmcdevamc, address):
    addr = int(address, 0)
    # mask the parity bit
    addr &= 0x7f
    # insert odd parity bit
    addr = (even_parity(addr) << 7) | addr 
    r = ipmcdevatca.SETHA(addr)
    return {
        'status': 'success' if r == 0 else 'failed',
        'hardware address': hex(addr)
    }

def get_atca_ha(ipmcdevmgt, ipmcdevatca, ipmcdevamc):
    hwaddr = int(ipmcdevatca.GETHA(), 16)
    return {
        'hardware address': hex(hwaddr)
    }

def set_atca_hs(ipmcdevmgt, ipmcdevatca, ipmcdevamc, state):
    if state == 'open':
        r = ipmcdevatca.CONFHSW(IPMCDevATCAObject.GND)
    elif state == 'closed':
        r = ipmcdevatca.CONFHSW(IPMCDevATCAObject.VCC)
    else:
        return {'status': 'failed', 'error': '-s can only be "open" or "closed"'}

    return {
        'status': 'success' if r == 0 else 'failed',
        'state': state
    }

def get_atca_blueled(ipmcdevmgt, ipmcdevatca, ipmcdevamc):
    r = ipmcdevatca.GETBLUELED()
    return {
            'status': 'success' if r >= 0 else 'failed',
            'state': 'off' if r == 0 else 'on'
        }

def get_atca_led1(ipmcdevmgt, ipmcdevatca, ipmcdevamc):
    r = ipmcdevatca.GETLED1()
    return {
            'status': 'success' if r >= 0 else 'failed',
            'state': 'off' if r == 0 else 'on'
        }

def get_atca_led2(ipmcdevmgt, ipmcdevatca, ipmcdevamc):
    r = ipmcdevatca.GETLED2()
    return {
            'status': 'success' if r >= 0 else 'failed',
            'state': 'off' if r == 0 else 'on'
        }

def get_atca_led3(ipmcdevmgt, ipmcdevatca, ipmcdevamc):
    r = ipmcdevatca.GETLED3()
    return {
            'status': 'success' if r >= 0 else 'failed',
            'state': 'off' if r == 0 else 'on'
        }

def get_atca_12ven(ipmcdevmgt, ipmcdevatca, ipmcdevamc):
    r = ipmcdevatca.GET12VEN()
    return {
            'status': 'success' if r >= 0 else 'failed',
            'state': 'off' if r == 0 else 'on'
        }

def set_atca_pgooda(ipmcdevmgt, ipmcdevatca, ipmcdevamc, state):
    if state == 'assert':
        r = ipmcdevatca.ASSERTPGOODA()
    elif state == 'de-assert':
        r = ipmcdevatca.DEASSERTPGOODA()
    else:
        return {'status': 'failed', 'error': '-s can only be "assert" or "de-assert"'}

    return {
        'status': 'success' if r == 0 else 'failed',
        'state': state
    }

def set_atca_pgoodb(ipmcdevmgt, ipmcdevatca, ipmcdevamc, state):
    if state == 'assert':
        r = ipmcdevatca.ASSERTPGOODB()
    elif state == 'de-assert':
        r = ipmcdevatca.DEASSERTPGOODB()
    else:
        return {'status': 'failed', 'error': '-s can only be "assert" or "de-assert"'}

    return {
        'status': 'success' if r == 0 else 'failed',
        'state': state
    }

def get_user_io(ipmcdevmgt, ipmcdevatca, ipmcdevamc, id):

    if int(id, 0) > 50:
        return {'status': 'failed', 'error': 'IO id shall be lower than 51'}

    r = ipmcdevatca.GETIO(int(id, 0))

    return {
            'status': 'success' if r >= 0 else 'failed',
            'state': 'vcc' if r == IPMCDevATCAObject.VCC else 'gnd'
        }

def set_user_io(ipmcdevmgt, ipmcdevatca, ipmcdevamc, id, state):

    if int(id, 0) > 50:
        return {'status': 'failed', 'error': 'IO id shall be lower than 51'}

    if state == 'vcc':
        r = ipmcdevatca.SETIO(int(id, 0), IPMCDevATCAObject.VCC)
    elif state == 'gnd':
        r = ipmcdevatca.SETIO(int(id, 0), IPMCDevATCAObject.GND)
    else:
        return {'status': 'failed', 'error': '-s can only be "vcc" or "gnd"'}

    return {
        'status': 'success' if r == 0 else 'failed',
        'state': state
    }

def set_amc_ga(ipmcdevmgt, ipmcdevatca, ipmcdevamc, address):
    addr = int(address, 0)
    r = ipmcdevamc.SETGA(addr)
    return {
        'status': 'success' if r == 0 else 'failed',
        'geographical address': addr
    }

def sel_amc_port(ipmcdevmgt, ipmcdevatca, ipmcdevamc, port):
    port = int(port, 0)
    r = ipmcdevamc.SELAMC(port)
    return {
        'status': 'success' if r == 0 else 'failed',
        'AMC slot': port
    }

def set_amc_state(ipmcdevmgt, ipmcdevatca, ipmcdevamc, state):
    if state == 'insert':
        r = ipmcdevamc.INSERTAMC()
    elif state == 'extract':
        r = ipmcdevamc.EXTRACTAMC()
    else:
        return {'status': 'failed', 'error': '-s can only be "insert" or "extract"'}

    return {
        'status': 'success' if r == 0 else 'failed',
        'state': state
    }

def set_amc_hs(ipmcdevmgt, ipmcdevatca, ipmcdevamc, state):
    if state == 'open':
        r = ipmcdevamc.OPENHSW()
    elif state == 'closed':
        r = ipmcdevamc.CLOSEHSW()
    else:
        return {'status': 'failed', 'error': '-s can only be "open" or "closed"'}

    return {
        'status': 'success' if r == 0 else 'failed',
        'state': state
    }

def get_amc_mpen(ipmcdevmgt, ipmcdevatca, ipmcdevamc):
    r = ipmcdevamc.GETMPEN()
    return {
            'status': 'success' if r >= 0 else 'failed',
            'state': 'off' if r == 0 else 'on'
        }

def get_amc_ppen(ipmcdevmgt, ipmcdevatca, ipmcdevamc):
    r = ipmcdevamc.GETPPEN()
    return {
            'status': 'success' if r >= 0 else 'failed',
            'state': 'off' if r == 0 else 'on'
        }

def set_amc_mpgood(ipmcdevmgt, ipmcdevatca, ipmcdevamc, state):
    if state == 'assert':
        r = ipmcdevamc.ASSERTMPGOOD()
    elif state == 'de-assert':
        r = ipmcdevamc.DEASSERTMPGOOD()
    else:
        return {'status': 'failed', 'error': '-s can only be "assert" or "de-assert"'}

    return {
        'status': 'success' if r == 0 else 'failed',
        'state': state
    }

def set_amc_ppgood(ipmcdevmgt, ipmcdevatca, ipmcdevamc, state):
    if state == 'assert':
        r = ipmcdevamc.ASSERTPPGOOD()
    elif state == 'de-assert':
        r = ipmcdevamc.DEASSERTPPGOOD()
    else:
        return {'status': 'failed', 'error': '-s can only be "assert" or "de-assert"'}

    return {
        'status': 'success' if r == 0 else 'failed',
        'state': state
    }

def get_power(ipmcdevmgt, ipmcdevatca, ipmcdevamc):
    mgt_v = ipmcdevmgt.GETMGTVOLTAGE()
    ipmc_v = ipmcdevmgt.GETIPMCVOLTAGE()
    ipmc_c = ipmcdevmgt.GETIPMCCURRENT()

    return {
            'status': 'success',
            'MGT supply  ': f'{mgt_v:.3f}V',
            'IPMC supply ': f'{ipmc_v:.3f}V',
            'IPMC current': f'{ipmc_c:.3f}A',
        }

def select_i2cout(ipmcdevmgt, ipmcdevatca, ipmcdevamc, bus):
    if bus == 'ipmba':
        r = ipmcdevmgt.SELI2C(0)
    elif bus == 'ipmbb':
        r = ipmcdevmgt.SELI2C(1)
    elif bus == 'ipmbl':
        r = ipmcdevmgt.SELI2C(2)
    else:
        return {'status': 'failed', 'error': '-b can only be "ipmba", "ipmbb" or "ipmbl"'}

    return {
        'status': 'success' if r == 0 else 'failed',
        'state': '{}-{}'.format(bus[:-1].upper(), bus[-1:].upper())
    }

actions = {


    'select_i2cout':
        {
            'help': 'Usage: devkit_ctrl -a select_i2cout -b <ipmba, ipmbb or ipmbl> [--json]',
            'function': select_i2cout,
            'params': [
                {
                    'opt': 'b',
                    'key': 'bus'
                }
            ]

        },

    'get_power':
        {
            'help': 'Usage: devkit_ctrl -a get_power [--json]',
            'function': get_power,
        },

    'set_amc_mpgood':
        {
            'help': 'Usage: devkit_ctrl -a set_amc_mpgood -s <assert or de-assert> [--json]',
            'function': set_amc_mpgood,
            'params': [
                {
                    'opt': 's',
                    'key': 'state'
                }
            ]

        },
    'set_amc_ppgood':
        {
            'help': 'Usage: devkit_ctrl -a set_amc_ppgood -s <assert or de-assert> [--json]',
            'function': set_amc_ppgood,
            'params': [
                {
                    'opt': 's',
                    'key': 'state'
                }
            ]

        },
    'get_amc_ppen':
        {
            'help': 'Usage: devkit_ctrl -a get_amc_ppen [--json]',
            'function': get_amc_ppen

        },
    'get_amc_mpen':
        {
            'help': 'Usage: devkit_ctrl -a get_amc_mpen [--json]',
            'function': get_amc_mpen

        },
    'set_amc_hs':
        {
            'help': 'Usage: devkit_ctrl -a set_amc_hs -s <open or closed> [--json]',
            'function': set_amc_hs,
            'params': [
                {
                    'opt': 's',
                    'key': 'state'
                }
            ]

        },

    'set_amc_state':
        {
            'help': 'Usage: devkit_ctrl -a set_amc_state -s <insert or extract> [--json]',
            'function': set_amc_state,
            'params': [
                {
                    'opt': 's',
                    'key': 'state'
                }
            ]

        },

    'sel_amc_port':
        {
            'help': 'Usage: devkit_ctrl -a sel_amc_port -p <port id> [--json]',
            'function': sel_amc_port,
            'params': [
                {
                    'opt': 'p',
                    'key': 'port'
                }
            ]

        },

    'set_amc_ga':
        {
            'help': 'Usage: devkit_ctrl -a set_amc_ga -v <address> [--json]',
            'function': set_amc_ga,
            'params': [
                {
                    'opt': 'v',
                    'key': 'address'
                }
            ]

        },

    'set_atca_ha':
        {
            'help': 'Usage: devkit_ctrl -a set_atca_ha -v <address> [--json]',
            'function': set_atca_ha,
            'params': [
                {
                    'opt': 'v',
                    'key': 'address'
                }
            ]

        },

    'get_atca_ha':
        {
            'help': 'Usage: devkit_ctrl -a get_atca_ha [--json]',
            'function': get_atca_ha,
        },

    'set_atca_hs':
        {
            'help': 'Usage: devkit_ctrl -a set_atca_hs -s <open or closed> [--json]',
            'function': set_atca_hs,
            'params': [
                {
                    'opt': 's',
                    'key': 'state'
                }
            ]

        },

    'get_atca_blueled':
        {
            'help': 'Usage: devkit_ctrl -a get_atca_blueled [--json]',
            'function': get_atca_blueled
        },
	
    'get_atca_led1':
        {
            'help': 'Usage: devkit_ctrl -a get_atca_led1 [--json]',
            'function': get_atca_led1
        },
	
    'get_atca_led2':
        {
            'help': 'Usage: devkit_ctrl -a get_atca_led2 [--json]',
            'function': get_atca_led2
        },

    'get_atca_led3':
        {
            'help': 'Usage: devkit_ctrl -a get_atca_led3 [--json]',
            'function': get_atca_led3
        },

    'get_atca_12ven':
        {
            'help': 'Usage: devkit_ctrl -a get_atca_12ven [--json]',
            'function': get_atca_12ven
        },

    'set_atca_pgooda':
        {
            'help': 'Usage: devkit_ctrl -a set_atca_pgooda -s <assert or de-assert> [--json]',
            'function': set_atca_pgooda,
            'params': [
                {
                    'opt': 's',
                    'key': 'state'
                }
            ]

        },

    'set_atca_pgoodb':
        {
            'help': 'Usage: devkit_ctrl -a set_atca_pgoodb -s <assert or de-assert> [--json]',
            'function': set_atca_pgoodb,
            'params': [
                {
                    'opt': 's',
                    'key': 'state'
                }
            ]

        },

    'set_user_io':
        {
            'help': 'Usage: devkit_ctrl -a set_user_io -i <IO id> -s <vcc or gnd> [--json]',
            'function': set_user_io,
            'params': [
                {
                    'opt': 's',
                    'key': 'state'
                },
                {
                    'opt': 'i',
                    'key': 'id'
                }
            ]

        },

    'get_user_io':
        {
            'help': 'Usage: devkit_ctrl -a get_user_io -i <IO id> [--json]',
            'function': get_user_io,
            'params': [
                {
                    'opt': 'i',
                    'key': 'id'
                }
            ]

        },
}

def print_result(r, jsonen):
    if jsonen:
        print(json.dumps(r, indent=4, sort_keys=True))
    else:
        print('Result(s):')
        print('==========')
        print('')

        for k in r:
            print('\t{}: {}'.format(k, r[k]))


def printHelp(action = None):

    ret = ''

    if action is None:
        ret += 'Usage: devkit_ctrl -a <ACTION> [--json] \n'
        ret += ' \n'
        ret += 'Where action can be: \n'
        ret += '==================== \n'

        ret += ' \n'
        ret += '\tDevelopment kit functions: \n'
        ret += '\t\tget_power: get power status \n'
        ret += '\t\tselect_i2cout: select I2C OUT portv'

        ret += ' \n'
        ret += '\tATCA functions: \n'
        ret += '\t\tset_atca_ha: set ATCA hardware address \n'
        ret += '\t\tget_atca_ha: get ATCA hardware address \n'
        ret += '\t\tset_atca_hs: set ATCA Handle Switch position \n'
        ret += '\t\tget_atca_blueled: get ATCA blue led status \n'
        ret += '\t\tget_atca_led1: get ATCA FRU_LED1 status \n'
        ret += '\t\tget_atca_led2: get ATCA FRU_LED2 status \n'
        ret += '\t\tget_atca_led3: get ATCA FRU_LED3 status \n'
        ret += '\t\tget_atca_12ven: get ATCA 12V Enable signal \n'
        ret += '\t\tset_atca_pgooda: set power good A signal \n'
        ret += '\t\tset_atca_pgoodb: set power good B signal \n'

        ret += ' \n'
        ret += '\tI/O functions: \n'
        ret += '\t\tset_user_io: set user I/O signal \n'
        ret += '\t\tget_user_io: get user I/O signal \n'

        ret += ''
        ret += '\tAMC functions: \n'
        ret += '\t\tset_amc_ga: set AMC geographical address \n'
        ret += '\t\tsel_amc_port: select AMC port on IPMC connector \n'
        ret += '\t\tset_amc_state: set AMC state - inserted or extracted \n'
        ret += '\t\tset_amc_hs: set AMC Handle Switch position \n'
        ret += '\t\tget_amc_mpen: get AMC MP Enable signal \n'
        ret += '\t\tget_amc_ppen: get AMC PP Enable signal \n'
        ret += '\t\tset_amc_mpgood: set AMC MP good signal \n'
        ret += '\t\tset_amc_ppgood: set AMC PP good signal \n'

    return ret

if __name__ == "__main__":

    #Init:
    action = None
    jsonen = False

    #Scan args
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('-a','--action', required=False)
    #parser.add_argument('-u','--umgt', required=False)
    parser.add_argument('-j','--json', action='store_true', required=False)
    parser.add_argument('-h', '--help', action='store_true', required=False)
    args = parser.parse_known_args()

    if (args[0].action is None and args[0].help) or args[0].action is None:
        print(printHelp())
        exit(0)

    action = actions[args[0].action]

    if args[0].help:
        print(action['help'])
        exit(-1)

    jsonen = args[0].json

    # uMGT port
    umgt_port = '/dev/ipmc-umgt'

    #Get action params if needed
    if 'params' in action and len(action['params']) > 0:
        parser = argparse.ArgumentParser(add_help=False)
        for p in action['params']:
            parser.add_argument('-{}'.format(p['opt']),'--{}'.format(p['key']), required=False)

        args = parser.parse_known_args()

        tmp = vars(args[0])
        for k in tmp:
            if tmp[k] == None:
                print("Missing argument - {}".format(action['help']))
                exit(-1)

    IPMCDevObject = IPMCDevCom.IPMCDevCom(umgt_port = umgt_port)
    IPMCDevMgtObject = IPMCDevMgt.IPMCDevMgt(IPMCDevObject)
    IPMCDevATCAObject = IPMCDevATCA.IPMCDevATCA(IPMCDevObject)
    IPMCDevAMCObject = IPMCDevAMC.IPMCDevAMC(IPMCDevObject)

    try:
        if 'params' in action and len(action['params']) > 0:
            r = action['function'](ipmcdevmgt = IPMCDevMgtObject, ipmcdevatca = IPMCDevATCAObject, ipmcdevamc = IPMCDevAMCObject, **vars(args[0]))
        else:
            r = action['function'](ipmcdevmgt = IPMCDevMgtObject, ipmcdevatca = IPMCDevATCAObject, ipmcdevamc = IPMCDevAMCObject)

    except Exception as e:
        r = {'status': 'failed', 'error': str(e)}

    IPMCDevObject.close()

    print_result(r, jsonen)

    exit(0)

    '''
    if port == -1 or filename == '':
        print('Usage: ipmb_sniffer -t <timeout> -f <filename> -p <ipmb port>')
        print('')
        print('Where:')
        print('\ttimeout: acquisition time in seconds')
        print('\tfilename: output file (pcap)')
        print('\tipmb port: ipmba, ipmbb or ipmbl')
        print('')
        sys.exit(-1)

    print('[Info] Open port {}-{} [{}]'.format(port_v[:-1].upper(), port_v[-1:].upper(),port))

    IPMCDevObject = IPMCDevCom.IPMCDevCom()
    IPMCDevMgtObject = IPMCDevMgt.IPMCDevMgt(IPMCDevObject)
    IPMCDevMgtObject.SELI2C(2)


    print('[Info] Start saving to {} for {} seconds'.format(filename, timeout))
    s = ipmbsniffer(filename, timeout)
    print('[Info] Saved {} bytes                       '.format(s.acq_length()))
    '''
