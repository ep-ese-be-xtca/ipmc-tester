'''
The I/O subtest restarts the ATCA blade and checks the functionality of the user I/Os,
which includes the USR_IO and the IPMI_IOs.
Therefore, it test for each pin the possibility to set it in output and drive the line
to VCC or GND before setting it in input and checking the level detection.

The pass value depends on the test result:
    *  0: Success
    * -1: Set GND failed
    * -2: Set VCC failed
    * -3: Get GND failed
    * -4: Get VCC failed
'''

#!/usr/bin/env python
# encoding: utf-8
import time
import pprint

import lib.sysapi as sysapi

from IPMCDevLib import IPMCDevCom as IPMCDevCom
from IPMCDevLib import IPMCDevATCA as IPMCDevATCA

import lib.IPMI as IPMI

@sysapi.register_debug('IO Subtest')
def test_io(io, hostname='192.168.1.34', timeout=100, **kwargs):
    '''
    The I/O subtest restarts the ATCA blade and checks the functionality of the user I/Os,
    which includes the USR_IO and the IPMI_IOs.
    Therefore, it test for each pin the possibility to set it in output and drive the line
    to VCC or GND before setting it in input and checking the level detection.

    When the timeout value is exceeded during the ATCA reset, the test is considered to
    be failed.

    Args:
        io: can be an integer from 0 to 50 (USR: 0-34, IPM: 35-50) or a list of I/O to be tested
        timeout: Maximum time to wait for a function to be executed

    Returns:
        The function returns a list of dictionary formated to be used in the test instance.
        Each instance of the list is one subtest, linked to a specific I/O.

        .. code-block:: javascript

            [{
                'test_type_name': 'IO_v.1.0',
                'name': 'io_tester_{}'.format(io_id),

                'details': {
                    'io': io_id,
                    'timeout': timeout
                },

                'summary': {
                    'verbose': verbose,
                    'duration': round((time.time()-start),2)
                },

                'measurements': measurements,
                'pass': passed
            }, ...]

        Where pass can be:
            *  0: success
            * -1: ATCA reset failed
            * -2: Set GND failed
            * -3: Set VCC failed
            * -4: Get GND failed
            * -5: Get VCC failed
    '''

    debug_level = kwargs['debug_level'] if 'debug_level' in kwargs else 0
    IPMCDevObject = IPMCDevCom.IPMCDevCom(**kwargs)
    IPMCDevATCAObject = IPMCDevATCA.IPMCDevATCA(IPMCDevObject)

    if isinstance(io, list):
        io_list = io

    else:
        io_list = [int(io)]

    timeout = int(timeout)

    start = time.time()

    subtests_ret = []

    ipmiLan = IPMI.IPMI(hostname=hostname, debug = 1 if debug_level >= 10 else 0)

    i=0
    subtests = []
    IO_USR_MAX = 34

    for io_id in io_list:
        measurements = []
        passed = 0
        verbose = 'Test successfully passed'

        if io_id <= IO_USR_MAX:
            IO_SET_CMD = 0x44
            IO_CLR_CMD = 0x46
            IO_GET_CMD = 0x48
            io_typ = 'USR'
            io_dev = io_id
            print('[Info] USR I/O {}'.format(io_id))
        else:
            IO_SET_CMD = 0x45
            IO_CLR_CMD = 0x47
            IO_GET_CMD = 0x49
            io_typ = 'IPM'
            io_dev = io_id
            io_id -= (IO_USR_MAX + 1)
            print('[Info] IPM I/O {}'.format(io_id))

        #Activate I/O
        ipmiLan.ipmi_send(0x2e, IO_SET_CMD, [96, 0, 0, io_id])
        r = IPMCDevATCAObject.GETIO(io_dev)

        measurements.append({'name': 'set_io', 'data': {'s': 'Set GND (act)', 'v': r}, 'pass': 0 if r == 0 else -1 })
        passed = passed if r == 0 else -1
        verbose = verbose if r == 0 else 'Set GND failed'

        #Deactivate I/O
        ipmiLan.ipmi_send(0x2e, IO_CLR_CMD, [96, 0, 0, io_id])
        r = IPMCDevATCAObject.GETIO(io_dev)

        measurements.append({'name': 'set_io', 'data': {'s': 'Set VCC (deact)', 'v': r}, 'pass': 0 if r == 1 else -1 })
        passed = passed if r == 1 else -2
        verbose = verbose if r == 1 else 'Set VCC failed'

        #get activated I/O
        ipmiLan.ipmi_send(0x2e, IO_GET_CMD, [96, 0, 0, io_id]) #Configure I/O in input
        IPMCDevATCAObject.SETIO(io_dev, IPMCDevATCAObject.GND)

        try:
            r = ipmiLan.ipmi_send(0x2e, IO_GET_CMD, [96, 0, 0, io_id])
            io_v = r[3]
            measurements.append({'name': 'get_io', 'data': {'s': 'Get GND (act)', 'v': io_v}, 'pass': 0 if io_v == 1 else -1 })
            passed = passed if io_v == 1 else -3
            verbose = verbose if io_v == 1 else 'Get GND failed'

        except Exception as e:
            measurements.append({'name': 'get_io', 'data': {'s': 'Get GND (act)', 'v': -1}, 'pass': -2 })
            passed = -6
            verbose = 'IPMI issue (r: {}) - {})'.format(r, str(e))
            print(verbose)


        #get deactivated I/O
        ipmiLan.ipmi_send(0x2e, IO_GET_CMD, [96, 0, 0, io_id]) #Configure I/O in input
        IPMCDevATCAObject.SETIO(io_dev, IPMCDevATCAObject.VCC)

        try:
            r = ipmiLan.ipmi_send(0x2e, IO_GET_CMD, [96, 0, 0, io_id])
            io_v = r[3]
            measurements.append({'name': 'get_io', 'data': {'s': 'Get VCC (deact)', 'v': io_v}, 'pass': 0 if io_v == 0 else -1 })
            passed = passed if io_v == 0 else -4
            verbose = verbose if io_v == 0 else 'Get VCC failed'

        except Exception as e:
            measurements.append({'name': 'get_io', 'data': {'s': 'Get VCC (deact)', 'v': -1}, 'pass': -2 })
            passed = -6
            verbose = 'IPMI issue (r: {}) - {})'.format(r, str(e))
            print(verbose)


        subtests.append({
                'test_type_name': 'IO_v.1.0',
                'name': 'io_tester_{}_{}'.format(io_typ,io_id),

                'details': {
                    'typ': io_typ,
                    'io': io_id,
                    'timeout': timeout
                },

                'summary': {
                    'verbose': verbose,
                    'duration': round((time.time()-start),2)
                },

                'measurements': measurements,
                'pass': passed
            })

        i += 1

    IPMCDevObject.close()
    ipmiLan.close()

    return subtests

if __name__ == "__main__":
    io_list = range(0,51)
    pprint.pprint(test_io(list(io_list), hardware_address=0x43, timeout = 100))
