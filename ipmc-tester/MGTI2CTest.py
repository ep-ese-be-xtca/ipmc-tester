'''
The MGT I2C subtest validate the functionality of the MGT i2c bus located on the edge connector.
Therefore, it uses an eeprom memory located on the tester card to perform read and write
transactions. It executes the rd/wr multiple times on different registers (generated randomly).
In order to randomize the test and check as many combination as possible, the value writen is also
a random byte.

The pass value depends on the test result:
    *  0: success
    * -1: ATCA init failed
    * -2: IPMI error
    * -3: Readback error
'''

#!/usr/bin/env python
# encoding: utf-8
import time
import pprint
import random

from IPMCDevLib import IPMCDevCom as IPMCDevCom
from IPMCDevLib import IPMCDevATCA as IPMCDevATCA

import lib.IPMI as IPMI
import lib.sysapi as sysapi

@sysapi.register_debug('I2C MGT Subtest')
def test_i2c_mgt(count, hardware_address=0xc1, hostname='192.168.1.34', timeout = 100, **kwargs):
    '''
    The MGT I2C subtest validate the functionality of the MGT i2c bus located on the edge connector.
    Therefore, it uses an eeprom memory located on the tester card to perform read and write
    transactions. It executes the rd/wr multiple times on different registers (generated randomly).
    In order to randomize the test and check as many combination as possible, the value writen is also
    a random byte.

    When the timeout value is exceeded during the ATCA reset procedure, the test is considered to
    be failed.

    Args:
        count: number of rd/wr process to be executed
        hardware_address: ATCA Hardware address (default: 0x43)
        timeout: Maximum time to wait for a function to be executed

    Returns:
        The function returns a dictionary formated to be used in the test instance.

        .. code-block:: javascript

            {
                'test_type_name': 'I2CMGT_v.1.0',
                'name': 'i2c_mgt_tester',

                'details': {
                    'count': int(count),
                    'hardware_address': hardware_address,
                    'timeout': timeout
                },

                'summary': {
                    'good_transactions': good_transactions,
                    'bad_transactions': (int(count) - good_transactions),
                    'register_tested': len(addresses),
                    'duration': round((time.time()-start),2)
                },

                'measurements': measurements,
                'pass': passed
            }

        Where pass can be:
            *  0: success
            * -1: ATCA init failed
            * -2: IPMI error
            * -3: Readback error
    '''

    IPMCDevObject = IPMCDevCom.IPMCDevCom(**kwargs)
    IPMCDevATCAObject = IPMCDevATCA.IPMCDevATCA(IPMCDevObject)

    start = time.time()

    hardware_address = int(hardware_address)
    timeout = int(timeout)

    #pwr_off_timeout, pwr_on_timeout = 0, 0

    passed = 0
    measurements = []

    ipmiLan = IPMI.IPMI(hostname=hostname)

    good_transactions = 0

    addresses = []

    print('[Info] Test {} registers'.format(count))

    for i in range(0,int(count)):
        addrMSB = random.randint(0, 254)
        addrLSB = random.randint(0, 254)
        val = random.randint(0, 254)
        r = ipmiLan.ipmi_send(46, 0x50, [96, 0, 0, addrMSB, addrLSB, val])

        if len(r) != 4:
            IPMCDevObject.close()
            ipmiLan.close()

            return {
                'test_type_name': 'I2CMGT_v.1.0',
                'name': 'i2c_mgt_tester',

                'details': {
                    'count': int(count),
                    'hardware_address': hardware_address,
                    'timeout': timeout
                },

                'summary': {
                    'good_transactions': good_transactions,
                    'bad_transactions': (int(count) - good_transactions),
                    'register_tested': len(addresses),
                    'duration': round((time.time()-start),2)
                },

                'measurements': measurements,
                'pass': -2
            }

        i2c_v = r[3]

        tmp = addrMSB << 8 | addrLSB
        if tmp not in addresses:
            addresses.append(tmp)

        if i2c_v != val:
            passed = -3
        else:
            good_transactions += 1

        measurements.append({'name': 'memory_i2c', 'data': {'addr': tmp, 'wr': val, 'rd': i2c_v}, 'pass': 0 if i2c_v == val else -1 })

    IPMCDevObject.close()
    ipmiLan.close()

    return {
        'test_type_name': 'I2CMGT_v.1.0',
        'name': 'i2c_mgt_tester',

        'details': {
            'count': int(count),
            'hardware_address': hardware_address,
            'timeout': timeout
        },

        'summary': {
            'good_transactions': good_transactions,
            'bad_transactions': (int(count) - good_transactions),
            'register_tested': len(addresses),
            'duration': round((time.time()-start),2)
        },

        'measurements': measurements,
        'pass': passed
    }

if __name__ == "__main__":
    pprint.pprint(test_i2c_mgt(1000, hardware_address=0x43, timeout = 100))
