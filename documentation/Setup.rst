CERN-IPMC Tester - Setup
************************

The CERN-IPMC tester v1 is made of 3 independant modules: a shelf manager evaluation kit, the tester board
and a raspberrypi. The latest is responsible for the control and test execution while the tester board
connects all of the module's interfaces and the shelf manager evaluation kit emulates a crate environment.

The picture below shows the setup:

.. thumbnail:: _static/setup.png
   :align:   center

   CERN-IPMC tester


.. toctree::
   :maxdepth: 2

Tester board
============

An automatic tester system was designed to verify the IPMC functionality after production.
To get a complete environment, which can be used for either automatic test or manual debugging,
all of the interfaces of the mezzanine cards are emulated. The figure below shows the hardware architecture
of the tester.

.. thumbnail:: _static/tester_blockdiag.png
   :align:   center

   CERN-IPMC tester - block diagram

The system is supervised by an 8bit microcontroller managed by a PC via a USB to serial
interface. A Python library was designed to execute commands for the monitoring and control of
the IPMC interfaces. The library provides different functions to configure the CPLD used for the
dynamic routing of the GPIOs and AMC ports. All of the AMC ports are tested using an emulated
mezzanine card with an on-board MMC module. Finally, the I2C interfaces are connected to onboard sensors and EEPROM to check their functionality. Serial, Ethernet and JTAG ports are
routed to connectors for functional check using external devices (not included in the kit).

.. thumbnail:: _static/tester_hw.png
   :align:   center

   CERN-IPMC tester board

The input power comes from the J5 connector located
on the bottom left corner of the IPMC tester. It must be supplied with a 5 volts / 1Amp DC
source connected as shown below:

.. thumbnail:: _static/tester_power.png
   :align:   center

   CERN-IPMC tester power


Raspberry pi
============

The Raspberry pi used in the current version of the setup is a version 3. It runs the software used to test the modules.
It is connected to the CERN network via wifi and to the tester via Ethernet, USB and GPIOs.

The pi's connector is used to power the module via the tester's supply and for the JTAG interface. The figure below shows
the connectivity.

.. thumbnail:: _static/pi_connection.png
   :align:   center

   Raspberry pi connectivity
