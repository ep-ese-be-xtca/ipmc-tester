
.. mdinclude:: ../README.md

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Test(s)

   CERN-IPMC General test <_gen/IPMCTest>

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Debuging

   AMC Subtest <_gen/AMCSlotTester>
   Program <_gen/ATCAProgrammer>
   ATCA Subtest <_gen/ATCATester>
   IO Subtest <_gen/IOTest>
   MAC Address Subtest <_gen/MACTester>
   I2C MGT Subtest <_gen/MGTI2CTest>
   Power check <_gen/Power>
   I2C Sensor Subtest <_gen/SENSI2CTest>

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Setup

   Overview <Setup>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

