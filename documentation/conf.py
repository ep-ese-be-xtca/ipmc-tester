# -*- coding: utf-8 -*-
#
# Configuration file for the Sphinx documentation builder.
#
# This file does only contain a selection of the most common options. For a
# full list see the documentation:
# http://www.sphinx-doc.org/en/master/config

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
from sphinx.ext.autosummary import Autosummary
from sphinx.ext.autosummary import get_documenter
from docutils.parsers.rst import directives
from sphinx.util.inspect import safe_getattr
import re
import PSMSFramework
import ast
import importlib

def generate(testname, packagename, type=0):

    packagename = packagename.replace('ipmc-tester.','')
    f = open("_gen/{}.rst".format(packagename.replace('.','_')), "w")
    m = importlib.import_module(packagename)
    filename = m.__file__

    with open(filename) as file:
        node = ast.parse(file.read())

    functions = [n for n in node.body if isinstance(n, ast.FunctionDef)]

    items = []
    for fn in functions:
        items.append('    {} <{}.{}>'.format(fn.name, packagename, fn.name))

    f.write('''{}
========================

.. automodule:: {}

.. rubric:: List of functions:

.. autosummary::
    :toctree:_stubs/

{}
    '''.format(testname, packagename, '\n'.join(items)))
    f.close()

    return '   {} <_gen/{}>'.format(t['name'], packagename.replace('.','_'))


import os
import sys
sys.path.insert(0, os.path.abspath('..'))
sys.path.insert(0, os.path.abspath('../ipmc-tester'))

for (dirpath, dirnames, filenames) in os.walk('../ipmc-tester'):
    for f in [os.path.join(dirpath, file) for file in filenames]:
        if f[-3:] == '.py' and 'sysapi.py' not in f and 'conf.py' not in f:

            package = f[:-3]
            package = package.replace('.\\','')
            package = package.replace('./','')
            package = package.replace('.','')
            package = package.replace('/','.')
            package = package.replace('\\','.')
            try:
                command_module = __import__(package)
            except Exception as e:
                print('Import {} failed! -> {} '.format(package, e))


dbglist_tmp = PSMSFramework.sysapi.getDebugList()
dbglist=[]
names = []

for t in dbglist_tmp:
    if t['name'] not in names:
        dbglist.append(t)
        names.append(t['name'])

toctreedbg = []

for t in dbglist:
    #supervisor.add(self.job, t['function'], t['name'], type=supervisor.TESTING)
    print('[DBG] Generate .rst for {}'.format(t['function'].__module__))
    lnk = generate(t['name'], t['function'].__module__)
    toctreedbg.append(lnk)

testlist_tmp = PSMSFramework.sysapi.getTestList()
testlist=[]

names = []

for t in testlist_tmp:
    if t['name'] not in names:
        testlist.append(t)
        names.append(t['name'])

toctreetest = []

for t in testlist:
    #supervisor.add(self.job, t['function'], t['name'], type=supervisor.TESTING)
    print('[TST] Generate .rst for {}'.format(t['function'].__module__))
    lnk = generate(t['name'], t['function'].__module__)
    toctreetest.append(lnk)


indexfile = '''
.. include:: ../README.md

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Test(s)

{}

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Debuging

{}

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Setup

   Overview <Setup>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

'''.format('\n'.join(toctreetest), '\n'.join(toctreedbg))

f = open("index.rst", "w")
f.write(indexfile)
f.close()

# -- Project information -----------------------------------------------------

project = u'IPMC tester'
copyright = u'2020, Mendez Julian'
author = u'Mendez Julian'

# The short X.Y version
version = u''
# The full version, including alpha/beta/rc tags
release = u''


# -- General configuration ---------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
#
# needs_sphinx = '1.0'

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.doctest',
    'sphinx.ext.todo',
    'sphinx.ext.mathjax',
    'sphinx.ext.ifconfig',
    'sphinx.ext.viewcode',
    'sphinx.ext.napoleon',
    'sphinx.ext.graphviz',
    'sphinx.ext.autosummary',
    'sphinxcontrib.images',
    'myst_parser'
]

autosummary_generate = True
autosummary_generate_overwrite = True

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
#
source_suffix = ['.rst', '.md']


# The master toctree document.
master_doc = 'index'

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#
# This is also used if you do content translation via gettext catalogs.
# Usually you set "language" from the command line for these cases.
language = None

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = [u'_build', 'Thumbs.db', '.DS_Store']

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = None


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'alabaster'

html_theme_options = {
    'sidebar_width': '250px',
    'page_width': '970px'
}
# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#
# html_theme_options = {}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# Custom sidebar templates, must be a dictionary that maps document names
# to template names.
#
# The default sidebars (for documents that don't match any pattern) are
# defined by theme itself.  Builtin themes are using these templates by
# default: ``['localtoc.html', 'relations.html', 'sourcelink.html',
# 'searchbox.html']``.
#
# html_sidebars = {}

# -- Options for HTMLHelp output ---------------------------------------------

# Output file base name for HTML help builder.
htmlhelp_basename = 'vldbtesterdoc'


# -- Options for LaTeX output ------------------------------------------------

latex_elements = {
    # The paper size ('letterpaper' or 'a4paper').
    #
    # 'papersize': 'letterpaper',

    # The font size ('10pt', '11pt' or '12pt').
    #
    # 'pointsize': '10pt',

    # Additional stuff for the LaTeX preamble.
    #
    # 'preamble': '',

    # Latex figure (float) alignment
    #
    # 'figure_align': 'htbp',
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
    (master_doc, 'vldbtester.tex', u'vldb+ tester Documentation',
     u'Mendez Julian', 'manual'),
]


# -- Options for manual page output ------------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    (master_doc, 'vldbtester', u'vldb+ tester Documentation',
     [author], 1)
]


# -- Options for Texinfo output ----------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
    (master_doc, 'vldbtester', u'vldb+ tester Documentation',
     author, 'vldbtester', 'One line description of project.',
     'Miscellaneous'),
]


# -- Options for Epub output -------------------------------------------------

# Bibliographic Dublin Core info.
epub_title = project

# The unique identifier of the text. This can be a ISBN number
# or the project homepage.
#
# epub_identifier = ''

# A unique identification for the text.
#
# epub_uid = ''

# A list of files that should not be packed into the epub file.
epub_exclude_files = ['search.html']


# -- Extension configuration -------------------------------------------------

# -- Options for todo extension ----------------------------------------------

# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = True
