# CERN-IPMC Tester

This repository contains the files required to configure all of the programmable devices located on the CERN-IPMC Tester boad:

- uMGT Controller: <a href="https://gitlab.cern.ch/ep-ese-be-xtca/ipmc-tester/tree/master/setup-config/umgt-firmware">setup-config/umgt-firmware</a>
- MMC Emulator: <a href="https://gitlab.cern.ch/ep-ese-be-xtca/ipmc-tester/tree/master/setup-config/mmc-firmware">setup-config/mmc-firmware</a>
- CPLD: <a href="https://gitlab.cern.ch/ep-ese-be-xtca/ipmc-tester/tree/master/setup-config/cpld-firmware">setup-config/cpld-firmware</a>

In addition, it contains the binary files required to flash the CERN-IPMC mezzanine card with a specific software and the latest version of the FPGA firmware:

- IPMC binary files and documentation (using FlashPRO): <a href="https://gitlab.cern.ch/ep-ese-be-xtca/ipmc-tester/tree/master/ipmc-config">ipmc-config</a>
- Stapl file that can be used with the jtag-player utility for raspberry pi.

Finally, it contains the python scripts used to test the CERN-IPMC modules

## Requirements

This python library was designed for python >= 3.4

## Dependencies

List of dependencies:
- <b>ipmc-devkit</b> (https://gitlab.cern.ch/ep-ese-be-xtca/ipmc-devkit.git)
- <b>wiringX</b> (https://gitlab.cern.ch/ep-ese-be-xtca/wiringx.git)
- <b>python pprint</b>

## Prerequisites

Set up the USB interfaces for the IPMC tester's uManagement interface and the IPMC's serial debug interface. Find the identifiers, add them to a udev rules file, reload the rules and trigger udev:

```
sudo udevadm info /dev/ttyUSB0
sudo udevadm info /dev/ttyUSB1
sudo udevadm info /dev/ttyUSB2
```
Note: take from the output of these commands the proper "AB0..." strings and use them in the 99-serial.rules file below
```
sudo cat > /etc/udev/rules.d/99-serial.rules << EOF
SUBSYSTEM=="tty", ENV{ID_SERIAL_SHORT}=="AB0KOOS9", ATTRS{bInterfaceNumber}=="00", GROUP="zp", SYMLINK+="ipmc-umgt"
SUBSYSTEM=="tty", ENV{ID_SERIAL_SHORT}=="AB0KOOS7", ATTRS{bInterfaceNumber}=="00", GROUP="zp", SYMLINK+="ipmc-debug"
SUBSYSTEM=="tty", ENV{ID_SERIAL_SHORT}=="AB0KOOSA", ATTRS{bInterfaceNumber}=="00", GROUP="zp", SYMLINK+="ipmc-payload"
EOF

sudo udevadm control --reload-rules && sudo udevadm trigger
```

Install the IPMC Tester and Development Kit software:

```
#In order to fetch LFS (binary) files automatically:
apt-get install git-lfs
git lfs install Git LFS initialized.

git clone https://gitlab.cern.ch/ep-ese-be-xtca/ipmc-tester.git
cd ipmc-tester
git submodule update --init --recursive
cd ipmc-devkit
python3 setup.py install --user
cd ../..
```

## Running

Run the IPMC Test software, which is composed of a number of sub-tests:

```
cd ipmc-tester
python3 IPMCTest.py -b '10184002A' -s '24020270' -m '80:d3:36:00:43:60' -p -P
```

The parameters have the following meaning:
```
    -b '10184002A'          batch name to be written to EEPROM  and FRU
    -s '2402027'            serial number to be written to EEPROM and FRU
    -m '80:d3:36:00:43:60'  MAC address to be written to EEPROM (and used by LAN of IPMC user firmware)
    -p                      program the IPMC tester firmware (in order to run the tests)
    -P                      program the IPMC user firmware (at the end of the tests for shipping)
```

## Notes concerning the Radxa Rock4

The JTAG player and the wiringX library need to be built as a 32-bit executable on the Radxa Rock4 target. For this
the 32-bit toolchain needs to be installed as follows:
```
sudo apt install gcc make gcc-arm-linux-gnueabi binutils-arm-linux-gnueabi
```

Clone the wiringX repository `https://gitlab.cern.ch/ep-ese-be-xtca/wiringx.git` and follow the build instructions below:
```
mkdir build; cd build
cmake -DCMAKE_C_COMPILER=arm-linux-gnueabi-gcc ..
make && sudo make install
```

Build the JTAG player in `jtag-player` as follows:
```
make CC=arm-linux-gnueabi-gcc LDFLAGS=-static
sudo chown root jtag-player
sudo chmod +s jtag-player
```

## MAC Addresses for CERN IPMC

CERN MAC addresses reserved for the IPMC:

```
80-D3-36-00-40-00 to
80-D3-36-00-47-FF (2k addresses)
```
