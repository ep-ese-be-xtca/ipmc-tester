# CERN-IPMC Tester

23-AUG-2024, R. Spiwoks, WORK IN PROGRESS - TO BE COMPLETED!

## Preparations

### Software

Log in to the IPMC testers and open two windows, one for the IPMC debug console and one for running the IPMC tests:
```
ssh ipmc-tester21 -l ipmc

# in the first window
minicom -D /dev/ipmc-debug

# in the second window
cd work/ipmc-tester
```

The IPMC debug console prints out debug messagess of the IPMC. If the terminal gets stuck, you can quit using "Ctrl-X A" and restart it.

### Hardware


Remove the jumper SW1 (close to the Raspberry Pi) to power off the IPMC. Remove the IPMC from the connector.
Insert a new IPMC into the connector making sure that it is well seated.
Power on the IPMC by placing the jumper SW1 (close to the Raspberry Pi).

## Running the tests

Run the suite of IPMC tests:
```
python ipmc-tester/IPMCTest.py -p
```

If the tests finish with "All tests requested finished successfully", the IPMC is good. If one or more of the tests fail. Repeat the tests once more. If there are still failures mark the IPMC as bad and continue with testing another IPMC.

## Configuring the IPMC

### Loading a new firmware
Load a different firmware used for configuring and shipping the IPMC:
```
ipmitool -H 192.168.1.34 -U '' -P '' hpm upgrade /home/joos/mj_temporary.img force
ipmitool -H 192.168.1.34 -U '' -P '' hpm activate
```

Wait approximately 5 seconds, or until on the IPMC debug console you see the message "<\_>: CORE\_10100: Link is UP, 100Mb FD"
You can also test if the IPMC is ready by running the following command:
```
ipmitool -I lan -H 192.168.1.34 -U "" -P "" lan print
```

If the IPMC still does not respond, repeat the command. If it still does not respond see "Trouble shooting" below.

### Writing IPMC identifiers

Read the serial number from the IPMC (also written on the bag), and the MAC address from the Excel sheet. Then write the identifiers using the following command:
```
python ipmc-tester/ipmc ... TO BE DONE!
```

## Update the inventory

Enter the test results in the Excel sheet: "OK" or "Bad"

## Trouble shooting

\- Repeat setting of hardware address and closing handle switch on the IPMC tester:
```
devkit_ctrl -a set_atca_ha -v 0x43
devkit_ctrl -a set_atca_hs -s closed
```

\- Check all cable connections and if the Raspberry Pi and the IPMC are well seated.

\- Power off the power supply of the IPMC tester and power it on again.

\- Power off the power supplies of the IPMC tester and the ShelfManager. Power on the power supply of the ShelfMananger first, and then the power supply of the IPMC tester.
