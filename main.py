#!/usr/bin/env python
# -*- coding: utf-8 -*-
import PSMSFramework
import sys
import os

sys.path.insert(0, os.path.abspath('ipmc-tester'))

if __name__ == "__main__":
    try:
        host = os.environ['IPMC_DB_HOST']
        user = os.environ['IPMC_DB_USERNAME']
        port = int(os.environ['IPMC_DB_PORT'])
        passwd = os.environ['IPMC_DB_PASSWD']
        dbname = os.environ['IPMC_DB_DBNAME']
    except:
        host = None
        user = None
        port = None
        passwd = None
        dbname = None

    PSMSFramework.PSMSFramework(
        title='<b>CERN-IPMC</b> Tester',
        tests_root='ipmc-tester',

        db_host=host,
        db_username=user,
        db_port=port,
        db_password=passwd,
        db_dbname=dbname
    )
