/*
 * LTC2990_sensor.h
 *
 * Created: 24/05/2017 10:33:02
 *  Author: jumendez
 */ 


#ifndef LTC2990_SENSOR_H_
#define LTC2990_SENSOR_H_

#define LTC2990_STATUS_REGISTER		0x0 
#define LTC2990_CONTROL_REGISTER	0x1
#define LTC2990_TRIGGER_REGISTER	0x2

unsigned int LTC2990_voltage_getvalue(unsigned char i2c_addr, unsigned char opMode, unsigned char channelAddress);
unsigned int LTC2990_current_getvalue(unsigned char i2c_addr, unsigned char opMode, unsigned char channelAddress, float shuntResistorValue, float scaleFactor);

#endif /* LTC2990_SENSOR_H_ */