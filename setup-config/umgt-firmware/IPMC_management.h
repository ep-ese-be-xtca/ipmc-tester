/*
 * IPMC_management.h
 *
 * Created: 08/05/2017 12:02:24
 *  Author: jumendez
 */ 


#ifndef IPMC_MANAGEMENT_H_
#define IPMC_MANAGEMENT_H_

void set_hardware_addr(unsigned char addr);
char get_hardware_addr(void);
void set_handle_switch();
void clear_handle_switch();
void reset_IPMC();
unsigned char get_blue_led_status();
unsigned char get_led1_status();
unsigned char get_led2_status();
unsigned char get_led3_status();
unsigned char get_12vEn_status();

#endif /* IPMC_MANAGEMENT_H_ */