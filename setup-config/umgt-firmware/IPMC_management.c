/*
 * IPMC_management.c
 *
 * Created: 08/05/2017 11:04:36
 *  Author: jumendez
 */ 

#include <avr/io.h>
#include <stdio.h>

#include "IPMC_management.h"
#include "gpio_intf.h"
#include "pinout.h"
#include "tools.h"

void set_hardware_addr(unsigned char addr)
{
	// set the HA pin output value to 0x00
	PORTA = 0x00;
	
	// pull the output low if the HA bit is zero, else leave high-impedance (input).
	set_signal_dir(HA0, (addr & 0x01) ? INPUT : OUTPUT);
	set_signal_dir(HA1, (addr & 0x02) ? INPUT : OUTPUT);
	set_signal_dir(HA2, (addr & 0x04) ? INPUT : OUTPUT);
	set_signal_dir(HA3, (addr & 0x08) ? INPUT : OUTPUT);
	set_signal_dir(HA4, (addr & 0x10) ? INPUT : OUTPUT);
	set_signal_dir(HA5, (addr & 0x20) ? INPUT : OUTPUT);
	set_signal_dir(HA6, (addr & 0x40) ? INPUT : OUTPUT);
	set_signal_dir(HA7, (addr & 0x80) ? INPUT : OUTPUT);
}

/**************************/
char get_hardware_addr(void)
/**************************/
{
	unsigned char bit0, bit1, bit2, bit3, bit4, bit5, bit6, bit7, addr;
	
	// Set the HA bits to input mode
	//DDRA = 0x00;

	bit0 = get_signal(HA0);
	bit1 = get_signal(HA1);
	bit2 = get_signal(HA2);
	bit3 = get_signal(HA3);
	bit4 = get_signal(HA4);
	bit5 = get_signal(HA5);
	bit6 = get_signal(HA6);
	bit7 = get_signal(HA7);
	
	addr = bit0 + (bit1 << 1) + (bit2 << 2) + (bit3 << 3) + (bit4 << 4) + (bit5 << 5) + (bit6 << 6) + (bit7 << 7); 
	
	return(addr);
}

void set_handle_switch(){
	unsigned char portc_v = PORTC;
	portc_v &= 0xFD;
	PORTC = portc_v;
}

void clear_handle_switch(){
	unsigned char portc_v = PORTC;
	portc_v |= 0x02;
	PORTC = portc_v;
}

unsigned char get_blue_led_status(){
	return (PINF & 0x01) ? 0xFF : 0x00;
}


/*****************************/
unsigned char get_led1_status()
/*****************************/
{
  return (PINF & 0x02) ? 0xFF : 0x00;
}


/*****************************/
unsigned char get_led2_status()
/*****************************/
{
  return (PINF & 0x04) ? 0xFF : 0x00;
}


/*****************************/
unsigned char get_led3_status()
/*****************************/
{
  return (PINF & 0x08) ? 0xFF : 0x00;
}

unsigned char get_12vEn_status(){
	return (PINC & 0x04) ? 0xFF : 0x00;
}