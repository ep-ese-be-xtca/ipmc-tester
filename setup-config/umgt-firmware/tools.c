/*
 * tools.c
 *
 * Created: 08/05/2017 12:05:40
 *  Author: jumendez
 */ 
#include "tools.h"

// Delay for a minimum of <us> microseconds
// The time resolution is dependent on the time the loop takes
// e.g. with 4Mhz and 5 cycles per loop, the resolution is 1.25 us
//***********************/
void delay_us(unsigned short time_us)     //Called from ipmi_if.c and fru.c
//***********************/
{
	unsigned short delay_loops;
	register unsigned short i;

	// one loop takes 5 cpu cycles
	delay_loops = (time_us + 3) / 22 * 3 * CYCLES_PER_US; // +3 for rounding up (dirty)  //MJ: "22 * 3" is the result of a manual calibration of the delay

	for (i=0; i < delay_loops; i++)
		asm volatile("nop");          //"NOP" ensure that the loop is not removed by the optimizer
}