/*
 * i2c_intf.c
 *
 * Created: 24/05/2017 09:52:25
 *  Author: jumendez
 */ 
/*! \file i2csw.c \brief Software I2C interface using port pins. */
//*****************************************************************************
//
// File Name	: 'i2csw.c'
// Title		: Software I2C interface using port pins
// Author		: Pascal Stang
// Created		: 11/22/2000
// Revised		: 5/2/2002
// Version		: 1.1
// Target MCU	: Atmel AVR series
// Editor Tabs	: 4
//
// Modified by  : Markus Joos (markus.joos@cern.ch)
// Modified by  : Julian Mendez (julian.mendez@cern.ch)
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//
//*****************************************************************************

#include <avr/io.h>

#include "gpio_intf.h"
#include "i2c_intf.h"

unsigned char sda_pin, scl_pin;

//******************/
unsigned char i2cPutbyte(unsigned char b)
//******************/
{
	short i;
	
	for (i = 7; i >= 0; i--)
	{
		if (b & (1 << i))
		I2C_SDL_HI;
		else
		I2C_SDL_LO;

		I2C_SCL_TOGGLE
	}

	I2C_SDL_HI;
	HDEL
	I2C_SCL_HI;
	
	b = get_signal(sda_pin);
	
	HDEL
	I2C_SCL_LO;
	HDEL
	
	return (b == 0);
}


//**********************/
unsigned char i2cGetbyte(unsigned int last)
//**********************/
{
	short i;
	unsigned char c, b = 0;

	I2C_SDL_HI;
	
	for (i = 7; i >= 0; i--)
	{
		HDEL
		I2C_SCL_HI; 
		
		c = get_signal(sda_pin);
		b <<= 1;
		if (c)
		b |= 1;
		
		HDEL
		I2C_SCL_LO;
	}

	set_signal_dir(sda_pin, OUTPUT);
	
	if (last)
	I2C_SDL_HI;
	else
	I2C_SDL_LO;

	I2C_SCL_TOGGLE
	I2C_SDL_HI;
	return(b);
}


//************************
//* I2C public functions *
//************************

//*****************/
void i2c_init(unsigned char sda_pin_i, unsigned char scl_pin_i)
//*****************/
{
	sda_pin = sda_pin_i;
	scl_pin = scl_pin_i;
	
	clear_signal(sda_pin);
	clear_signal(scl_pin);
	set_signal_dir(sda_pin, INPUT);
	set_signal_dir(scl_pin, INPUT);
	
	I2C_STOP
}


//*********************************************************************/
void ext_i2c_send(unsigned char device, unsigned short subAddr, unsigned char nsub, unsigned char length, unsigned char *data)
//*********************************************************************/
{
	I2C_START
	i2cPutbyte(device << 1);
	
	if(nsub == 2)
	{
		i2cPutbyte(subAddr >> 8);
		i2cPutbyte(subAddr & 0xff);
	}
	else if (nsub == 1)
	i2cPutbyte(subAddr & 0xFF);
	
	while (length--)
	i2cPutbyte(*data++);
	
	I2C_SDL_LO;
	I2C_STOP
}

//************************************************************************/
unsigned char ext_i2c_received(unsigned char device, unsigned short subAddr, unsigned char nsub, unsigned char length, unsigned char *data)
//************************************************************************/
{
	
	unsigned char j, device_address = device << 1;
		
	I2C_START

	if(nsub == 2)
	{
		i2cPutbyte(device_address);
		i2cPutbyte(subAddr >> 8);
		i2cPutbyte(subAddr);
	}
	else if (nsub == 1)
	{
		i2cPutbyte(device_address);
		i2cPutbyte(subAddr);
	}

	HDEL

	if(nsub != 0)
	{
		I2C_SCL_HI;
		I2C_START
	}

	i2cPutbyte(device_address | READ);

	for (j = 0; j < length; j++)
	data[j] = i2cGetbyte(j == (length - 1));

	I2C_SDL_LO;
	I2C_STOP
	
	return 0x00;
}

unsigned char ext_i2c_poll(unsigned char addr){
	unsigned char ack_ok = 0;
	
	I2C_START
	ack_ok = i2cPutbyte(addr << 1);
	I2C_SDL_LO;
	I2C_STOP
	
	return ack_ok;
}