/*
 * LTC2990_sensor.c
 *
 * Created: 24/05/2017 10:28:14
 *  Author: jumendez
 */
#include <math.h> 

#include "i2c_intf.h" 
#include "LTC2990_sensor.h"
#include <stdio.h>

unsigned int LTC2990_voltage_getvalue(unsigned char i2c_addr, unsigned char opMode, unsigned char channelAddress){
	
	unsigned char ltc2990_trigger = 0xFF;
	unsigned char xRes[2];
	unsigned char conversion_status[1];
	unsigned char timeout = 0;
	unsigned int fullResolutionInt = 0;
	//float fullResolutionVoltage = 0.0;
	
	ext_i2c_send(i2c_addr, LTC2990_CONTROL_REGISTER, 1, 1, &opMode);	//LTC2990 operation mode configuration
	conversion_status[0] = 1;
	ext_i2c_send(i2c_addr, LTC2990_TRIGGER_REGISTER, 1, 1, &ltc2990_trigger);	//Trigger a conversion
	
	ext_i2c_received(i2c_addr, LTC2990_STATUS_REGISTER, 1, 1, conversion_status);
	
	timeout = 0;
	while((conversion_status[0] & 0x01) && (timeout < 1000)){
		ext_i2c_received(i2c_addr, LTC2990_STATUS_REGISTER, 1, 1, conversion_status); // while bit0 not 0 i.e. conversion in progress, keep reading the status 
		timeout++; 	
	} 
	
	ext_i2c_received(i2c_addr, channelAddress, 1, 2, xRes); 		// Single-ended voltage measurement LSB = 305.18uV 
	
	// Calculate the voltage with the full 14-bit ADC resolution 
	// Return an 8-bit Integer with step size of 0.02 V 
	// Max possible Measured voltage is thus 5.1 V
	if ((xRes[0] & 0x80) && ~(xRes[0] & 0x40)){ 		// Check for DataValid = 1 	and Sign Bit = 0
		fullResolutionInt = ((xRes[0] & 0x3F) << 8) + xRes[1];	// Discard DV and Sign bits 			
		//fullResolutionVoltage = ( (float) fullResolutionInt * 305.18e-6 );
		//printf("fullResolutionInt: %d, fullResolutionVoltage: %f", fullResolutionInt, fullResolutionVoltage);
	}
	
	return fullResolutionInt;
}

unsigned int LTC2990_current_getvalue(unsigned char i2c_addr, unsigned char opMode, unsigned char channelAddress, float shuntResistorValue, float scaleFactor){ 	
		
	unsigned char ltc2990_trigger = 0xFF; 		
	unsigned char xRes[2]; 		
	unsigned char conversion_status[1]; 		
	unsigned char timeout = 0; 		
	unsigned int fullResolutionInt = 0; 		
	//float fullResolutionCurrent = 0.0; 		
	
	/************************************************************************/ 
	/* // config a/b cases 
	   // 0x1F => case 7, V1,V2,V3,V4 -> auto trigger 
	   // 0x5F => case 7, V1,V2,V3,V4 -> single trigger 
	   // 0x1E => case 6, V1-V2,V3-V4 -> auto trigger 
	   // 0x5E => case 6, V1-V2,V3-V4 -> single trigger 
	   // 0x18 => case 0, V1,T2       -> auto trigger 
	   // 0x58 => case 0, V1,T2       -> single trigger 
	   // 0x19 => case 1, I1,T2       -> auto trigger 
	   // 0x59 => case 1, I1,T2       -> single trigger
	                                                                         */ 
	/************************************************************************/						
	ext_i2c_send(i2c_addr, LTC2990_CONTROL_REGISTER, 1, 1, &opMode);
		
	//LTC2990 operation mode configuration 				
	conversion_status[0] = 1; 				
	ext_i2c_send(i2c_addr, LTC2990_TRIGGER_REGISTER, 1, 1, &ltc2990_trigger);	
	
	//Trigger a conversion 		
	ext_i2c_received(i2c_addr, LTC2990_STATUS_REGISTER, 1, 1, conversion_status); 		
	while(conversion_status[0] & 0x01){ 			
		ext_i2c_received(i2c_addr, LTC2990_STATUS_REGISTER, 1, 1, conversion_status); 
		// while bit0 not 0 i.e. conversion in progress, keep reading the status 			
		timeout++; 		
	} 		
	
	ext_i2c_received(i2c_addr, channelAddress, 1, 2, xRes); 		
	
	// Differential voltage measurement LSB = 19.42uV 
	// Calculate the current with the full 14-bit ADC resolution 
	// Take into account different Shunt Resistor values 
	// Return an 8-bit Integer with step size of 0.1 A 
	// Max possible Measured current is thus 25.5A 	
		
    // Check for DataValid = 1 	and Sign Bit = 0
	if ((xRes[0] & 0x80) && ~(xRes[0] & 0x40)){ 		
		fullResolutionInt = ((xRes[0] & 0x3F) << 8) + xRes[1];	
		// Discard DV and Sign bits 			
		//fullResolutionCurrent = ( (float) fullResolutionInt * 19.42e-6 * scaleFactor) / shuntResistorValue;	// I = V/R 
	}			
	
	return fullResolutionInt;
}