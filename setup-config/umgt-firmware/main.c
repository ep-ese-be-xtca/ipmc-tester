/*
 * uController.c
 *
 * Created: 21/04/2017 19:56:40
 * Author : jumendez
 */ 
#include <stdio.h>
#include <string.h>

#include <avr/io.h>
#include "uart.h"
#include "IPMC_management.h"
#include "CPLD_management.h"
#include "AMC_management.h"
#include "testpad_manager.h"
#include "gpio_intf.h"
#include "i2c_intf.h"

#include "LTC2990_sensor.h"

int main(void){
	
	unsigned char command[MAX_COMMAND_LEN];
	unsigned ipmc_hardware_address = 0x00000000;
	unsigned amc_port_select = 0x00000000;
	unsigned amc_ga = 0x00000000;
	unsigned io_id = 0x00000000;
	unsigned i2c_port = 0x00000000;
	
	//float ltc2990_value;
	unsigned int ltc2990_value;
	
    // Port A: Hardware address
	DDRA  = 0x00;	// Default pin mode: input
	PORTA = 0x00;	// Default value: x"00"
	
	DDRB  = 0x61;	// Port B: AMC management signals
	PORTB = 0x61;	//           (Default: PS1 in input mode (not connected), MP/PWR good to '1')
		
	DDRC  = 0xE2;	// Port C: General configuration
	PORTC = 0x62;	//			 (Default: HSWITCH opened and PowerGood_A/B to '1' (asserted))
	
	DDRD  = 0x30;	// Port D: Monitoring and AMC handle
	PORTD = 0x10;	//			 (Default: AMC Handle opened and Wr_config to '0')
	
	DDRE  = 0xFF;	// Port E: Configuration data byte
	PORTE = 0xFF;	//			 (Default value: 0xFF: reset CPLD)
	
	DDRF  = 0x00;	// Port F: LEDs management
	
	DDRG  = 0xFF;	// Port G: I2C selection and CLK signal for config
	PORTG = 0x01;	//			 (Default: IPMB-L selected)
	
	i2c_init(MGT_Mon_SDA, MGT_Mon_SCL);
	
	memset(command, 0, MAX_COMMAND_LEN);
	FILE *uart_fd = USART_Init();
	
    while(1){
		
		if(USART_wait_for_command(command) >= 0){
			
			if(!strncmp((const char *)command, "GETDEVICE", 9)){					// Get device, used to check the UART port: "GETDEVICE"
				 fprintf(uart_fd, "IPMCTP_CTRLER_V1.0\n\r");
			}
			 
			else if(!strncmp((const char *)command, "SETHA", 5)){					//Set IPMC HA: "SETHA 0x00"
				sscanf((const char *)(&command[8]),"%x", &ipmc_hardware_address);
				set_hardware_addr(ipmc_hardware_address);
				fprintf(uart_fd, "Set IPMC Hardware (0x%02x) \n\r", ipmc_hardware_address);
			}
			
			else if(!strncmp((const char *)command, "GETHA", 5)){					//Get the IPMC HA
				ipmc_hardware_address = get_hardware_addr();
				fprintf(uart_fd, "Get IPMC Hardware: 0x%02x\n\r", ipmc_hardware_address);
			}

			else if(!strncmp((const char *)command, "SETHSW", 6)){					// Set Handle switch: "SETHSW"
				set_handle_switch();
				fprintf(uart_fd, "Set Handle switch\n\r");
			}
			
			else if(!strncmp((const char *)command, "CLRHSW", 6)){					// Clear Handle switch: "CLRHSW"
				clear_handle_switch();
				fprintf(uart_fd, "Clear Handle switch\n\r");
			}
			
			else if(!strncmp((const char *)command, "SETAMCGA", 8)){				//Set IPMC HA: "SETAMCGA 0x00"
				sscanf((const char *)(&command[11]),"%x", &amc_ga);
				if(!set_amc_ga(amc_ga))
					fprintf(uart_fd, "Set AMC Geographical address (0x%02x)\n\r", amc_ga);
				else
					fprintf(uart_fd, "Error: unknown AMC address 0x%02x\n\r", amc_ga);
			}
			
			else if(!strncmp((const char *)command, "SELAMC", 6)){					//Select AMC slot: "SELAMC 0x00"
				sscanf((const char *)(&command[9]),"%x", &amc_port_select);
				select_amc_port((unsigned char) amc_port_select);
				fprintf(uart_fd, "Select AMC slot (%d) \n\r", amc_port_select);
			}
		
			else if(!strncmp((const char *)command, "SETPS1", 6)){					//Set PS1 to GND: "SETPS1"
				set_ps_gnd();
				fprintf(uart_fd, "Set AMC PS1# to GND \n\r");
			}
		
			else if(!strncmp((const char *)command, "CLRPS1", 6)){					//Set PS1 to GND: "SETPS1"
				set_ps_vcc();
				fprintf(uart_fd, "Set AMC PS1# to VCC \n\r");
			}
						
			else if(!strncmp((const char *)command, "GETSTATUS", 9)){				//Get AMC status "GETSTATUS"
				fprintf(uart_fd, "PIND: 0x%02x \n\r", get_amc_status());
			}
			
			else if(!strncmp((const char *)command, "CLOSEAMCHS", 10)){				//Get AMC status "CLOSEAMCHS"
				close_AMC_handle();
				fprintf(uart_fd, "AMC Handle switch closed\n\r");
			}
			
			else if(!strncmp((const char *)command, "OPENAMCHS", 9)){				//Get AMC status "OPENAMCHS"
				open_AMC_handle();
				fprintf(uart_fd, "AMC Handle switch opened\n\r");
			}
			
			else if(!strncmp((const char *)command, "GETIO", 5)){				//Get IO "GETIO"
				sscanf((const char *)(&command[8]),"%x", &io_id);
				fprintf(uart_fd, "Pin state (%d): %02xh \n\r", io_id, get_io(io_id));
			}
		
			else if(!strncmp((const char *)command, "SETIO", 5)){				//Set IO "SETIO"
				sscanf((const char *)(&command[8]),"%x", &io_id);
				set_io(io_id);
				fprintf(uart_fd, "Set IO (%d) \n\r", io_id);
			}
			
			else if(!strncmp((const char *)command, "CLRIO", 5)){				//Clear IO "CLRIO"
				sscanf((const char *)(&command[8]),"%x", &io_id);
				clr_io(io_id);
				fprintf(uart_fd, "Clear IO (%d) \n\r", io_id);
			}
			
			else if(!strncmp((const char *)command, "GETBLUELED", 10)){				//Get Blue led "GETBLUELED"
				fprintf(uart_fd, "Blue led: %s \n\r", get_blue_led_status() ? "ON" : "OFF");
			}
			
            else if(!strncmp((const char *)command, "GETLED1", 10)){                //Get FRU LED 1
                fprintf(uart_fd, "FRU LED 1: %s \n\r", get_led1_status() ? "ON" : "OFF");
            }

            else if(!strncmp((const char *)command, "GETLED2", 10)){                //Get FRU LED 2
                fprintf(uart_fd, "FRU LED 2: %s \n\r", get_led2_status() ? "ON" : "OFF");
            }

            else if(!strncmp((const char *)command, "GETLED3", 10)){                //Get FRU LED 3
                fprintf(uart_fd, "FRU LED 3: %s \n\r", get_led3_status() ? "ON" : "OFF");
            }

			else if(!strncmp((const char *)command, "GET12VEN", 8)){				//Get 12V enable status "GET12VEN"
				fprintf(uart_fd, "12V Enable: %s \n\r", get_12vEn_status() ? "ON" : "OFF");
			}
			
			else if(!strncmp((const char *)command, "ASSERTPGOODA", 12)){			//Assert PowerGood A "ASSERTPGOODA"
				PORTC |= 0x20;
			}
			
			else if(!strncmp((const char *)command, "DEASSERTPGOODA", 14)){			//De-assert PowerGood A "DEASSERTPGOODA"
				PORTC &= 0xDF;
			}
		
			else if(!strncmp((const char *)command, "ASSERTPGOODB", 12)){			//Assert PowerGood A "ASSERTPGOODB"
				PORTC |= 0x40;
			}
		
			else if(!strncmp((const char *)command, "DEASSERTPGOODB", 14)){			//De-assert PowerGood A "DEASSERTPGOODB"
				PORTC &= 0xBF;
			}
			
			else if(!strncmp((const char *)command, "GETAMCMPEN", 10)){				//Get AMC management power "GETAMCMPEN"
				fprintf(uart_fd, "MP Enable: %s \n\r", get_mpen_status() ? "ON" : "OFF");
			}
			
			else if(!strncmp((const char *)command, "GETAMCPPEN", 10)){				//Get AMC payload power "GETAMCPPEN"
				fprintf(uart_fd, "PP Enable: %s \n\r", get_ppen_status() ? "ON" : "OFF");
			}
			
			else if(!strncmp((const char *)command, "ASSERTMPGOOD", 12)){			//Assert Management power good "ASSERTMPGOOD"
				PORTB |= 0x20;
				fprintf(uart_fd, "Assert Management power good");
			}
		
			else if(!strncmp((const char *)command, "DEASSERTMPGOOD", 14)){			//De-assert Management power good "DEASSERTMPGOOD"
				PORTB &= 0xDF;
				fprintf(uart_fd, "De-assert Management power good");
			}
		
			else if(!strncmp((const char *)command, "ASSERTPPGOOD", 12)){			//Assert Payload power good "ASSERTPPGOOD"
				PORTB |= 0x40;
				fprintf(uart_fd, "Assert Payload power good");
			}
		
			else if(!strncmp((const char *)command, "DEASSERTPPGOOD", 14)){			//De-assert Payload power good "DEASSERTPPGOOD"
				PORTB &= 0xBF;
				fprintf(uart_fd, "De-assert Payload power good");
			}
		
			else if(!strncmp((const char *)command, "GETMGTVOLTAGE", 14)){			//Get IPMC voltage "GETIPMCVOLTAGE"
				ltc2990_value = LTC2990_voltage_getvalue(0x4c, 0x4f, 0x06);
				//ltc2990_value *= 0.02;
				fprintf(uart_fd, "MGT +3.3V: %d\n\r", ltc2990_value);
			}
		
		
			else if(!strncmp((const char *)command, "GETIPMCVOLTAGE", 14)){			//Get IPMC voltage "GETIPMCVOLTAGE"
				ltc2990_value = LTC2990_voltage_getvalue(0x4c, 0x4f, 0x08);
				//ltc2990_value *= 0.02;
				fprintf(uart_fd, "IPMC +3.3V: %d\n\r", ltc2990_value);
			}
			
			else if(!strncmp((const char *)command, "GETIPMCCURRENT", 14)){			//Get IPMC voltage "GETIPMCVOLTAGE"
				ltc2990_value = LTC2990_current_getvalue(0x4c, 0x56, 0x0a, 0.1, 1.0);
				//ltc2990_value *= 0.02;
				fprintf(uart_fd, "IPMC Curr: %d\n\r", ltc2990_value);
			}
		
			else if(!strncmp((const char *)command, "SELI2C", 6)){					//Select I2c port "SELI2C 0x(val)"
				sscanf((const char *)(&command[9]),"%x", &i2c_port);
				sel_i2c_port(i2c_port);
				fprintf(uart_fd, "Select I2C port (%d)\n\r", i2c_port);
			}
		
			else{
				fprintf(uart_fd, "Unknown command\n\r");
			}
					
		}else{
			fprintf(uart_fd, "ERROR: (USART_wait_for_command) command was longer than expected \n\r");
		}
		
	}
}