/*
 * gpio_intf.c
 *
 * Created: 24/05/2017 09:54:56
 *  Author: jumendez
 */ 

#include <avr/io.h>		    // include I/O definitions (port names, pin names, etc)

#include "tools.h"
#include "gpio_intf.h"

void set_signal_val(unsigned char id, unsigned char state){
	if(state == HIGH)
		set_signal(id);
	else
		clear_signal(id);
}

unsigned char get_signal(unsigned char id){
	
	switch(id & 0xF0){
		
		case 0x00:	//PORTA
			return (inb(PINA) & BV(id & 0x0F)) ? HIGH:LOW;
			break;
		
		case 0x10:	//PORTB
			return (inb(PINB) & BV(id & 0x0F)) ? HIGH:LOW;
			break;
		
		case 0x20:	//PORTC
			return (inb(PINC) & BV(id & 0x0F)) ? HIGH:LOW;
			break;
		
		case 0x30:	//PORTD
			return (inb(PIND) & BV(id & 0x0F)) ? HIGH:LOW;
			break;
		
		case 0x40:	//PORTE
			return (inb(PINE) & BV(id & 0x0F)) ? HIGH:LOW;
			break;
		
		case 0x50:	//PORTF
			return (inb(PINF) & BV(id & 0x0F)) ? HIGH:LOW;
			break;
		
		case 0x60:	//PORTG
			return (inb(PING) & BV(id & 0x0F)) ? HIGH:LOW;
			break;
	}
	
	return 0xFF;
}

void set_signal(unsigned char id){
	
	switch(id & 0xF0){
		
		case 0x00:	//PORTA
			sbi(PORTA, (id & 0x0F));
			break;
		
		case 0x10:	//PORTB
			sbi(PORTB, (id & 0x0F));
			break;
		
		case 0x20:	//PORTC
			sbi(PORTC, (id & 0x0F));
			break;
		
		case 0x30:	//PORTD
			sbi(PORTD, (id & 0x0F));
			break;
		
		case 0x40:	//PORTE
			sbi(PORTE, (id & 0x0F));
			break;
		
		case 0x50:	//PORTF
			sbi(PORTF, (id & 0x0F));
			break;
			
		case 0x60:	//PORTG
			sbi(PORTG, (id & 0x0F));
			break;
	}
}

void clear_signal(unsigned char id){
	
	switch(id & 0xF0){
		
		case 0x00:	//PORTA
			cbi(PORTA, (id & 0x0F));
			break;
			
		case 0x10:	//PORTB
			cbi(PORTB, (id & 0x0F));
			break;
		
		case 0x20:	//PORTC
			cbi(PORTC, (id & 0x0F));
			break;
		
		case 0x30:	//PORTD
			cbi(PORTD, (id & 0x0F));
			break;
		
		case 0x40:	//PORTE
			cbi(PORTE, (id & 0x0F));
			break;
		
		case 0x50:	//PORTF
			cbi(PORTF, (id & 0x0F));
			break;
		
		case 0x60:	//PORTG
			cbi(PORTG, (id & 0x0F));
			break;
	}
}

void set_signal_dir(unsigned char id, unsigned char inout){
	
	switch(id & 0xF0){
		
		case 0x00:	//PORTA
			if(inout)	cbi(DDRA, (id & 0x0F));
			else		sbi(DDRA, (id & 0x0F));
			break;
		
		case 0x10:	//PORTB
			if(inout)	cbi(DDRB, (id & 0x0F));
			else		sbi(DDRB, (id & 0x0F));
			break;
		
		case 0x20:	//PORTC
			if(inout)	cbi(DDRC, (id & 0x0F));
			else		sbi(DDRC, (id & 0x0F));
			break;
		
		case 0x30:	//PORTD
			if(inout)	cbi(DDRD, (id & 0x0F));
			else		sbi(DDRD, (id & 0x0F));
			break;
		
		case 0x40:	//PORTE
			if(inout)	cbi(DDRE, (id & 0x0F));
			else		sbi(DDRE, (id & 0x0F));
			break;
		
		case 0x50:	//PORTF
			if(inout)	cbi(DDRF, (id & 0x0F));
			else		sbi(DDRF, (id & 0x0F));
			break;
		
		case 0x60:	//PORTG
			if(inout)	cbi(DDRG, (id & 0x0F));
			else		sbi(DDRG, (id & 0x0F));
			break;
	}
}

void init_port(void){
}