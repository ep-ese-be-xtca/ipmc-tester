/*
 * boot.c
 *
 * Created: 14/10/2015 15:23:29
 *  Author: jumendez
 */ 
#include <avr/io.h>
#include <avr/wdt.h>

#include "../inc/a2d.h"
#include "../inc/timer.h"
#include "../inc/i2csw.h"
#include "../inc/iodeclaration.h"
#include "../inc/avrlibdefs.h"
#include "../inc/i2c.h"

void boot_uC(){
	SFIOR |= 0x04;					// disable internal pull-up resistors
	
	init_port();					// I/Os init
	a2dInit();						// A2D init
	i2cswInit();					// Custom I2C init (sensors)
	timerInit();					//Timer initialization
	i2cInit();                      // initialize i2c function library
	
	sei();							// enable all interrupts
}

void reset_uC(){
	wdt_disable();
	wdt_enable(WDTO_15MS);
	while(1);
}
