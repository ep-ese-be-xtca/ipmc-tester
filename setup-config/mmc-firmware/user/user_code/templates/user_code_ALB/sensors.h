/*
 * sensors.h
 *
 * Created: 26/03/2015 22:22:20
 *  Author: jumendez
 */ 


#ifndef SENSORS_H_
#define SENSORS_H_

//Sensors ID
#define TEMPERATURE_SENSOR1     2
#define TEMPERATURE_SENSOR2		3
#define TEMPERATURE_SENSOR3		4
#define TEMPERATURE_SENSOR4		5
#define TEMPERATURE_SENSOR5		6
#define TEMPERATURE_SENSOR6		7
#define TEMPERATURE_SENSOR7		8
#define TEMPERATURE_SENSOR8		9
#define CURRENT_SENSOR			10

#define RTM_CURRENT_SENSOR      12      // 11 : RTM Hotswap sensor
#define RTM_TEMPERATURE1        13
#define RTM_TEMPERATURE2        14

/** User function called before executing init or update function (driver functions) -> E.g.: for I2C mux initialization */
void user_sensor_func(unsigned char sens_num);
#define USER_SENSOR_FUNC	user_sensor_func

/** Sensor drivers */
#define USER_SENSOR_CNT		8

#define LM82									\
	{											\
		{										\
			sensor_number: TEMPERATURE_SENSOR1,	\
			init_time: PP_PRESENT,				\
			name: "LM82-IC1",					\
			i2c_addr: 0x2A,						\
			p1: POINT(0,0),						\
			p2: POINT(1,1),						\
			upper_non_rec: 85,					\
			upper_critical: 75,					\
			upper_non_critical: 70,				\
			lower_non_critical: 10,				\
			lower_critical: 5,					\
			lower_non_rec: 0					\
		},										\
		{										\
			sensor_number: TEMPERATURE_SENSOR2,	\
			init_time: PP_PRESENT,				\
			name: "LM82-IC2",					\
			i2c_addr: 0x2B,						\
			p1: POINT(0,0),						\
			p2: POINT(1,1),						\
			upper_non_rec: 85,					\
			upper_critical: 75,					\
			upper_non_critical: 70,				\
			lower_non_critical: 10,				\
			lower_critical: 5,					\
			lower_non_rec: 0					\
		}										\
	}


#define MCP9808									\
{												\
	{											\
		sensor_number: TEMPERATURE_SENSOR3,		\
		init_time: MP_PRESENT,					\
		name: "MCP9808-IC4",					\
		i2c_addr: 0x1E,							\
		p1: POINT(0,0),							\
		p2: POINT(10,5),						\
		upper_non_rec: 85,						\
		upper_critical: 75,						\
		upper_non_critical: 70,					\
		lower_non_critical: 10,					\
		lower_critical: 5,						\
		lower_non_rec: 0						\
	},											\
	{											\
		sensor_number: TEMPERATURE_SENSOR4,		\
		init_time: MP_PRESENT,					\
		name: "MCP9808-IC12",					\
		i2c_addr: 0x19,							\
		p1: POINT(0,0),							\
		p2: POINT(10,5),						\
		upper_non_rec: 85,						\
		upper_critical: 75,						\
		upper_non_critical: 70,					\
		lower_non_critical: 10,					\
		lower_critical: 5,						\
		lower_non_rec: 0						\
	},											\
	{											\
		sensor_number: TEMPERATURE_SENSOR5,		\
		init_time: MP_PRESENT,					\
		name: "MCP9808-IC11",					\
		i2c_addr: 0x1A,							\
		p1: POINT(0,0),							\
		p2: POINT(10,5),						\
		upper_non_rec: 85,						\
		upper_critical: 75,						\
		upper_non_critical: 70,					\
		lower_non_critical: 10,					\
		lower_critical: 5,						\
		lower_non_rec: 0						\
	},											\
	{											\
		sensor_number: TEMPERATURE_SENSOR6,		\
		init_time: MP_PRESENT,					\
		name: "MCP9808-IC3",					\
		i2c_addr: 0x1B,							\
		p1: POINT(0,0),							\
		p2: POINT(10,5),						\
		upper_non_rec: 85,						\
		upper_critical: 75,						\
		upper_non_critical: 70,					\
		lower_non_critical: 10,					\
		lower_critical: 5,						\
		lower_non_rec: 0						\
	},											\
	{											\
		sensor_number: TEMPERATURE_SENSOR7,		\
		init_time: MP_PRESENT,					\
		name: "MCP9808-IC10",					\
		i2c_addr: 0x1C,							\
		p1: POINT(0,0),							\
		p2: POINT(10,5),						\
		upper_non_rec: 85,						\
		upper_critical: 75,						\
		upper_non_critical: 70,					\
		lower_non_critical: 10,					\
		lower_critical: 5,						\
		lower_non_rec: 0						\
	},											\
	{											\
		sensor_number: TEMPERATURE_SENSOR8,		\
		init_time: MP_PRESENT,					\
		name: "MCP9808-IC2",					\
		i2c_addr: 0x1D,							\
		p1: POINT(0,0),							\
		p2: POINT(10,5),						\
		upper_non_rec: 85,						\
		upper_critical: 75,						\
		upper_non_critical: 70,					\
		lower_non_critical: 10,					\
		lower_critical: 5,						\
		lower_non_rec: 0						\
	}											\
}
/*
#define MCP9808																											\
	SENSOR_MCP9808(TEMPERATURE_SENSOR3, "MCP9008-IC4",	0x1E,	POINT(0,0), POINT(5,2.5), 85, 75, 65, 10, 5, 0),		\
	SENSOR_MCP9808(TEMPERATURE_SENSOR4, "MCP9808-IC12",	0x19,	POINT(0,0), POINT(5,2.5), 85, 75, 65, 10, 5, 0),		\
	SENSOR_MCP9808(TEMPERATURE_SENSOR5, "MCP9808-IC11", 0x1A,	POINT(0,0), POINT(5,2.5), 85, 75, 65, 10, 5, 0),		\
	SENSOR_MCP9808(TEMPERATURE_SENSOR6, "MCP9808-IC3",	0x1B,	POINT(0,0), POINT(5,2.5), 85, 75, 65, 10, 5, 0),		\
	SENSOR_MCP9808(TEMPERATURE_SENSOR7, "MCP9808-IC10", 0x1C,	POINT(0,0), POINT(5,2.5), 85, 75, 65, 10, 5, 0),		\
	SENSOR_MCP9808(TEMPERATURE_SENSOR8, "MCP9808-IC2",	0x1D,	POINT(0,0), POINT(5,2.5), 85, 75, 65, 10, 5, 0)

#define LM82																											\
	SENSOR_LM82(TEMPERATURE_SENSOR1, "LM82-IC1", 0x2A, POINT(0,0), POINT(5,5), 85, 75, 65, 0, -20, -10),				\
	SENSOR_LM82(TEMPERATURE_SENSOR2, "LM82-IC2", 0x2B, POINT(0,0), POINT(255,255), 85, 75, 65, 0, -20, -10)

#define AD7997_current																									\
	SENSOR_AD7997_current(CURRENT_SENSOR, "Current", 2, 0x22, POINT(0,-40), POINT(255,7.94), 254, 255, 219, 0, 0, 0)
*/

#define RTM0_RECORD     {                                                                                                           \
	0x00,                                                           /*Record ID: Filled by init */                                  \
	0x00,                                                           /*Record ID: Filled by init */                                  \
	0x51,                                                           /*SDR Version: 51h */                                           \
	0x01,                                                           /*Record type: 01h - full sensor */                             \
	0x33,                                                           /*Record length: Filled by init */                              \
	0x00,                                                           /*Sensor owner ID: Filled by init */							\
	0x00,                                                           /*Sensor owner LUN: 00h */                                      \
	RTM_CURRENT_SENSOR,												/*Sensor number */                                              \
	0xc0,                                                           /*Entity ID: C1h - RTM */                                       \
	0x00,                                                           /*Entity Instance */                                            \
	0x7F,                                                           /*Sensor Initialization */                                      \
	0xFC,                                                           /*Sensor capabilities */                                        \
	CURRENT,                                                        /*Sensor type*/                                                 \
	0x01,                                                           /*Event/Reading type code */                                    \
	0xFF,                                                           /*Assertion event mask (LSB) */                                 \
	0x7F,                                                           /*Assertion event mask (MSB) */                                 \
	0xFF,                                                           /*Deassertion event mask (LSB) */                               \
	0x7F,                                                           /*Deassertion event mask (MSB) */                               \
	0x00,                                                           /*Settable / Readable threshold mask (LSB) */					\
	0x00,                                                           /*Settable / Readable threshold mask (MSB) */					\
	0x00,                                                           /*Sensor unit: 00h - Unsigned */                                \
	0x05,                                                           /*Sensor unit 2 - Base unit */                                  \
	0x00,                                                           /*Sensor unit 3 - Modifier unit */                              \
	0x00,                                                           /*Linearization: 00h - Linear */                                \
	0x20,                                                           /*M */                                                          \
	0x10,                                                           /*M, Tolerance */                                               \
	0x00,                                                           /*B */                                                          \
	0x04,                                                           /*B, Accuracy */                                                \
	0x00,                                                           /*Accuracy, Accuracy exp */                                     \
	0xd0,                                                           /*R exp, B exp */                                               \
	0x07,                                                           /*Analog characteristic flag */                                 \
	1,                                                              /*Normal reading */                                             \
	219,                                                            /*Normal max */                                                 \
	0,                                                              /*Normal min */                                                 \
	0xFF,                                                           /*Maximum reading */                                            \
	0x00,                                                           /*Minimum reading */                                            \
	254,                                                            /*Upper non-recoverable threshold */							\
	250,                                                            /*Upper critical threshold */                                   \
	219,                                                            /*Upper non-critical threshold */                               \
	0,                                                              /*Lower non-recoverable threshold */                            \
	0,                                                              /*Lower critical threshold */                                   \
	0,                                                              /*Lower non critical threshold */								\
	0x00,                                                           /*Positive-going hysteresis */                                  \
	0x00,                                                           /*Negative-going hysteresis */                                  \
	0x00,                                                           /*reserved: 00h */                                              \
	0x00,                                                           /*reserved: 00h */                                              \
	0x00,                                                           /*reserved for OEM use */                                       \
	0xC7,                                                           /*ID string type/length */                                      \
	'C','u','r','r','e','n','t'										/*ID String */                                                  \
}
	
#define RTM1_RECORD             {                                                                                                   \
	0x00,                                                           /*Record ID: Filled by init */                                  \
	0x00,                                                           /*Record ID: Filled by init */                                  \
	0x51,                                                           /*SDR Version: 51h */                                           \
	0x01,                                                           /*Record type: 01h - full sensor */                             \
	0x33,                                                           /*Record length: Filled by init */                              \
	0x00,                                                           /*Sensor owner ID: Filled by init */							\
	0x00,                                                           /*Sensor owner LUN: 00h */                                      \
	RTM_TEMPERATURE1,												/*Sensor number */                                              \
	0xc0,                                                           /*Entity ID: C1h - AMC */                                       \
	0x00,                                                           /*Entity Instance */                                            \
	0x7F,                                                           /*Sensor Initialization */                                      \
	0xFC,                                                           /*Sensor capabilities */                                        \
	TEMPERATURE,													/*Sensor type*/                                                 \
	0x01,                                                           /*Event/Reading type code */                                    \
	0xFF,                                                           /*Assertion event mask (LSB) */                                 \
	0x7F,                                                           /*Assertion event mask (MSB) */                                 \
	0xFF,                                                           /*Deassertion event mask (LSB) */                               \
	0x7F,                                                           /*Deassertion event mask (MSB) */                               \
	0x00,                                                           /*Settable / Readable threshold mask (LSB) */					\
	0x00,                                                           /*Settable / Readable threshold mask (MSB) */					\
	0x00,                                                           /*Sensor unit: 00h - Unsigned */                                \
	0x01,                                                           /*Sensor unit 2 - Base unit */                                  \
	0x00,                                                           /*Sensor unit 3 - Modifier unit */                              \
	0x00,                                                           /*Linearization: 00h - Linear */                                \
	0x01,                                                           /*M */                                                          \
	0x01,                                                           /*M, Tolerance */                                               \
	0x00,                                                           /*B */                                                          \
	0x25,                                                           /*B, Accuracy */                                                \
	0x88,                                                           /*Accuracy, Accuracy exp */                                     \
	0x00,                                                           /*R exp, B exp */                                               \
	0x07,                                                           /*Analog characteristic flag */                                 \
	25,                                                             /*Normal reading */												\
	50,                                                             /*Normal max */													\
	20,																/*Normal min */													\
	127,                                                            /*Maximum reading */                                            \
	-128,                                                           /*Minimum reading */                                            \
	85,                                                             /*Upper non-recoverable threshold */							\
	75,                                                             /*Upper critical threshold */                                   \
	65,																/*Upper non-critical threshold */                               \
	-20,                                                            /*Lower non-recoverable threshold */							\
	-10,                                                            /*Lower critical threshold */                                   \
	0,                                                              /*Lower non critical threshold */								\
	0x02,                                                           /*Positive-going hysteresis */                                  \
	0x02,                                                           /*Negative-going hysteresis */                                  \
	0x00,                                                           /*reserved: 00h */                                              \
	0x00,                                                           /*reserved: 00h */                                              \
	0x00,                                                           /*reserved for OEM use */                                       \
	0xC8,                                                           /*ID string type/length */                                      \
	'L','M','8','2','R','T','M','1'         /*ID String */                                  \
}

#define RTM2_RECORD             {                                                                                                                               \
	0x00,                                                           /*Record ID: Filled by init */                                  \
	0x00,                                                           /*Record ID: Filled by init */                                  \
	0x51,                                                           /*SDR Version: 51h */                                                   \
	0x01,                                                           /*Record type: 01h - full sensor */                             \
	0x33,                                                           /*Record length: Filled by init */                              \
	0x00,                                                           /*Sensor owner ID: Filled by init */                    \
	0x00,                                                           /*Sensor owner LUN: 00h */                                              \
	RTM_TEMPERATURE2,                                       /*Sensor number */                                                              \
	0xc0,                                                           /*Entity ID: C1h - AMC */                                               \
	0x00,                                                           /*Entity Instance */                                                    \
	0x7F,                                                           /*Sensor Initialization */                                              \
	0xFC,                                                           /*Sensor capabilities */                                                \
	TEMPERATURE,                                            /*Sensor type*/                                                                 \
	0x01,                                                           /*Event/Reading type code */                                    \
	0xFF,                                                           /*Assertion event mask (LSB) */                                 \
	0x7F,                                                           /*Assertion event mask (MSB) */                                 \
	0xFF,                                                           /*Deassertion event mask (LSB) */                               \
	0x7F,                                                           /*Deassertion event mask (MSB) */                               \
	0x00,                                                           /*Settable / Readable threshold mask (LSB) */   \
	0x00,                                                           /*Settable / Readable threshold mask (MSB) */   \
	0x00,                                                           /*Sensor unit: 00h - Unsigned */                                \
	0x01,                                                           /*Sensor unit 2 - Base unit */                                  \
	0x00,                                                           /*Sensor unit 3 - Modifier unit */                              \
	0x00,                                                           /*Linearization: 00h - Linear */                                \
	0x01,                                                           /*M */                                                                                  \
	0x01,                                                           /*M, Tolerance */                                                               \
	0x00,                                                           /*B */                                                                                  \
	0x25,                                                           /*B, Accuracy */                                                                \
	0x88,                                                           /*Accuracy, Accuracy exp */                                             \
	0x00,                                                           /*R exp, B exp */                                                               \
	0x07,                                                           /*Analog characteristic flag */                                 \
	25,                                                                     /*Normal reading */                                                             \
	50,                                                                     /*Normal max */                                                                 \
	20,                                                                     /*Normal min */                                                                 \
	127,                                                            /*Maximum reading */                                                    \
	-128,                                                           /*Minimum reading */                                                    \
	85,                                                                     /*Upper non-recoverable threshold */                    \
	75,                                                                     /*Upper critical threshold */                                   \
	65,                                                                     /*Upper non-critical threshold */                               \
	-20,                                                            /*Lower non-recoverable threshold */                    \
	-10,                                                            /*Lower critical threshold */                                   \
	0,                                                                      /*Lower non critical threshold */                               \
	0x02,                                                           /*Positive-going hysteresis */                                  \
	0x02,                                                           /*Negative-going hysteresis */                                  \
	0x00,                                                           /*reserved: 00h */                                                              \
	0x00,                                                           /*reserved: 00h */                                                              \
	0x00,                                                           /*reserved for OEM use */                                               \
	0xC8,                                                           /*ID string type/length */                                              \
	'L','M','8','2','R','T','M','2'         /*ID String */                                  \
}

#endif /* SENSORS_H_ */