/*
 * sensor_drivers.h
 *
 * Created: 23/10/2015 09:31:35
 *  Author: jumendez
 */ 

	/** Include all the needed headers */
	#include "../sensors/SHT21_temperature.h"
	#include "../sensors/LTC2990_temperature.h"
	#include "../sensors/LTC2990_voltage.h"
	#include "../sensors/LM82.h"