/*
 * sensors.c
 *
 * Created: 27/03/2015 12:37:17
 *  Author: jumendez
 */ 
#include "../../drivers/drivers.h"

#include "sensors.h"


//Sensor initialization
unsigned char setMUXchan(unsigned char sensor_number){	
	
	unsigned char MUXchannelToSet;
	unsigned char MUXchanSet;
	switch (sensor_number)
	{
		case 2:	MUXchannelToSet = 0x1;	break;	// U37 Bus 0: Humidity
		case 3:	MUXchannelToSet = 0x20; break;	// U37 Bus 5: Power_MON4
		case 4:	MUXchannelToSet = 0x2; break;	// U37 Bus 1: Power_MON1
		case 5:	MUXchannelToSet = 0x2; break;	// U37 Bus 1: Power_MON1
		case 6:	MUXchannelToSet = 0x4; break;	// U37 Bus 2: Power_MON2
		case 7:	MUXchannelToSet = 0x4; break;	// U37 Bus 2: Power_MON2
		default:
		/* Your code here */
		break;
	}
	/**/
	set_signal_dir(AVR32_PIN_PX44, 0x00);		//Multiplexer reset pin configuration
	set_signal(AVR32_PIN_PX44);				//Disable multiplexer reset
	
	ext_i2c_send (0x70, 0x00, 0, 1, &MUXchannelToSet);	//Multiplexer configuration
	ext_i2c_received(0x70, 0x00, 0, 1, &MUXchanSet);
	if(MUXchanSet != MUXchannelToSet)	return 0xFF;
	
	return 0x00;
}

//Sensor are polled (Every 100 ms)
void sensor_monitoring_user(){
}


void sensor_init_user(){
}