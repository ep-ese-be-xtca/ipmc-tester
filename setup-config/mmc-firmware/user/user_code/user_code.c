//*****************************************************************************
// File Name    : user_code.c
//
// Author		: Markus Joos (markus.joos@cern.ch)
// Modified by	: Julian Mendez <julian.mendez@cern.ch>
//
// Description	: Implementation of use commands:
//					-	OEM function: mandatory
//					-	Controller specific function: optionnal, enabled with 
//						ENABLE_CONTROLLER_SPECIFIC in user_code.h
//*****************************************************************************

// Header file
#include "../../application/inc/ipmi_if.h"	//Used for error code definitions
#include "../../application/inc/payload.h"	//Used for user main function (payload activation/deactivation)
#include "../../drivers/drivers.h"

#include "config.h"

/** User main function */
unsigned char user_main(unsigned char addr){
	while(1);
	return 0x01;
}

/** IPMI OEM commands */
	/*
	unsigned char ipmi_oem_user(unsigned char command, unsigned char *iana, unsigned char *user_data, unsigned char data_size, unsigned char *buf, unsigned char *error){
		//Note: You must not return more than "MAX_BYTES_READ" bytes defined in project.h file

		//Check if this OEM command is supported by us.
		//The originator of the OEM command (see table 5.1 in the IPMI 1.5 spec will send a 3-byte IANA ID
		//If we recognise this ID we respond to the command. Otherwise we return an error code

		*error = IPMI_CC_OK;

		if (iana[0] != IPMI_MSG_MANU_ID_MSB || iana[1] != IPMI_MSG_MANU_ID_B2 || iana[2] != IPMI_MSG_MANU_ID_LSB)
		{
			*error = IPMI_CC_PARAM_OUT_OF_RANGE;
			return(0);             //Not for us
		}

		buf[0] = IPMI_MSG_MANU_ID_MSB;   //We have to return our IANA ID as the first 3 bytes of the reply
		buf[1] = IPMI_MSG_MANU_ID_B2;
		buf[2] = IPMI_MSG_MANU_ID_LSB;

		//Check if we are supporting the command
		//if (command == 0x01)       //supported
		//...do something
		//else if (command == 0x02)  //not supported
		//{
		//    *error = IPMI_CC_INV_CMD;
		//    return(0);
		//}

		//Put here the additional data you want to return
		buf[3] = command + data_size;   //Just a test

		return(4);  //4 because we are returning 4 bytes.
	}
	*/
	
/** IPMI Controller Specific commands */
	/*
	unsigned char ipmi_controller_specific(unsigned char cmd, unsigned char *data, unsigned char data_len, unsigned char *buf, unsigned char *err){
		unsigned char rsp_length = 0;
	
		*error = IPMI_CC_OK;
	
		switch(command){
			case FRU_PROM_REVISION_CMD:	
				// ipmi_prom_version_change(user_data[0]);
				rsp_length = 0;
				break;
			
			case JTAG_CTRL_SET_CMD:	
				// ipmi_jtag_ctrl(rqs.data[0]);
				rsp_length = 0;
				break;
			
			case FPGA_JTAG_PLR_CMD:	
				// ipmi_fpga_jtag_plr_set(rqs.data[0]);
				rsp_length = 0;
				break;
			
			default:
				*error = IPMI_CC_INV_CMD;
		}
	
		return rsp_length;
	}
	*/