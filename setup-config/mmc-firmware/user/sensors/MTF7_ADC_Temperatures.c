/*
* MTF7_ADC_Temperatures.c
*
* Created: 2016/01/21 10:49:52
*  Author: 
*/

#include <string.h> 
#include <math.h> 

#include "../../drivers/drivers.h" 
#include "../../application/inc/sdr.h" 
#include "../../application/inc/payload.h" 
#include "../../application/inc/project.h" 
#include "../../application/inc/toolfunctions.h" 
#include "../user_code/sensors.h" 

#include "MTF7_ADC_Temperatures.h" 

#ifdef MTF7_ADC_Temperatures
	MTF7_ADC_Temperatures_t MTF7_ADC_Temperatures_sensors[] = MTF7_ADC_Temperatures; 

	#define MTF7_ADC_Temperatures_CNT		(sizeof(MTF7_ADC_Temperatures_sensors) / sizeof((MTF7_ADC_Temperatures_sensors)[0]))

	void MTF7_ADC_Temperatures_init(unsigned char Mux_ch, unsigned char ADC_ch){ 
//        adc_configure( &AVR32_ADC );
//        adc_enable   ( &AVR32_ADC, ADC_ch );
//        adc_start    ( &AVR32_ADC );
	}

	unsigned char MTF7_ADC_Temperatures_getvalue(unsigned char Mux_ch, unsigned char ADC_ch){ 
        if( Mux_ch & 0x1 ) set_signal  (MX0);
            else           clear_signal(MX0);
        if( Mux_ch & 0x2 ) set_signal  (MX1);
            else           clear_signal(MX1);
        if( Mux_ch & 0x4 ) set_signal  (MX2);
            else           clear_signal(MX2);
        if( Mux_ch & 0x8 ) set_signal  (MX3);
            else           clear_signal(MX3);
	
        adc_enable (&AVR32_ADC, ADC_ch );
        adc_start( &AVR32_ADC );
		   
        long xRawValue = adc_get_value( &AVR32_ADC, ADC_ch );
        adc_disable( &AVR32_ADC, ADC_ch );
	
        return (unsigned char)(xRawValue*(255.0f/1024.0f));
	}

	unsigned char update_MTF7_ADC_Temperatures_value(){
		unsigned int i; 
		unsigned char sdr[] = MTF7_ADC_Temperatures_SDR;	

		for(i=0; i < MTF7_ADC_Temperatures_CNT; i++){ 
			/** Check if init */
			if(!MTF7_ADC_Temperatures_sensors[i].is_init){
				if(MTF7_ADC_Temperatures_sensors[i].init_time == MP_PRESENT || (MTF7_ADC_Temperatures_sensors[i].init_time == PP_PRESENT && get_fru_state() == ACTIVATED)){
					if(MTF7_ADC_Temperatures_sensors[i].pre_user_func && MTF7_ADC_Temperatures_sensors[i].pre_user_func(MTF7_ADC_Temperatures_sensors[i].sensor_number)){
						set_sensor_value(MTF7_ADC_Temperatures_sensors[i].sensor_number,0);
						continue;
					}
					MTF7_ADC_Temperatures_init(MTF7_ADC_Temperatures_sensors[i].Mux_ch, MTF7_ADC_Temperatures_sensors[i].ADC_ch); 
					if(MTF7_ADC_Temperatures_sensors[i].post_user_func && MTF7_ADC_Temperatures_sensors[i].post_user_func(MTF7_ADC_Temperatures_sensors[i].sensor_number)){
						set_sensor_value(MTF7_ADC_Temperatures_sensors[i].sensor_number,0);
						continue;
					}
					MTF7_ADC_Temperatures_sensors[i].is_init = 1;
				}
			}else{
				if(MTF7_ADC_Temperatures_sensors[i].init_time == PP_PRESENT && get_fru_state() != ACTIVATED){
					MTF7_ADC_Temperatures_sensors[i].is_init = 0;
				}
			}

			/** Call getValue function */
			if(MTF7_ADC_Temperatures_sensors[i].is_init){
				if(MTF7_ADC_Temperatures_sensors[i].pre_user_func && MTF7_ADC_Temperatures_sensors[i].pre_user_func(MTF7_ADC_Temperatures_sensors[i].sensor_number)){
					set_sensor_value(MTF7_ADC_Temperatures_sensors[i].sensor_number,0);
					continue;
				}
				set_sensor_value(MTF7_ADC_Temperatures_sensors[i].sensor_number,MTF7_ADC_Temperatures_getvalue(MTF7_ADC_Temperatures_sensors[i].Mux_ch, MTF7_ADC_Temperatures_sensors[i].ADC_ch)); 
				if(MTF7_ADC_Temperatures_sensors[i].post_user_func && MTF7_ADC_Temperatures_sensors[i].post_user_func(MTF7_ADC_Temperatures_sensors[i].sensor_number)){
					set_sensor_value(MTF7_ADC_Temperatures_sensors[i].sensor_number,0);
					continue;
				}
				sensor_state_check(MTF7_ADC_Temperatures_sensors[i].sensor_number, MTF7_ADC_Temperatures_sensors[i].upper_non_rec, MTF7_ADC_Temperatures_sensors[i].upper_critical, MTF7_ADC_Temperatures_sensors[i].upper_non_critical, MTF7_ADC_Temperatures_sensors[i].lower_non_critical, MTF7_ADC_Temperatures_sensors[i].lower_critical, MTF7_ADC_Temperatures_sensors[i].lower_non_rec); 
				check_temp_event(MTF7_ADC_Temperatures_sensors[i].sensor_number, MTF7_ADC_Temperatures_sensors[i].upper_non_rec, MTF7_ADC_Temperatures_sensors[i].upper_critical, MTF7_ADC_Temperatures_sensors[i].upper_non_critical, MTF7_ADC_Temperatures_sensors[i].lower_non_critical, MTF7_ADC_Temperatures_sensors[i].lower_critical, MTF7_ADC_Temperatures_sensors[i].lower_non_rec, sdr[42], sdr[43]); 
			}
			else						set_sensor_value(MTF7_ADC_Temperatures_sensors[i].sensor_number,0); 
		}
		return 0;
	} 

	unsigned char init_MTF7_ADC_Temperatures_sdr(unsigned char sdr_id, unsigned char entity_inst, unsigned char owner_id){
		/** Variables */
		unsigned char i;	
		float x1, x2, y1, y2;	
		float a, b;	
		signed char Bexp, Rexp;	

		unsigned char sdr[] = MTF7_ADC_Temperatures_SDR;	

		/** Computing */
		for(i=0; i < MTF7_ADC_Temperatures_CNT; i++){ 
			MTF7_ADC_Temperatures_sensors[i].id = sdr_id+i;
			MTF7_ADC_Temperatures_sensors[i].entity_inst = entity_inst;
			MTF7_ADC_Temperatures_sensors[i].owner_id = owner_id;

			x1 = MTF7_ADC_Temperatures_sensors[i].p1[0];
			x2 = MTF7_ADC_Temperatures_sensors[i].p2[0];
			y1 =  MTF7_ADC_Temperatures_sensors[i].p1[1];
			y2 =  MTF7_ADC_Temperatures_sensors[i].p2[1];

			a = (y2-y1)/(x2-x1);
			b = y2-(a*x2);

			Bexp = Rexp = 0;

			if(floorf(a) != a || (a > 0 && a > 512) || (a < 1 && a < 512)){
				for(;((a > 0 && a > 512) || (a < 0 && a < -512)) && Rexp < 8; a/=10){
					Rexp++;
					Bexp--;
				}

				for(; ((a > 0 && a < 512) || (a < 0 && a > -512)) && Rexp > -8; a*=10){
					Rexp--;
					Bexp++;
				}

				if((a > 0 && a >= 512) || (a < 0 && a <= -512)){
					a /= 10;
					Rexp++;
					Bexp--;
				}
			}

			if(floorf(b) != b || (b > 0 && b > 512) || (b < 1 && b < 512)){
				for(;((b > 0 && b > 512) || (b < 0 && b < -512)) && Bexp < 8; b/=10){
					Bexp++;
				}

				for(; ((b > 0 && a < 512) || (b < 0 && b > -512)) && Bexp > -8; b*=10){
					Bexp--;
				}

				if((b > 0 && b >= 512) || (b < 0 && b <= -512)){
					b /= 10;
					Bexp++;
				}
			}

			MTF7_ADC_Temperatures_sensors[i].M = floor(a);
			MTF7_ADC_Temperatures_sensors[i].B = floor(b);
			MTF7_ADC_Temperatures_sensors[i].Rexp = Rexp;
			MTF7_ADC_Temperatures_sensors[i].Bexp = Bexp;

			/** Decimal to raw for thresholds */
			MTF7_ADC_Temperatures_sensors[i].upper_non_rec = val_to_raw(MTF7_ADC_Temperatures_sensors[i].upper_non_rec, MTF7_ADC_Temperatures_sensors[i].M, MTF7_ADC_Temperatures_sensors[i].B, MTF7_ADC_Temperatures_sensors[i].Rexp, MTF7_ADC_Temperatures_sensors[i].Bexp, (sdr[20] & 0x80)); 
			MTF7_ADC_Temperatures_sensors[i].upper_critical = val_to_raw(MTF7_ADC_Temperatures_sensors[i].upper_critical, MTF7_ADC_Temperatures_sensors[i].M, MTF7_ADC_Temperatures_sensors[i].B, MTF7_ADC_Temperatures_sensors[i].Rexp, MTF7_ADC_Temperatures_sensors[i].Bexp, (sdr[20] & 0x80)); 
			MTF7_ADC_Temperatures_sensors[i].upper_non_critical = val_to_raw(MTF7_ADC_Temperatures_sensors[i].upper_non_critical, MTF7_ADC_Temperatures_sensors[i].M, MTF7_ADC_Temperatures_sensors[i].B, MTF7_ADC_Temperatures_sensors[i].Rexp, MTF7_ADC_Temperatures_sensors[i].Bexp, (sdr[20] & 0x80)); 
			MTF7_ADC_Temperatures_sensors[i].lower_non_rec = val_to_raw(MTF7_ADC_Temperatures_sensors[i].lower_non_rec, MTF7_ADC_Temperatures_sensors[i].M, MTF7_ADC_Temperatures_sensors[i].B, MTF7_ADC_Temperatures_sensors[i].Rexp, MTF7_ADC_Temperatures_sensors[i].Bexp, (sdr[20] & 0x80)); 
			MTF7_ADC_Temperatures_sensors[i].lower_critical = val_to_raw(MTF7_ADC_Temperatures_sensors[i].lower_critical, MTF7_ADC_Temperatures_sensors[i].M, MTF7_ADC_Temperatures_sensors[i].B, MTF7_ADC_Temperatures_sensors[i].Rexp, MTF7_ADC_Temperatures_sensors[i].Bexp, (sdr[20] & 0x80)); 
			MTF7_ADC_Temperatures_sensors[i].lower_non_critical = val_to_raw(MTF7_ADC_Temperatures_sensors[i].lower_non_critical, MTF7_ADC_Temperatures_sensors[i].M, MTF7_ADC_Temperatures_sensors[i].B, MTF7_ADC_Temperatures_sensors[i].Rexp, MTF7_ADC_Temperatures_sensors[i].Bexp, (sdr[20] & 0x80)); 
		}
		return MTF7_ADC_Temperatures_CNT;
	}

	unsigned char get_MTF7_ADC_Temperatures_sdr_byte(signed char *byte, unsigned char len, unsigned char sensor_id, unsigned char offset){
		unsigned int i, j;
		unsigned char sdr[] = MTF7_ADC_Temperatures_SDR;
		unsigned char max_sdr_size = sizeof(sdr);
		unsigned char read_len = 0;	

		for(i=0; i < MTF7_ADC_Temperatures_CNT; i++){
			if(MTF7_ADC_Temperatures_sensors[i].sensor_number == sensor_id){
				for(j=0; j < len && (j+offset) < (max_sdr_size+strlen((const char *)MTF7_ADC_Temperatures_sensors[i].name)); j++, read_len++){
					if(j+offset == 0)						byte[j] = MTF7_ADC_Temperatures_sensors[i].id;
					else if(j+offset == 4)				byte[j] = (max_sdr_size+strlen((const char *)MTF7_ADC_Temperatures_sensors[i].name)) - 5;
					else if(j+offset == 9)				byte[j] = MTF7_ADC_Temperatures_sensors[i].entity_inst;
					else if(j+offset == 5)				byte[j] = MTF7_ADC_Temperatures_sensors[i].owner_id;
					else if(j+offset == 7)				byte[j] = MTF7_ADC_Temperatures_sensors[i].sensor_number;
					else if(j+offset == max_sdr_size-1)	byte[j] = 0xC0 | strlen((const char *)MTF7_ADC_Temperatures_sensors[i].name);
					else if(j+offset == 24)				byte[j] = ((unsigned char)MTF7_ADC_Temperatures_sensors[i].M) & 0xFF;
					else if(j+offset == 25)				byte[j] = (((unsigned char)(MTF7_ADC_Temperatures_sensors[i].M >> 2)) & 0xC0) | (sdr[j+offset] & 0x3F);
					else if(j+offset == 26)				byte[j] = ((unsigned char)MTF7_ADC_Temperatures_sensors[i].B) & 0xFF;
					else if(j+offset == 27)				byte[j] = (((unsigned char)(MTF7_ADC_Temperatures_sensors[i].B >> 2)) & 0xC0) | (sdr[j+offset] & 0x3F);
					else if(j+offset == 29)				byte[j] = ((MTF7_ADC_Temperatures_sensors[i].Rexp & 0x0F) << 4) | (MTF7_ADC_Temperatures_sensors[i].Bexp & 0x0F);
					else if(j+offset == 31)				byte[j] = (MTF7_ADC_Temperatures_sensors[i].upper_non_critical - MTF7_ADC_Temperatures_sensors[i].lower_non_critical)/2;
					else if(j+offset == 32)				byte[j] = MTF7_ADC_Temperatures_sensors[i].upper_non_critical;
					else if(j+offset == 33)				byte[j] = MTF7_ADC_Temperatures_sensors[i].lower_non_critical;
					else if(j+offset == 36)				byte[j] = MTF7_ADC_Temperatures_sensors[i].upper_non_rec;
					else if(j+offset == 37)				byte[j] = MTF7_ADC_Temperatures_sensors[i].upper_critical;
					else if(j+offset == 38)				byte[j] = MTF7_ADC_Temperatures_sensors[i].upper_non_critical;
					else if(j+offset == 39)				byte[j] = MTF7_ADC_Temperatures_sensors[i].lower_non_rec;
					else if(j+offset == 40)				byte[j] = MTF7_ADC_Temperatures_sensors[i].lower_critical;
					else if(j+offset == 41)				byte[j] = MTF7_ADC_Temperatures_sensors[i].lower_non_critical;
					else if(j+offset >= max_sdr_size)		byte[j] = MTF7_ADC_Temperatures_sensors[i].name[(j+offset)-max_sdr_size];
					else									byte[j] = sdr[j+offset];
				}
				return read_len;
			}
		}
		return 0x00;
	}

unsigned char set_MTF7_ADC_Temperatures_threshold(unsigned char sensor_id, unsigned char mask, unsigned char unr, unsigned char uc, unsigned char unc, unsigned char lnc, unsigned char lc, unsigned char lnr){ 
	unsigned char i; 

	for(i = 0; i < MTF7_ADC_Temperatures_CNT; i++){ 
		if(MTF7_ADC_Temperatures_sensors[i].sensor_number == sensor_id){ 
			if (mask & 0x01) MTF7_ADC_Temperatures_sensors[i].lower_non_critical = lnc;  
			if (mask & 0x02) MTF7_ADC_Temperatures_sensors[i].lower_critical = lc;  
			if (mask & 0x04) MTF7_ADC_Temperatures_sensors[i].lower_non_rec = lnr;  
			if (mask & 0x08) MTF7_ADC_Temperatures_sensors[i].upper_non_critical = unc; 
			if (mask & 0x10) MTF7_ADC_Temperatures_sensors[i].upper_critical = uc; 
			if (mask & 0x20) MTF7_ADC_Temperatures_sensors[i].upper_non_rec = unr; 

			return 0x00;		 
		} 
	} 

	return 0xFF; 
} 

#endif 
