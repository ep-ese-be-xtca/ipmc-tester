/*
* LM75.h
*
* Created: 24/06/2015 10:56:20
*  Author: jumendez
*/

#ifdef REGISTER_SENSOR
	#ifdef LM75
		SENSOR_DECLARATION(update_LM75_value, init_LM75_sdr, get_LM75_sdr_byte, set_LM75_threshold, 1);
	#endif
#else

	#ifndef LM75_H_
	#define LM75_H_

		typedef struct LM75_s{
			
			/** Generic information */
			unsigned char sensor_location;		/** AMC (default) or RTM */
			unsigned char init_time;			/** Execute initialization when MP (default) or PP is present */
			unsigned char is_init; 
			unsigned char sensor_number;
			char *name;
			
			/** Threshold information */
			unsigned char upper_non_rec;
			unsigned char upper_critical;
			unsigned char upper_non_critical;
			unsigned char lower_non_critical;
			unsigned char lower_critical;
			unsigned char lower_non_rec;
			
			/** Data used to fill SDR */
			unsigned char id;
			unsigned char entity_inst;
			unsigned char owner_id;
			
			/** Sensor dependent data */
			unsigned char i2c_addr;
			
			/** Raw to data computing */
			float p1[2];
			float p2[2];
			signed short M, B;
			signed char Bexp, Rexp;
			
			/** User specific function */			
			unsigned char  (*pre_user_func)(unsigned char sensor_number);
			unsigned char  (*post_user_func)(unsigned char sensor_number);
		} LM75_t; 
		
		unsigned char update_LM75_value();  
		unsigned char get_LM75_sdr_byte(signed char *byte, unsigned char len, unsigned char sensor_id, unsigned char offset); 
		unsigned char init_LM75_sdr(unsigned char sdr_id, unsigned char sensor_id, unsigned char entity_inst, unsigned char entity_id, unsigned char owner_id); 
		unsigned char set_LM75_threshold(unsigned char sensor_id, unsigned char mask, unsigned char unr, unsigned char uc, unsigned char unc, unsigned char lnc, unsigned char lc, unsigned char lnr);
		
		#define LM75_SDR { \
			0x00, 	  \
			0x00, 	  \
			0x51, 	  \
			0x01, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0xc1, 	  \
			0x00, 	  \
			0x7f, 	  \
			0xFC, 	  \
			TEMPERATURE, 	  \
			0x01, 	  \
			0xff, 	  \
			0x7f, 	  \
			0xff, 	  \
			0x7f, 	  \
			0x3f, 	  \
			0x3f, 	  \
			0x80, 	  \
			0x1, 	  \
			0x00, 	  \
			0x00, 	  \
			0x01, 	  \
			0x01, 	  \
			0x00, 	  \
			0x25, 	  \
			0x88, 	  \
			0x00, 	  \
			0x07, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x7F, 	  \
			0x80, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0x02, 	  \
			0x02, 	  \
			0x00, 	  \
			0x00, 	  \
			0x00, 	  \
			0xC0 	  \
		}

	#endif 
#endif 
