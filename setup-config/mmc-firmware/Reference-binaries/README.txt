These are the reference files for the MMC part of the IPMC-Tester.

Please note that the term "reference" is problematic because any change to the uMGT source code requires a re-compilation of the files.
The files in this directory have been built on the basis of the source code of 11 March 2022.