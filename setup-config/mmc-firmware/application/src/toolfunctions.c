/*! \file toolfunctions.c \ Tool functions */
//*****************************************************************************
//
// File Name	: 'toolfunctions.c'
// Title		: Tool functions
// Author		: Julian Mendez (julian.mendez@cern.ch)
// Created		: 20/04/2015
// Target MCU	: Atmel AVR series
//
//*****************************************************************************
#include <math.h>

#include "../inc/toolfunctions.h"

unsigned char checksum_clc(unsigned char* buf, unsigned char length){
	unsigned char crc = 0x00;

	while (length--)
		crc += buf[length];

	return (0 - crc);
}

unsigned char checsum_verify(unsigned char* buf, unsigned char length, unsigned char checksum){
	return (checksum - checksum_clc(buf, length));
}

unsigned char compare_val(unsigned char val1, unsigned char val2, unsigned char comp, unsigned char sig){
	if(sig == SIGNED){
		switch(comp){
			case UPPER_EQ:
				return (((signed char)val1) >= ((signed char)val2));
			case LOWER_EQ:
				return (((signed char)val1) <= ((signed char)val2));
		}
	}else{
		switch(comp){
			case UPPER_EQ:
			return ((val1) >= (val2));
			case LOWER_EQ:
			return ((val1) <= (val2));
		}		
	}
	
	return 0x00;
}

double val_d = 0;
	
unsigned char val_to_raw(unsigned char val, signed short M, signed short B, signed char Rexp, signed char Bexp, unsigned char signed_value){
	signed char Rexp_s, Bexp_s;
	//double val_d = 0;
	signed val_s;
	
	double M_d = (double) ((signed short)M);
	double B_d = (double) ((signed short)B);
	double Rexp_d;
	double Bexp_d;
	signed char retval;
	
	if(signed_value){
		val_s = (signed char) val;
		val_d = (double) val_s;
	}else{
		val_d = (double) val;
	}
	
	if(Rexp & 0x08)	Rexp_s = 0xF0 | Rexp;
	else			Rexp_s = Rexp;
	
	if(Bexp & 0x08)	Bexp_s = 0xF0 | Bexp;
	else			Bexp_s = Bexp;
	
	Rexp_d = (double) Rexp_s;
	Bexp_d = (double) Bexp_s;
	
	val_d /= pow(10,Rexp_d);
	val_d -= (B_d*pow(10, Bexp_d));
	val_d /= M_d;
	
	retval = (signed char)floor(val_d);
	return (unsigned char)retval;
}