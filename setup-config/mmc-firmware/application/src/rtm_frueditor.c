/*! \file rtm_frueditor.c \ FRU editor for RTM info */
//*****************************************************************************
//
// File Name	: 'rtm_frueditor.c'
// Title		: FRU editor for RTM info
// Author		: Julian Mendez (julian.mendez@cern.ch)
// Created		: 20/04/2015
// Target MCU	: Atmel AVR series
//
//*****************************************************************************
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "../../drivers/drivers.h"

#include "../../user/user_code/config.h"
#include "../../user/user_code/fru_info.h"

#include "../inc/toolfunctions.h"
#include "../inc/rtm_mng.h"
#include "../inc/i2crtm.h"
#include "../inc/rtm_frueditor.h"
#include "../inc/languages.h"

#ifdef USE_RTM

	#ifdef BOARD_INFO_AREA_ENABLE
		unsigned char *rtm_board_info_area;
	#endif

	#ifdef PRODUCT_INFO_AREA_ENABLE
		unsigned char *rtm_product_info_area;
	#endif

	unsigned char rtm_fru_header[8];
	unsigned char compatibility_record[18];
	
	void rtm_fru_header_function(void){
	
		unsigned char next_offset = 0x01;	//Header
	
		rtm_fru_header[0] = 0x01;
		rtm_fru_header[1] = 0x00;
		rtm_fru_header[2] = 0x00;
	
		#ifdef BOARD_INFO_AREA_ENABLE
			rtm_fru_header[3] = next_offset;
			next_offset += rtm_board_info_area[1];
		#else
			rtm_fru_header[3] = 0x00;
		#endif
	
		#ifdef PRODUCT_INFO_AREA_ENABLE
			rtm_fru_header[4] = next_offset;
			next_offset += rtm_product_info_area[1];
		#else
			rtm_fru_header[4] = 0x00;
		#endif
	
		#ifdef MULTIRECORD_AREA_ENABLE
			rtm_fru_header[5] = next_offset;
		#else
			rtm_fru_header[5] = 0x00;
		#endif
	
		rtm_fru_header[6] = 0x00;
	
		rtm_fru_header[7] = checksum_clc(rtm_fru_header, 8);
	}

	unsigned char tmp_rtmfru;
	void rtm_board_info_area_function(void){
		#ifdef BOARD_INFO_AREA_ENABLE
			int i;
		
			char board_name[strlen(BOARD_NAME)+strlen(FRU_RTM_EXTENSION)+1];
			char board_sn[strlen(BOARD_SN)+strlen(FRU_RTM_EXTENSION)+1];
			char board_pn[strlen(BOARD_PN)+strlen(FRU_RTM_EXTENSION)+1];
		
			unsigned char length = 13 + strlen(BOARD_MANUFACTURER) +
				strlen(BOARD_NAME) + strlen(FRU_RTM_EXTENSION) +
				strlen(BOARD_SN) + strlen(FRU_RTM_EXTENSION) +
				strlen(BOARD_PN) + strlen(FRU_RTM_EXTENSION) +
				strlen(FRU_FILE_ID);
		
			snprintf(board_name, strlen(BOARD_NAME)+strlen(FRU_RTM_EXTENSION)+1, "%s%s", BOARD_NAME, FRU_RTM_EXTENSION);
			snprintf(board_sn, strlen(BOARD_SN)+strlen(FRU_RTM_EXTENSION)+1, "%s%s", BOARD_SN, FRU_RTM_EXTENSION);
			snprintf(board_pn, strlen(BOARD_PN)+strlen(FRU_RTM_EXTENSION)+1, "%s%s", BOARD_PN, FRU_RTM_EXTENSION);
		
			while(length%8)
				length++;
		
			tmp_rtmfru = length;
			
			rtm_board_info_area = (unsigned char *)malloc(sizeof(unsigned char) * length);
		
			for(i=0; i < length; i++)
				rtm_board_info_area[i] = 0;
		
			rtm_board_info_area[0] = 0x01;
			rtm_board_info_area[1] = length/8;
			rtm_board_info_area[2] = LANG_CODE;
			rtm_board_info_area[3] = 0x00;
			rtm_board_info_area[4] = 0x00;
			rtm_board_info_area[5] = 0x00;
		
			rtm_board_info_area[6] = 0xC0 | (strlen(BOARD_MANUFACTURER) & 0x3F);
			for(i=0; i < strlen(BOARD_MANUFACTURER); i++)
				rtm_board_info_area[7+i] = BOARD_MANUFACTURER[i];
		
			rtm_board_info_area[7+strlen(BOARD_MANUFACTURER)] = 0xC0 | (strlen(board_name) & 0x3F);
			for(i=0; i < strlen(board_name); i++)
			rtm_board_info_area[8+strlen(BOARD_MANUFACTURER)+i] = board_name[i];
		
			rtm_board_info_area[8+strlen(BOARD_MANUFACTURER)+strlen(board_name)] = 0xC0 | (strlen(board_sn) & 0x3F);
			for(i=0; i < strlen(board_sn); i++)
			rtm_board_info_area[9+strlen(BOARD_MANUFACTURER)+strlen(board_name)+i] = board_sn[i];
		
			rtm_board_info_area[9+strlen(BOARD_MANUFACTURER)+strlen(board_name)+strlen(board_sn)] = 0xC0 | (strlen(board_pn) & 0x3F);
			for(i=0; i < strlen(board_pn); i++)
			rtm_board_info_area[10+strlen(BOARD_MANUFACTURER)+strlen(board_name)+strlen(board_sn)+i] = board_pn[i];
		
			rtm_board_info_area[10+strlen(BOARD_MANUFACTURER)+strlen(board_name)+strlen(board_sn)+strlen(board_pn)] = 0xC0 | (strlen(FRU_FILE_ID) & 0x3F);
			for(i=0; i < strlen(FRU_FILE_ID); i++)
			rtm_board_info_area[11+strlen(BOARD_MANUFACTURER)+strlen(board_name)+strlen(board_sn)+strlen(board_pn)+i] = FRU_FILE_ID[i];
		
			rtm_board_info_area[11+strlen(BOARD_MANUFACTURER)+strlen(board_name)+strlen(board_sn)+strlen(board_pn)+strlen(FRU_FILE_ID)] = 0xC1;
				
			rtm_board_info_area[length-1] = checksum_clc(rtm_board_info_area, length);
		#endif
	}

	void rtm_product_info_area_function(void){
		#ifdef PRODUCT_INFO_AREA_ENABLE
			int i;
			unsigned char length = 12 + strlen(PRODUCT_MANUFACTURER) +
				strlen(PRODUCT_NAME) + strlen(FRU_RTM_EXTENSION) +
				strlen(PRODUCT_PN) + strlen(FRU_RTM_EXTENSION) +
				strlen(PRODUCT_VERSION) + strlen(FRU_RTM_EXTENSION) +
				strlen(PRODUCT_SN) + strlen(FRU_RTM_EXTENSION) +
				strlen(PRODUCT_ASSET_TAG) + strlen(FRU_RTM_EXTENSION) +
				strlen(FRU_FILE_ID);
	
			char rtm_product_name[strlen(PRODUCT_NAME) + strlen(FRU_RTM_EXTENSION) + 1];
			char rtm_product_pn[strlen(PRODUCT_PN) + strlen(FRU_RTM_EXTENSION) + 1];
			char rtm_product_version[strlen(PRODUCT_VERSION) + strlen(FRU_RTM_EXTENSION) + 1];
			char rtm_product_sn[strlen(PRODUCT_SN) + strlen(FRU_RTM_EXTENSION) + 1];
			char rtm_product_asset_tag[strlen(PRODUCT_ASSET_TAG) + strlen(FRU_RTM_EXTENSION) + 1];
	
	
			snprintf(rtm_product_name, strlen(PRODUCT_NAME)+strlen(FRU_RTM_EXTENSION)+1, "%s%s", PRODUCT_NAME, FRU_RTM_EXTENSION);
			snprintf(rtm_product_pn, strlen(PRODUCT_PN)+strlen(FRU_RTM_EXTENSION)+1, "%s%s", PRODUCT_PN, FRU_RTM_EXTENSION);
			snprintf(rtm_product_version, strlen(PRODUCT_VERSION)+strlen(FRU_RTM_EXTENSION)+1, "%s%s", PRODUCT_VERSION, FRU_RTM_EXTENSION);
			snprintf(rtm_product_sn, strlen(PRODUCT_SN)+strlen(FRU_RTM_EXTENSION)+1, "%s%s", PRODUCT_SN, FRU_RTM_EXTENSION);
			snprintf(rtm_product_asset_tag, strlen(PRODUCT_ASSET_TAG)+strlen(FRU_RTM_EXTENSION)+1, "%s%s", PRODUCT_ASSET_TAG, FRU_RTM_EXTENSION);
	
			while(length%8)
				length++;
	
			rtm_product_info_area = (unsigned char *)malloc(sizeof(unsigned char) * length);
	
			for(i=0; i < length; i++)
				rtm_product_info_area[i] = 0;
	
			rtm_product_info_area[0] = 0x01;
			rtm_product_info_area[1] = length/8;
			rtm_product_info_area[2] = LANG_CODE;
	
			rtm_product_info_area[3] = 0xC0 | (strlen(PRODUCT_MANUFACTURER) & 0x3F);
			for(i=0; i < strlen(PRODUCT_MANUFACTURER); i++)
			rtm_product_info_area[4+i] = PRODUCT_MANUFACTURER[i];
	
			rtm_product_info_area[4+strlen(PRODUCT_MANUFACTURER)] = 0xC0 | (strlen(rtm_product_name) & 0x3F);
			for(i=0; i < strlen(rtm_product_name); i++)
			rtm_product_info_area[5+strlen(PRODUCT_MANUFACTURER)+i] = rtm_product_name[i];
	
			rtm_product_info_area[5+strlen(PRODUCT_MANUFACTURER)+strlen(rtm_product_name)] = 0xC0 | (strlen(rtm_product_pn) & 0x3F);
			for(i=0; i < strlen(rtm_product_pn); i++)
			rtm_product_info_area[6+strlen(PRODUCT_MANUFACTURER)+strlen(rtm_product_name)+i] = rtm_product_pn[i];
	
			rtm_product_info_area[6+strlen(PRODUCT_MANUFACTURER)+strlen(rtm_product_name)+strlen(rtm_product_pn)] = 0xC0 | (strlen(rtm_product_version) & 0x3F);
			for(i=0; i < strlen(rtm_product_version); i++)
			rtm_product_info_area[7+strlen(PRODUCT_MANUFACTURER)+strlen(rtm_product_name)+strlen(rtm_product_pn)+i] = rtm_product_version[i];
	
			rtm_product_info_area[7+strlen(PRODUCT_MANUFACTURER)+strlen(rtm_product_name)+strlen(rtm_product_pn)+strlen(rtm_product_version)] = 0xC0 | (strlen(rtm_product_sn) & 0x3F);
			for(i=0; i < strlen(rtm_product_sn); i++)
			rtm_product_info_area[8+strlen(PRODUCT_MANUFACTURER)+strlen(rtm_product_name)+strlen(rtm_product_pn)+strlen(rtm_product_version)+i] = rtm_product_sn[i];
	
			rtm_product_info_area[8+strlen(PRODUCT_MANUFACTURER)+strlen(rtm_product_name)+strlen(rtm_product_pn)+strlen(rtm_product_version)+strlen(rtm_product_sn)] = 0xC0 | (strlen(rtm_product_asset_tag) & 0x3F);
			for(i=0; i < strlen(rtm_product_asset_tag); i++)
			rtm_product_info_area[9+strlen(PRODUCT_MANUFACTURER)+strlen(rtm_product_name)+strlen(rtm_product_pn)+strlen(rtm_product_version)+strlen(rtm_product_sn)+i] = rtm_product_asset_tag[i];
	
			rtm_product_info_area[9+strlen(PRODUCT_MANUFACTURER)+strlen(rtm_product_name)+strlen(rtm_product_pn)+strlen(rtm_product_version)+strlen(rtm_product_sn)+strlen(rtm_product_asset_tag)] = 0xC0 | (strlen(FRU_FILE_ID) & 0x3F);
			for(i=0; i < strlen(FRU_FILE_ID); i++)
			rtm_product_info_area[10+strlen(PRODUCT_MANUFACTURER)+strlen(rtm_product_name)+strlen(rtm_product_pn)+strlen(rtm_product_version)+strlen(rtm_product_sn)+strlen(rtm_product_asset_tag)+i] = FRU_FILE_ID[i];
	
			rtm_product_info_area[10+strlen(PRODUCT_MANUFACTURER)+strlen(rtm_product_name)+strlen(rtm_product_pn)+strlen(rtm_product_version)+strlen(rtm_product_sn)+strlen(rtm_product_asset_tag)+strlen(FRU_FILE_ID)] = 0xC1;
	
			rtm_product_info_area[length-1] = checksum_clc(rtm_product_info_area, length);
		#endif
	}
	
	void rtm_board_info_area_function_light(void){
		#ifdef BOARD_INFO_AREA_ENABLE
			int i;
		
			char board_name[strlen(BOARD_NAME)+strlen(FRU_RTM_EXTENSION)+1];
		
			unsigned char length = 13 + strlen(BOARD_MANUFACTURER) +
				strlen(BOARD_NAME) + strlen(FRU_RTM_EXTENSION) +
				strlen(BOARD_SN) +
				strlen(BOARD_PN) +
				strlen("");
		
			snprintf(board_name, strlen(BOARD_NAME)+strlen(FRU_RTM_EXTENSION)+1, "%s%s", BOARD_NAME, FRU_RTM_EXTENSION);
		
			while(length%8)
				length++;
		
			rtm_board_info_area = (unsigned char *)malloc(sizeof(unsigned char) * length);
		
			for(i=0; i < length; i++)
				rtm_board_info_area[i] = 0;
		
			rtm_board_info_area[0] = 0x01;
			rtm_board_info_area[1] = length/8;
			rtm_board_info_area[2] = LANG_CODE;
			rtm_board_info_area[3] = 0x00;
			rtm_board_info_area[4] = 0x00;
			rtm_board_info_area[5] = 0x00;
		
			rtm_board_info_area[6] = 0xC0 | (strlen(BOARD_MANUFACTURER) & 0x3F);
			for(i=0; i < strlen(BOARD_MANUFACTURER); i++)
				rtm_board_info_area[7+i] = BOARD_MANUFACTURER[i];
		
			rtm_board_info_area[7+strlen(BOARD_MANUFACTURER)] = 0xC0 | (strlen(board_name) & 0x3F);
			for(i=0; i < strlen(board_name); i++)
			rtm_board_info_area[8+strlen(BOARD_MANUFACTURER)+i] = board_name[i];
		
			rtm_board_info_area[8+strlen(BOARD_MANUFACTURER)+strlen(board_name)] = 0xC0 | (strlen(BOARD_SN) & 0x3F);
			for(i=0; i < strlen(BOARD_SN); i++)
			rtm_board_info_area[9+strlen(BOARD_MANUFACTURER)+strlen(board_name)+i] = BOARD_SN[i];
		
			rtm_board_info_area[9+strlen(BOARD_MANUFACTURER)+strlen(board_name)+strlen(BOARD_SN)] = 0xC0 | (strlen(BOARD_PN) & 0x3F);
			for(i=0; i < strlen(BOARD_PN); i++)
			rtm_board_info_area[10+strlen(BOARD_MANUFACTURER)+strlen(board_name)+strlen(BOARD_SN)+i] = BOARD_PN[i];
		
			rtm_board_info_area[10+strlen(BOARD_MANUFACTURER)+strlen(board_name)+strlen(BOARD_SN)+strlen(BOARD_PN)] = 0xC0 | (strlen("") & 0x3F);
			for(i=0; i < strlen(""); i++)
			rtm_board_info_area[11+strlen(BOARD_MANUFACTURER)+strlen(board_name)+strlen(BOARD_SN)+strlen(BOARD_PN)+i] = ""[i];
		
			rtm_board_info_area[11+strlen(BOARD_MANUFACTURER)+strlen(board_name)+strlen(BOARD_SN)+strlen(BOARD_PN)+strlen("")] = 0xC1;
		
			rtm_board_info_area[length-1] = checksum_clc(rtm_board_info_area, length);
		#endif
	}

	void rtm_product_info_area_function_light(void){
		#ifdef PRODUCT_INFO_AREA_ENABLE
			int i;
			unsigned char length = 12 + strlen(PRODUCT_MANUFACTURER) +
				strlen(PRODUCT_NAME) + strlen(FRU_RTM_EXTENSION) +
				strlen("") +
				strlen("") +
				strlen("") +
				strlen("") +
				strlen("");
	
			char rtm_product_name[strlen(PRODUCT_NAME) + strlen(FRU_RTM_EXTENSION) + 1];
			snprintf(rtm_product_name, strlen(PRODUCT_NAME)+strlen(FRU_RTM_EXTENSION)+1, "%s%s", PRODUCT_NAME, FRU_RTM_EXTENSION);
			
			while(length%8)
				length++;
	
			rtm_product_info_area = (unsigned char *)malloc(sizeof(unsigned char) * length);
	
			for(i=0; i < length; i++)
				rtm_product_info_area[i] = 0;
	
			rtm_product_info_area[0] = 0x01;
			rtm_product_info_area[1] = length/8;
			rtm_product_info_area[2] = LANG_CODE;
	
			rtm_product_info_area[3] = 0xC0 | (strlen(PRODUCT_MANUFACTURER) & 0x3F);
			for(i=0; i < strlen(PRODUCT_MANUFACTURER); i++)
			rtm_product_info_area[4+i] = PRODUCT_MANUFACTURER[i];
	
			rtm_product_info_area[4+strlen(PRODUCT_MANUFACTURER)] = 0xC0 | (strlen(rtm_product_name) & 0x3F);
			for(i=0; i < strlen(rtm_product_name); i++)
			rtm_product_info_area[5+strlen(PRODUCT_MANUFACTURER)+i] = rtm_product_name[i];
	
			rtm_product_info_area[5+strlen(PRODUCT_MANUFACTURER)+strlen(rtm_product_name)] = 0xC0 | (strlen("") & 0x3F);
			for(i=0; i < strlen(""); i++)
			rtm_product_info_area[6+strlen(PRODUCT_MANUFACTURER)+strlen(rtm_product_name)+i] = ""[i];
	
			rtm_product_info_area[6+strlen(PRODUCT_MANUFACTURER)+strlen(rtm_product_name)+strlen("")] = 0xC0 | (strlen("") & 0x3F);
			for(i=0; i < strlen(""); i++)
			rtm_product_info_area[7+strlen(PRODUCT_MANUFACTURER)+strlen(rtm_product_name)+strlen("")+i] = ""[i];
	
			rtm_product_info_area[7+strlen(PRODUCT_MANUFACTURER)+strlen(rtm_product_name)+strlen("")+strlen("")] = 0xC0 | (strlen("") & 0x3F);
			for(i=0; i < strlen(""); i++)
			rtm_product_info_area[8+strlen(PRODUCT_MANUFACTURER)+strlen(rtm_product_name)+strlen("")+strlen("")+i] = ""[i];
	
			rtm_product_info_area[8+strlen(PRODUCT_MANUFACTURER)+strlen(rtm_product_name)+strlen("")+strlen("")+strlen("")] = 0xC0 | (strlen("") & 0x3F);
			for(i=0; i < strlen(""); i++)
			rtm_product_info_area[9+strlen(PRODUCT_MANUFACTURER)+strlen(rtm_product_name)+strlen("")+strlen("")+strlen("")+i] = ""[i];
	
			rtm_product_info_area[9+strlen(PRODUCT_MANUFACTURER)+strlen(rtm_product_name)+strlen("")+strlen("")+strlen("")+strlen("")] = 0xC0 | (strlen("") & 0x3F);
			for(i=0; i < strlen(""); i++)
			rtm_product_info_area[10+strlen(PRODUCT_MANUFACTURER)+strlen(rtm_product_name)+strlen("")+strlen("")+strlen("")+strlen("")+i] = ""[i];
	
			rtm_product_info_area[10+strlen(PRODUCT_MANUFACTURER)+strlen(rtm_product_name)+strlen("")+strlen("")+strlen("")+strlen("")+strlen("")] = 0xC1;
	
			rtm_product_info_area[length-1] = checksum_clc(rtm_product_info_area, length);
		#endif
	}
	
	void rtm_compatibility_record(){
		compatibility_record[0] = 0xC0;
		compatibility_record[1] = 0x82;
		compatibility_record[2] = 13;
		compatibility_record[3] = 0x00;	//Record checksum - to be calculated
		compatibility_record[4] = 0x00;	//Header checksum - to be calculated
		compatibility_record[5] = 0x5A;
		compatibility_record[6] = 0x31;
		compatibility_record[7] = 0x00;
		compatibility_record[8] = 0x30;
		compatibility_record[9] = 0x01;
		compatibility_record[10] = 0x03;
		compatibility_record[11] = IPMI_MSG_MANU_ID_LSB;
		compatibility_record[12] = IPMI_MSG_MANU_ID_B2;
		compatibility_record[13] = IPMI_MSG_MANU_ID_MSB;
		compatibility_record[14] = (unsigned char)((unsigned long)COMPATIBILITY_IDENTIFIER & (unsigned long)0x000000FF);
		compatibility_record[15] = (unsigned char)(((unsigned long)COMPATIBILITY_IDENTIFIER & (unsigned long)0x0000FF00) >> 8);
		compatibility_record[16] = (unsigned char)(((unsigned long)COMPATIBILITY_IDENTIFIER & (unsigned long)0x00FF0000) >> 16);
		compatibility_record[17] = (unsigned char)(((unsigned long)COMPATIBILITY_IDENTIFIER & (unsigned long)0xFF000000) >> 24);
		
		compatibility_record[3] = checksum_clc(&(compatibility_record[5]), 13);
		compatibility_record[4] = checksum_clc(compatibility_record, 5);
	}
	
	void free_rtm_editor(){
		unsigned char i;
		free(rtm_product_info_area);
		free(rtm_board_info_area);
		
		for(i=0; i<8; i++)	rtm_fru_header[i] = 0x00;
	}

	unsigned char err = 0;
	
	unsigned char write_rtm_fru_binary(){
		//unsigned char reset_eeprom[32] = {255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255};
		//unsigned char set_eeprom[32] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		
		rtm_board_info_area_function();
		rtm_product_info_area_function();
		rtm_compatibility_record();
		rtm_fru_header_function();
		
		if(26+rtm_board_info_area[1]*8+rtm_product_info_area[1]*8 >= 0x80){
			free_rtm_editor();
			rtm_board_info_area_function_light();
			rtm_product_info_area_function_light();
			rtm_fru_header_function();
		}
		
		clear_PCF8574_bit(RTM_IO_PORTS_ADDR, RTM_EEPROM_WRITE_PR_PIN);
		
		err = write_rtm_eeprom_page(0x00, 8, rtm_fru_header, 1);
		err = write_rtm_eeprom_page(rtm_fru_header[3]*8, rtm_board_info_area[1]*8, rtm_board_info_area, 1);
		err = write_rtm_eeprom_page(rtm_fru_header[4]*8, rtm_product_info_area[1]*8, rtm_product_info_area, 1);
		err = write_rtm_eeprom_page(rtm_fru_header[5]*8, 18, compatibility_record, 1);	
		
		if(err == 0xFF){
			rtm_fru_header[4] = 0;	//Remove product info area
			rtm_fru_header[5] = rtm_board_info_area[1] + rtm_fru_header[3];	//Change multi record offset
			rtm_fru_header[7] = checksum_clc(rtm_fru_header, 8);
			
			err = write_rtm_eeprom_page(0x00, 8, rtm_fru_header, 1);
			err = write_rtm_eeprom_page(8, rtm_board_info_area[1]*8, rtm_board_info_area, 1);
			err = write_rtm_eeprom_page(rtm_fru_header[5]*8, 18, compatibility_record, 1);
		}
		
		set_PCF8574_bit(RTM_IO_PORTS_ADDR, RTM_EEPROM_WRITE_PR_PIN);
		
		free_rtm_editor();
		
		return err;
	}
	
#endif