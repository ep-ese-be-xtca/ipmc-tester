/*! \file hpm.c \ HPM1 functions */
//*****************************************************************************
//
// File Name	: 'hpm.c'
// Title		: HPM.1 functions
// Author		: Julian Mendez (julian.mendez@cern.ch)
// Created		: 18/10/2014
// Target MCU	: Atmel AVR series
//
//*****************************************************************************
#include <string.h>

#include "../../drivers/drivers.h"

#include "../../user/user_code/config.h"

#include "../inc/project.h"
#include "../inc/ipmi_if.h"
#include "../inc/hpm.h"

unsigned char cmd_in_progress, last_cmd_comp_code, comp_estimate; //Get status upgrade cmd informations

unsigned char g_jump_to_bootloader;

void force_app_start(){	write_startup_flag(0xFF); }
void force_bootloader_start(){ write_startup_flag(0x00); }
	
void manage_hpm(){
	//Boot loader jump - HPM feature
	if(g_jump_to_bootloader){
		g_jump_to_bootloader = 0;
		force_bootloader_start();
		reset_uC();
	}
}
	
unsigned char hpm_response_send(unsigned char cmd, unsigned char *data, unsigned char *rsp_data, unsigned char *rsp_data_len){	//Return completion code
	
	unsigned char cc = IPMI_CC_OK;
	unsigned char i;
		
	switch (cmd){
		
		case GET_UPGRADE_STATUS:
			
			rsp_data[0] = cmd_in_progress;
			rsp_data[1] = last_cmd_comp_code;
			rsp_data[2] = comp_estimate;
			
			*rsp_data_len = 3;
			
			break;
			
		case GET_TARGET_UPGRADE_CAPABILITIES:
			rsp_data[0] = 0x00;		//HPM.1 version
			rsp_data[1] = 0x28;		//Capabilities: Table 3-3 HPM.1 specification
			rsp_data[2] = 24;		//Upgrade timeout: 2 min
			rsp_data[3] = 0x00;		//Self-test is not supported
			rsp_data[4] = 0x00;		//Rollback is not supported
			rsp_data[5] = 12;		//Inaccessibility timeout: 1 min
			rsp_data[6] = 0x01;		//Only one component
			
			*rsp_data_len = 7;
			
			break;
			
		case GET_COMPONENT_PROPERTIES:
			if (data[1] != 0)
				return 0x82;	//Invalid component ID
			
			switch(data[2]){
				case GENERAL_COMP_PROPERTIES:
					rsp_data[0] = 0x24;		//General component properties
					*rsp_data_len = 1;
					break;
					
				case COMP_CURRENT_VERSION:
					rsp_data[0] = MMC_FW_REL_MAJ;	//Firmware revision (Major)
					rsp_data[1] = MMC_FW_REL_MIN;	//Firmware revision (Minor)
					for(i=2; i<6; i++)				//Auxilary Firmware Revision Information
						rsp_data[i] = 0x00;
						
					*rsp_data_len = 6;
					break;
					
				case COMP_DESCRIPTION:
					for (i=0; i<12 ; i++){
						if(i< strlen(COMP_DESC_STR))
							rsp_data[i] = COMP_DESC_STR[i];			
						else
							rsp_data[i] = 0;
					}
					
					*rsp_data_len = 12;
					
					break;
					
				case COMP_ROLLBACK_FW_VERSION:	//Not implemented yet
					return 0x83;
					
				case COMP_DEFERRED_UPG_FW_VERSION: //Not implemented yet
					return 0x83;	//Invalid component properties selector
					
				//case COMP_OEM_OFFSET:					//No OEM properties implemented yet
				
				default:
					return 0xcc;	//Invalid component properties selector
				
			}
		
			break;
			
		case INITIATE_UPGRADE_ACTION:
			if (data[1] != 0x00){	//Invalid component
				return 0x81;
			}else if (last_cmd_comp_code == 0x80){	//Command in progress
				return 0x80;
			}
			
			switch (data[2]){
				case UPGRADE_ACT_BACKUP_COMP: //Not implemented yet
					return 0xCC;
					break;
					
				case UPGRADE_ACT_PREPARE_COMP:	//Not implemented yet
					return 0x00;
					break;
					
				case UPGRADE_ACT_UPLOAD_FOR_UPGRADE:	//Not implemented yet
					g_jump_to_bootloader = 1;
					last_cmd_comp_code = 0x80;
					cmd_in_progress = INITIATE_UPGRADE_ACTION;
					cc = 0x80;
					break;
					
				case UPGRADE_ACT_UPLOAD_FOR_COMPARE:	//Not implemented yet
					return 0xCC;
					break;
					
				default:
					return 0xCC;
			}
			
			break;
		
		case UPLOAD_FIRMWARE_BLOCK:
			cc = 0x83;		
			break;
			
		case FINISH_FIRMWARE_UPLOAD:
			cc = 0x81;
			break;
			
		default: 
			cc = 0xCC;
	}
	
	return cc;
}