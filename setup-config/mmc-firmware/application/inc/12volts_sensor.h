/*
 * _12volts_sensor.h
 *
 * Created: 22/05/2017 11:48:32
 *  Author: jumendez
 */ 


#ifndef VOLTS12_SENSOR_H_
#define VOLTS12_SENSOR_H_

unsigned char update_volt12_value();
unsigned char get_volt12_sdr_byte(signed char *byte, unsigned char len, unsigned char sensor_id, unsigned char offset);
unsigned char init_volt12_sdr(unsigned char sdr_id, unsigned char sensor_id, unsigned char entity_inst, unsigned char entity_id, unsigned char owner_id);
unsigned char set_volt12_threshold(unsigned char sensor_id, unsigned char mask, unsigned char unr, unsigned char uc, unsigned char unc, unsigned char lnc, unsigned char lc, unsigned char lnr);

#define V12_SDR																				\
{																							\
	0x00,				/* record number, LSB  - filled by SDR_Init() */					\
	0x00,				/* record number, MSB  - filled by SDR_Init() */					\
	0x51,				/* IPMI protocol version */											\
	0x01,				/* record type: full sensor */										\
	0x00,				/* record length: remaining bytes -> SDR_Init */					\
	0x00,				/* i2c address, -> SDR_Init */										\
	0x00,				/* sensor owner LUN */												\
	VOLT_12,			/* sensor number */													\
	0xc1,				/* entity id: AMC Module */											\
	0x00,				/* entity instance -> SDR_Init */									\
	0x7f,				/* init: event generation + scanning enabled */						\
	0xfc,				/* capabilities: auto re-arm, Threshold is readable and setable */	\
	VOLTAGE,			/* sensor type: Voltage */											\
	0x01,				/* sensor reading: Threshold */										\
	0xFF,				/* LSB assert event mask: 3 bit value */							\
	0x7F,				/* MSB assert event mask */											\
	0xFF,				/* LSB deassert event mask: 3 bit value */							\
	0x7F,				/* MSB deassert event mask */										\
	0x00,				/* LSB: readable Threshold mask: all thresholds are readable */		\
	0x00,				/* MSB: setable Threshold mask: all thresholds are setable */		\
	0x00,				/* sensor units 1 : unsigned */										\
	0x04,				/* sensor units 2 : VOLT */											\
	0x00,				/* sensor units 3 : Modifier */										\
	0x00,				/* Linearization */													\
	77,					/* M // M=77 */														\
	0x02,				/* M - Tolerance */													\
	6,					/* B = 6*/															\
	0x01,				/* B - Accuracy */													\
	0x00,				/* Accuracy - Accuracy exp */										\
	0xDE,				/* R-Exp , B-Exp 0xDE // *10^-4 */									\
	0x07,				/* Analogue characteristics flags */								\
	154,				/* Nominal reading */												\
	180,				/* Normal maximum */												\
	129,				/* Normal minimum */												\
	0xFF,				/* Sensor Maximum reading */										\
	0x00,				/* Sensor Minimum reading */										\
	245,				/* Upper non-recoverable Threshold */								\
	206,				/* Upper critical Threshold */										\
	180,				/* Upper non critical Threshold */									\
	129,				/* Lower non-recoverable Threshold */								\
	103,				/* Lower critical Threshold */										\
	77,					/* Lower non-critical Threshold */									\
	6,					/* positive going Threshold hysteresis value */						\
	6,					/* negative going Threshold hysteresis value */						\
	0x00,				/* reserved */														\
	0x00,				/* reserved */														\
	0x00,				/* OEM reserved */													\
	0xc3,				/* 8 bit ASCII, number of bytes */									\
	'1', '2', 'V'		/* sensor string */													\
}

#endif /* 12VOLTS_SENSOR_H_ */