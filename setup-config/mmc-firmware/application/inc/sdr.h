//*****************************************************************************
// Copyright (C) 2007 DESY(Deutsches-Elektronen Synchrotron) 
//
// File Name	: sdr.h
// 
// Title		: SDR header file
// Revision		: 1.0
// Notes		:	
// Target MCU	: Atmel AVR series
//
// Author       : Vahan Petrosyan (vahan_petrosyan@desy.de)
// Modified by  : Markus Joos (markus.joos@cern.ch)
// Modified by  : Julian Mendez (julian.mendez@cern.ch)
//
// Description  : Sensor Data Records definitions
//					
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//*****************************************************************************

#ifndef SDR_H
#define SDR_H

#define FULL_SENSOR				0x01

// Constants
#define ASSERTION_EVENT			0x00 //0x6F
#define DEASSERTION_EVENT		0x80 //0xEF

// Analogue or discrete sensors
#define ANALOG_SENSOR           1          //MJ: not used
#define DISCRETE_SENSOR         2          //MJ: not used

// Sensor type codes
#define TEMPERATURE			0x01
#define VOLTAGE             0x02
#define HOT_SWAP            0xF2
#define CURRENT				0x03
#define NONE				0x00

// Sensor unit type
#define DEGREES_C               1          //MJ: not used
#define VOLTS                   2          //MJ: not used

// Temperature States
#define TEMP_STATE_NORMAL	    0x00	   // temperature is in normal range
#define TEMP_STATE_LOW		    0x01	   // temperature is below lower non critical
#define TEMP_STATE_LOW_CRIT	    0x02	   // temperature is below lower critical
#define TEMP_STATE_LOW_NON_REC	0x04	   // temperature is below lower non recoverable
#define TEMP_STATE_HIGH		    0x08	   // temperature is higher upper non critical
#define TEMP_STATE_HIGH_CRIT	0x10	   // temperature is higher upper critical
#define TEMP_STATE_HIGH_NON_REC	0x20	   // temperature is higher high non recoverable

// Module handle sensor status
#define HOT_SWAP_CLOSED         0
#define HOT_SWAP_OPENED         1
#define HOT_SWAP_QUIESCED       2

//#define MAX_RECORD_SIZE         64

// registers for temperature sensor
#define WCRW                    0x0a       // write conversion rate
#define RLTS                    0x00       // local temperature
#define RRTE                    0x01       // remote temperature

// IPMI Sensor Events
#define IPMI_THRESHOLD_LNC_GL	                                0x00  // lower non critical going low
#define IPMI_THRESHOLD_LNC_GH	                                0x01  // lower non critical going high
#define IPMI_THRESHOLD_LC_GL	                                0x02  // lower critical going low
#define IPMI_THRESHOLD_LC_GH	                                0x03  // lower critical going HIGH
#define IPMI_THRESHOLD_LNR_GL	                                0x04  // lower non recoverable going low
#define IPMI_THRESHOLD_LNR_GH	                                0x05  // lower non recoverable going high
#define IPMI_THRESHOLD_UNC_GL	                                0x06  // upper non critical going low
#define IPMI_THRESHOLD_UNC_GH	                                0x07  // upper non critical going high
#define IPMI_THRESHOLD_UC_GL	                                0x08  // upper critical going low
#define IPMI_THRESHOLD_UC_GH	                                0x09  // upper critical going HIGH
#define IPMI_THRESHOLD_UNR_GL	                                0x0a  // upper non recoverable going low
#define IPMI_THRESHOLD_UNR_GH                                   0x0b  // upper non recoverable going high

#define MAX_SDR_LEN				80

#define P99_PROTECT(...)	__VA_ARGS__
#define POINT(x,y)			{x,y}
	
// Types
typedef struct sensor_info_s{
	unsigned char declared;
	unsigned char  (*update_f)();
	unsigned char  (*sdr_init)(unsigned char sdr_id, unsigned char sensor_id, unsigned char entity_inst, unsigned char entity_id, unsigned char owner_id);
	unsigned char  (*get_sdr)(signed char *byte, unsigned char len, unsigned char sensor_id, unsigned char offset);
	unsigned char  (*update_thresholds)(unsigned char sensor_id, unsigned char mask, unsigned char unr, unsigned char uc, unsigned char unc, unsigned char lnc, unsigned char lc, unsigned char lnr);
	unsigned int pulling_time;
	unsigned int time_cnter;
	
	unsigned char record_type;
	unsigned char type;
	unsigned char event_type_code;
}sensor_info_t;

typedef struct sensor
{
	unsigned char rec_type;
    unsigned char value;
	unsigned char value_changed_flag;
    unsigned char state;
	unsigned char old_state;
    unsigned char type;
    unsigned char evnt_scan;   //MJ: This flag indicates if a RTM sensor is active (i.e. if the RTM is present)
    unsigned char event_type_code;
    signed short sdr_ptr;
	unsigned char signed_flag;
	
	struct {
		struct {
			unsigned short upper_non_recoverable_go_high:1;
			unsigned short upper_non_recoverable_go_low:1;
			unsigned short upper_critical_go_high:1;
			unsigned short upper_critical_go_low:1;
			unsigned short upper_non_critical_go_high:1;
			unsigned short upper_non_critical_go_low:1;
			unsigned short lower_non_recorverable_go_high:1;
			unsigned short lower_non_recoverable_go_low:1;
			unsigned short lower_critical_go_high:1;
			unsigned short lower_critical_go_low:1;
			unsigned short lower_non_critical_go_high:1;
			unsigned short lower_non_critical_go_low:1;			
		} asserted;
		
		unsigned char ev;
		unsigned char assert_deassert;
		unsigned char sent_err;
	} event_management;
	/*
	struct {
		unsigned char normal_reading;			//SDR[31]
		unsigned char normal_maximum;			//SDR[32]
		unsigned char normal_minimum;			//SDR[33]
		unsigned char upper_non_recoverable;	//SDR[36]
		unsigned char upper_critical;			//SDR[37]
		unsigned char upper_non_critical;		//SDR[38]
		unsigned char lower_non_recoverable;	//SDR[39]
		unsigned char lower_critical;			//SDR[40]
		unsigned char lower_non_critical;		//SDR[41]
		unsigned char positive_going;			//SDR[42]
		unsigned char negative_going;			//SDR[43]
	} threshold;
	
	struct {
		unsigned short reserved:1;
		unsigned short lower_non_recoverable_comp_returned:1;
		unsigned short lower_critical_comp_returned:1;
		unsigned short lower_non_critical_comp_returned:1;
		unsigned short upper_non_recoverable_go_high_supported:1;
		unsigned short upper_non_recoverable_go_low_supported:1;
		unsigned short upper_critical_go_high_supported:1;
		unsigned short upper_critical_go_low_supported:1;
		unsigned short upper_non_critical_go_high_supported:1;
		unsigned short upper_non_critical_go_low_supported:1;
		unsigned short lower_non_recorverable_go_high_supported:1;
		unsigned short lower_non_recoverable_go_low_supported:1;
		unsigned short lower_critical_go_high_supported:1;
		unsigned short lower_critical_go_low_supported:1;
		unsigned short lower_non_critical_go_high_supported:1;
		unsigned short lower_non_critical_go_low_supported:1;
	} assertion_event_mask;	//SDR[14] / SDR[15]: For threshold based sensor(s) only
	
	struct {
		unsigned short reserved:1;
		unsigned short lower_non_recoverable_comp_returned:1;
		unsigned short lower_critical_comp_returned:1;
		unsigned short lower_non_critical_comp_returned:1;
		unsigned short upper_non_recoverable_go_high_supported:1;
		unsigned short upper_non_recoverable_go_low_supported:1;
		unsigned short upper_critical_go_high_supported:1;
		unsigned short upper_critical_go_low_supported:1;
		unsigned short upper_non_critical_go_high_supported:1;
		unsigned short upper_non_critical_go_low_supported:1;
		unsigned short lower_non_recorverable_go_high_supported:1;
		unsigned short lower_non_recoverable_go_low_supported:1;
		unsigned short lower_critical_go_high_supported:1;
		unsigned short lower_critical_go_low_supported:1;
		unsigned short lower_non_critical_go_high_supported:1;
		unsigned short lower_non_critical_go_low_supported:1;
	} deassertion_event_mask;	//SDR[16] / SDR[17]: For threshold based sensor(s) only
	
	struct {
		unsigned short upper_non_recoverable_go_high:1;
		unsigned short upper_non_recoverable_go_low:1;
		unsigned short upper_critical_go_high:1;
		unsigned short upper_critical_go_low:1;
		unsigned short upper_non_critical_go_high:1;
		unsigned short upper_non_critical_go_low:1;
		unsigned short lower_non_recorverable_go_high:1;
		unsigned short lower_non_recoverable_go_low:1;
		unsigned short lower_critical_go_high:1;
		unsigned short lower_critical_go_low:1;
		unsigned short lower_non_critical_go_high:1;
		unsigned short lower_non_critical_go_low:1;
	} asserted_event;	//SDR[14] / SDR[15]: For threshold based sensor(s) only*/
} sensor_t;


// Function prototypes
void sdr_init();			   //initialisation of the SDRs
void sensor_init();            //initialisation of the sensors
void check_temp_event(unsigned char sensID, unsigned char unr, unsigned char uc, unsigned char unc, unsigned char lnc, unsigned char lc, unsigned char lnr, unsigned char pos_hyst, unsigned char neg_hyst);
void sensor_state_check(unsigned char sensID, unsigned char unr, unsigned char uc, unsigned char unc, unsigned char lnc, unsigned char lc, unsigned char lnr);
unsigned char ipmi_sdr_info(unsigned char* buf, unsigned char sdr_sensor);
unsigned char ipmi_sdr_get(unsigned char* id, unsigned char offs, unsigned char size, unsigned char* buf);
unsigned char ipmi_get_sensor_reading(unsigned char sensor_number, unsigned char* buf);
unsigned char ipmi_picmg_get_device_locator(unsigned char fru, unsigned char* buf);
unsigned char ipmi_get_sensor_threshold(unsigned char sensor_number, unsigned char* buf);
unsigned char ipmi_set_sensor_threshold(unsigned char sensor_number, unsigned char mask, unsigned char lnc, unsigned char lcr, unsigned char lnr, unsigned char unc, unsigned char ucr, unsigned char unr);

void set_sensor_value(unsigned char sensID, unsigned char val);
unsigned char get_sensor_type(unsigned char sensID);

void sensor_init_user();
void sensor_monitoring_user();
void sensor_monitoring();
void set_sensor_value_changed(unsigned char sensID);
unsigned char get_sensor_value_changed_flag(unsigned char sensID);
unsigned char get_sensor_value(unsigned char sensID);
unsigned char get_event_type_code(unsigned char sensID);

void disable_sensor(unsigned char sensorID);
void enable_sensor(unsigned char sensorID);

#endif //SDR_H
