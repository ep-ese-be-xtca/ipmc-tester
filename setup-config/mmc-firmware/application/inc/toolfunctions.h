/*! \file toolfunctions.h \ Tool functions - Header file */
//*****************************************************************************
//
// File Name	: 'toolfunctions.h'
// Title		: Tool functions - Header file
// Author		: Julian Mendez (julian.mendez@cern.ch)
// Created		: 20/04/2015
// Target MCU	: Atmel AVR series
//
//*****************************************************************************
#ifndef TOOLFUNCTIONS_H_
#define TOOLFUNCTIONS_H_

#define SIGNED		0x01
#define UNSIGNED	0x00

#define LOWER_EQ	0x00
#define UPPER_EQ	0x01

unsigned char checksum_clc(unsigned char* buf, unsigned char length);										//Calculate checksum
unsigned char checsum_verify(unsigned char* buf, unsigned char length, unsigned char checksum);				//Verify checksum
unsigned char compare_val(unsigned char val1, unsigned char val2, unsigned char comp, unsigned char sig);	//Compare val1 with val2 (valx signs depend on the sig value)
unsigned char val_to_raw(unsigned char val, signed short M, signed short B, signed char Rexp, signed char Bexp, unsigned char signed_value);
#endif /* TOOLFUNCTIONS_H_ */