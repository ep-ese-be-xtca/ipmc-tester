open_project -project {./cern_ipmc/cern_ipmc.pro} -connect_programmers 1 
update_programming_file \
		 -name {A2F200M3F} \
		 -feature {setup_security:off} \
		 -feature {prog_fpga:on} \
		 -feature {prog_from:on} \
		 -feature {prog_nvm:on} \
		 -from_content {ufc} \
		 -from_config_file {./binaries/MSS_UFROM_0.ufc} \
		 -number_of_devices {1} \
		 -from_program_pages {0 1 } \
		 -efm_content {location:0;source:efc} \
		 -efm_block {location:0;config_file:{./binaries/MSS_ENVM_0.efc}} \
		 -fdb_file {./binaries/TOPLEVEL.fdb} \
		 -fdb_source {fdb} \
		 -pdb_file {./binaries/A2F200-ATCA.pdb} \
		 -enable_m3debugger {no} 
run_selected_actions
close_project