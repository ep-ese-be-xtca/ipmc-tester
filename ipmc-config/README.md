# How to program the CERN-IPMC with the default firmware

The program.py python script can be used to program the CERN-IPMC module via JTAG.
In order to use it, the FPExpress.exe location should be added to the PATH variable.