#!/usr/bin/env python
# encoding: utf-8

import os
import subprocess

def create_pdb_depends():
	filePath = os.path.dirname(os.path.abspath(__file__))
	fileContent = '''{}\\binaries\\TOPLEVEL.fdb
1573118330
{}\\binaries\\MSS_UFROM_0.ufc
1361973641
{}\\binaries\\MSS_ENVM_0.efc
1476693110
{}\\binaries\\nvm.ihx
1561018680
	'''.format(filePath,filePath,filePath,filePath)
	
	f = open('{}/cern_ipmc/A2F200-ATCA.pdb.depends'.format(filePath), "w")
	f.write(fileContent)
	f.close()	
	
def create_pro():
	filePath = os.path.dirname(os.path.abspath(__file__))
	
	fileContent = '''
		<project name="cern_ipmc" version="1.1">
			<ProjectDirectory>{}\\cern_ipmc</ProjectDirectory>
			<View>ChainView</View>
			<LogFile>{}\\cern_ipmc\\cern_ipmc.log</LogFile>
			<SerializationOption>Skip</SerializationOption>
			<programmer status="enable" type="FlashPro4" revision="UndefRev" connection="usb2.0">
				<name>74463</name>
				<id>74463</id>
			</programmer>
			<configuration>
				<Hardware>
					<FlashPro>
						<TCK>4000000</TCK>
						<Vpp/>
						<Vpn/>
						<Vddl/>
						<Vdd>2500</Vdd>
					</FlashPro>
					<FlashProLite>
						<TCK>4000000</TCK>
						<Vpp/>
						<Vpn/>
					</FlashProLite>
					<FlashPro3>
						<TCK>000000</TCK>
						<Vpump/>
						<ClkMode>FreeRunningClk</ClkMode>
					</FlashPro3>
					<FlashPro4>
						<TCK>4000000</TCK>
						<Vpump/>
						<ClkMode>FreeRunningClk</ClkMode>
					</FlashPro4>
					<FlashPro5>
						<TCK>4000000</TCK>
						<Vpump/>
						<ClkMode>FreeRunningClk</ClkMode>
					</FlashPro5>
					<FlashPro5>
						<TCK>4000000</TCK>
						<Vpump/>
						<ClkMode>FreeRunningClk</ClkMode>
					</FlashPro5>
				</Hardware>
				<Device type="ACTEL">
					<Name>A2F200M3F</Name>
					<Custom>A2F200M3F</Custom>
					<Algo type="PDB">
						<filename>
							{}\\binaries\\A2F200-ATCA.pdb
						</filename>
						<local>
							projectData\\A2F200-ATCA.pdb
						</local>
						<SelectedAction>
							PROGRAM
						</SelectedAction>
					</Algo>
				</Device>
				<Algo type="unknown">
					<irlength>
						0
					</irlength>
					<MaxTCK>
						0
					</MaxTCK>
				</Algo>
			</configuration>
		</project>
	'''.format(filePath, filePath, filePath)
	
	f = open('{}/cern_ipmc/cern_ipmc.pro'.format(filePath), "w")
	f.write(fileContent)
	f.close()
	
def program():
	create_pdb_depends()
	create_pro()

	filePath = os.path.dirname(os.path.abspath(__file__))
	process = subprocess.Popen(['FPExpress','script:script.tcl'], cwd=filePath, shell=True)

	cnter = 0

	while True:
		poll = process.poll()
		if poll != None:
			return poll

if __name__ == '__main__':
	program()